## Sekai 🌐 🗺

Sekai (世界) is the [kanji][1] for "the world".  That's a great word because of
the scale that it designates.

[1]: https://en.wikipedia.org/wiki/Kanji
