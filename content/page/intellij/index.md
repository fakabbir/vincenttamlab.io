---
title: "IntelliJ"
subtitle: "My notes for IntelliJ's shortcuts"
date: 2023-04-11T15:48:55+02:00
toc: true
draft: false
---

The following shortcuts are based on
<i class="fab fa-windows" aria-hidden></i> M\$ Windows.

### <i class="fas fa-arrows-alt fa-fw" aria-hidden></i> Navigation

- `<C-Up>` / `<C-Down>`: move up ↑ / down ↓ _without_ changing
<i class="fas fa-i-cursor" aria-hidden></i> cursor position.
- `<C-Alt-↑>` / `<C-Alt-↓>`: open file explorer for the project root.

### <i class="fas fa-search fa-fw" aria-hidden></i> Searching / Replacing

As usual, it supports regular expressions as expected.

- `<C-F>`: search
- `<C-R>`: replace
- `<F3>`: jump to next matching word
- `<S-F3>`: jump to previous matching word

### <i class="far fa-edit fa-fw" aria-hidden></i> Editing / Selection

Syntax-independent shortcuts:

- `<C-d>`: duplicate selected content, or the current line in case of empty
selection.
- `<Tab>` / `<S-Tab>`: indent / unindent selected line(s), like `<<` / `>>` in
[Vim].
- `<Alt-S-↑>` / `<Alt-S-↓>`: move selected line(s) up ↑ / down ↓ to next
attribute / method.
- `<Alt-j>`: select next instance that matches the currect selection.
- `<Alt-S-j>`: unselect current instance that matches the currect selection.
i.e. reverse of `<Alt-j>`.
- `<C-C-↑>` / `<C-C-↓>`: vertically aligned carets on multiple lines.
    1. Press and release `<C>` once.
    1. Press without releasing `<C>`.
    1. Add caret(s) to neighbouring lines with `↑` and/or `↓`.

### <i class="fas fa-code fa-fw" aria-hidden></i> Quick code navigation and generation

Syntax-dependent shortcuts:

- `<Alt-↑>` / `<Alt-↓>`: move <i class="fas fa-i-cursor" aria-hidden></i> cursor
up ↑ / down ↓ to next attribute / method.
- `<C-S-↑>` / `<C-S-↓>`: move selected attribute(s) and/or method(s) up ↑ /
down ↓.
- `<Alt-Ins>`: generate constructor, getter(s), setter(s), etc.
- `<Alt-Enter>`: open popup window to resolve syntax error(s).
- `<C-Enter>`: jump to definition.
- `<S-F6>`: refactor current variable/method/etc.
- `<S-F10>`: run program in the current configuration.
- `<Alt-S-F10>`: run current class's `main` method, and update the current
configuration.

Remarks: I don't add the shortcuts for debugging mode since it requires the use
of mouse for breakpoint insertion.

### <i class="far fa-file-code fa-fw" aria-hidden></i> Emmets

Shortcuts for code generation.

- `main`: `main` method
- `fori`: classical `for` loop over an integer index.
- `collection.iter`: `for` loop over `collection`

### <i class="fas fa-gear fa-fw" aria-hidden></i> Other IDE Configurations
#### gitignore

In `.gitignore`, I'm taught to use these three lines.

```
.idea/
out/
*.iml
```

#### Class import

In settings (`<C-Alt-S>`), type "Java" to edit some settings in **Editor → Code
Style → Java**:

- Class count to use import with `*`: 1000
- Names count to use static import with `*`: 1000

In **Packages to Use Import with `*`**, I've removed all packages.

#### shift Terminal focus

To disable escape from the Terminal by `<Esc>` (imagine you're using [Vim] for a
Git commit), you may carry out the following steps.

1. In settings, choose **Tools → Terminal** on the left side bar.
1. In the main area, under the **Application Settings** section, click
**Configure terminal keybindings**.
1. In the text field next to the
<i class="fas fa-magnifying-glass" aria-hidden></i> magnifying glass, type
"terminal".
1. On the right-hand side of the row **Switch Focus To Editor**, right-click
`<Esc>`.
1. In the popup menu, select the last item **Remove Escape**.

![IntelliJ settings for terminal][term_settings_fig]

#### Apache Log4J

Apache Log4J is a great debugging tool.  Here're the steps for its installation.

1. Go to [MVN Repository][mvnrepo].
1. Search "log4j".
1. Download **Apache Log4j Core** and **Apache Log4j API**.
    + Choose JAR file.
    + I've only tried version 2.17.2.
1. In the top menu, click **File → Project Structure** (`<C-Alt-S-S>`).
1. In the left side menu in the window, click '+' then **Java**.
1. Select the two JAR files downloaded from MVN Repository.
1. Create <i class="far fa-file-code" aria-hidden></i> `src/log4j2.xml`.  Other
filename doesn't work.  It can be anywhere in the app's classpath.

![add Apache log4j][apache_log4j_fig]

I was taught to copy and paste a Rolling File Appender similar to the
[one on How To Do In Java][log4j2.xml_sample].

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="warn">
	<Properties>
		<Property name="basePath">C:/temp/logs</Property>
	</Properties>

	<Appenders>
		<RollingFile name="fileLogger"
			fileName="${basePath}/app.log"
			filePattern="${basePath}/app-%d{yyyy-MM-dd}.log">
			<PatternLayout>
				<pattern>[%-5level] %d{yyyy-MM-dd HH:mm:ss.SSS} [%t] %c{1} - %msg%n
				</pattern>
			</PatternLayout>
			<Policies>
				<TimeBasedTriggeringPolicy interval="1" modulate="true" />
				<SizeBasedTriggeringPolicy size="10MB" />
			</Policies>
			<!-- Max 10 files will be created everyday -->
			<DefaultRolloverStrategy max="10">
				<Delete basePath="${basePathr}" maxDepth="10">
					<!-- Delete all files older than 30 days -->
					<IfLastModified age="30d" />
				</Delete>
			</DefaultRolloverStrategy>
		</RollingFile>
	</Appenders>
	<Loggers>
		<Root level="info" additivity="false">
			<appender-ref ref="fileLogger" />
		</Root>
	</Loggers>
</Configuration>
```

Once it's setup, it can be used by `logger.error(String errorMessage)`.

```java
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Main {

	private static final Logger logger = LogManager.getLogger(Main.class);

	public static void main(final String... args) 
	{
		logger.debug("Debug Message Logged !!!");
		logger.info("Info Message Logged !!!");
		logger.error("Error Message Logged !!!", new NullPointerException("NullError"));
	}
}
```

- The `logger` isn't a constant, since it's state can be changed.
- The `final` keyword forbids another instance of `Logger` from being affected
with `logger`.
- The `private` keyword forbids other classes from accessing `logger`.
- `Logger` follows the singleton pattern.

The `debug(String message)` is for development.

### <i class="far fa-rectangle-list" aria-hidden></i> References

1. [*Multiple cursors and selection ranges*][doc_mult]

[Vim]: https://www.vim.org
[doc_mult]: https://www.jetbrains.com/help/idea/multicursor.html#copy-and-paste-multiple-selection-ranges
[term_settings_fig]: terminal-settings.png
[apache_log4j_fig]: add-log4j.png
[mvnrepo]: https://mvnrepository.com/
[log4j2.xml_sample]: https://howtodoinjava.com/log4j2/log4j2-xml-configuration-example/
