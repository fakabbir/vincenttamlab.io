---
title: "Simple Document Templates"
subtitle: "MWE $\\LaTeX$ and pandoc examples"
date: 2023-03-22T12:32:10+01:00
type: page
toc: true
draft: false
---

### <i class="fa-regular fa-circle-play fa-lg"></i> To get started

1.  <i class="fa-solid fa-download fa-lg"></i>
<i class="fa-solid fa-screwdriver-wrench fa-lg"></i>
Download FOSS (free and open source) tools:

    -   <i class="fa-regular fa-file-code fa-lg"></i>
        <i class="fa-solid fa-superscript fa-lg"></i>
        [$\LaTeX$][latex]: a typesetting system often used by academians and
        researchers.  It gives documents with a more impressive appearance.

        -   <i class="fa-brands fa-windows fa-lg"></i>
            Windows: [Mik$\TeX$][miktex] → click **Download** in the navbar
        -   <i class="fa-brands fa-linux fa-lg"></i>
            Linux: install [$\TeX$ Live][texlive] directly from the TUG ($\TeX$
            User Group)
        -   <i class="fa-brands fa-apple fa-lg"></i>
            Mac OS: [Mac$\TeX$][mactex] (I've never used that.)

    -   <i class="fa-regular fa-file-code fa-lg"></i>
        <i class="fa-solid fa-rotate fa-lg"></i>
        <i class="fa-solid fa-file-code fa-lg"></i>
        [pandoc][pandoc]: a document markup language converter (e.g.
        <i class="fa-brands fa-markdown fa-lg"></i> ↔︎
        <i class="fa-brands fa-html5 fa-lg"></i>, $\LaTeX$ ↔︎
        <i class="fa-regular fa-file-word fa-lg"></i>,
        <i class="fa-brands fa-markdown fa-lg"></i> →
        <i class="fa-regular fa-file-pdf fa-lg"></i>)

        Click **Installing** in the navbar.

1. (Optional, recommended) <i class="fa-solid fa-download fa-lg"></i>
<img src="vscode-icon.svg" alt="VS Code icon" class="small-inline-icons">
<i class="fa-solid fa-plus fa-lg"></i>
<img src="pandoc-logo2.svg" alt="pandoc logo" class="small-inline-icons">
<i class="fa-solid fa-puzzle-piece fa-lg"></i>
Download [VS Code][vscode] and plugin [vscode-pandoc][vscode_pandoc].
1. <i class="fa-solid fa-laptop-code fa-lg"></i>
<i class="fa-solid fa-chalkboard-user fa-lg"></i>
While waiting for the download, you may start some basic tutorials.
    - <i class="fa-brands fa-markdown fa-lg"></i>
    <i class="fa-solid fa-code fa-lg"></i>
    Markdown: a markup language with simple syntax.  It allows you to focus on
    typing contents without having to leave the keyboard to do the styling.
    There're numerous online tutorials.  Here's
    [an example][sample_markdown_tutorial].
    - (optional) <i class="fa-solid fa-superscript fa-lg"></i>
    <i class="fa-solid fa-code fa-lg"></i>
    $\LaTeX$: a markup language for math equations.  Again, there're many online
    tutorials, for example
        - <i class="fa-brands fa-wikipedia-w fa-lg"></i>
        <i class="fa-solid fa-book fa-lg"></i>
        [WikiBook][wiki_latex_tut]'s $\LaTeX$ tutorial → *Mathematics*
        - <i class="fa-solid fa-leaf fa-lg"></i>
        <i class="fa-solid fa-book fa-lg"></i>
        [Overleaf][overleaf_latex_tut]'s $\LaTeX$ guide → *Mathematical
        expressions*
        - <i class="fa-solid fa-globe fa-lg"></i>
        <i class="fa-solid fa-pen-to-square fa-lg"></i>
        <i class="fa-solid fa-superscript fa-lg"></i>
        [$\KaTeX$ online editor][katex_editor] contains some samples and
        supports `mhchem`.
    - (optional) <i class="fa-solid fa-flask-vial fa-lg"></i>
    <i class="fa-solid fa-superscript fa-lg"></i>
    <i class="fa-solid fa-box-open fa-lg"></i>
    [$\LaTeX$ `mhchem` package][mhchem]: an extension for rendering chemical
    equations.
    - (optional) <i class="fa-solid fa-flask-vial fa-lg"></i>
    <i class="fa-solid fa-diagram-project fa-lg"></i>
    <i class="fa-solid fa-box-open fa-lg"></i>
    $\LaTeX$ `chemfig` package: an extension for rendering chemical figures.
    You may see [Overleaf's chemfig tutorial][chemfig_tut].
1. (Optional) <i class="fa-solid fa-download fa-lg"></i>
<i class="fa-solid fa-box-open fa-lg"></i>
Download additional $\LaTeX$ packages if necessary.
    - (Recommended) <i class="fa-solid fa-superscript fa-lg"></i>
    Download `amsmath` in your $\LaTeX$ package manager.
    - <i class="fa-solid fa-flask-vial fa-lg"></i>
    <i class="fa-solid fa-superscript fa-lg"></i>
    Download `mhchem` in your $\LaTeX$ package manager.

        Here's the procedure on MikTeX Console.
        <i class="fa-brands fa-windows fa-lg"></i>

        ![MikTeX console](miktex-console.png)

    - <i class="fa-solid fa-flask-vial fa-lg"></i>
    <i class="fa-solid fa-diagram-project fa-lg"></i>
    Download `chemfig` in your $\LaTeX$ package manager.

<i class="fa-solid fa-info fg-lg"></i> Remarks:
Even though $\TeX$Live supports Windows, but Mik$\TeX$'s installation size is
much smaller (so that the installation time is much shorter), and it supports
on-the-fly installation for missing packages.

### <i class="fa-regular fa-pen-to-square fa-lg"></i> Articles
#### <i class="fa-regular fa-lightbulb fa-lg"></i> Basics

1. <i class="fa-solid fa-scroll fa-lg"></i>
<i class="fa-brands fa-markdown fa-lg"></i>
<i class="fa-regular fa-file-code fa-lg"></i>
<i class="fa-regular fa-floppy-disk fa-lg"></i>
<i class="fa-solid fa-arrow-right fa-lg"></i>
<i class="fa-solid fa-hard-drive fa-lg"></i>
Save the Markdown source file using a text editor (say, Notepad).
1. <i class="fa-brands fa-markdown fa-lg"></i>
<i class="fa-regular fa-file-code fa-lg"></i>
<img src="pandoc-logo2.svg" alt="pandoc icon" class="small-inline-icons">
<i class="fa-solid fa-arrow-right fa-lg"></i>
<i class="fa-regular fa-file-pdf fa-lg"></i> /
<i class="fa-regular fa-file-word fa-lg"></i>
Compile the source file using pandoc.
1. <i class="fa-regular fa-eye fa-lg"></i>
<i class="fa-regular fa-file-lines fa-lg"></i>
View the compiled document using a suitable document viewer.

<i class="fa-brands fa-markdown fa-lg"></i>
<i class="fa-regular fa-file-code fa-lg"></i>
<i class="fa-solid fa-diagram-project fa-lg"></i>
Basic structure of the Markdown source file:

1. (Optional) <i class="fa-solid fa-heading fa-lg"></i> Header
1. <i class="fa-regular fa-user fa-lg"></i> Body

<i class="fa-brands fa-markdown fa-lg"></i>
<i class="fa-regular fa-file-code fa-lg"></i>
<i class="fa-regular fa-floppy-disk fa-lg"></i>
<i class="fa-solid fa-arrow-right fa-lg"></i>
<i class="fa-solid fa-hard-drive fa-lg"></i>
Markdown source file to be saved.

~~~~
---
title: Sample document title
author: Vincent Tam
date: 23rd March, 2023
---

Document body goes below the YAML header.
~~~~

[<i class="fa-regular fa-file-pdf fa-lg"></i> Click to view PDF in new tab][pdf_mwe].

<div class="embed-responsive small-pdf-embed-container">
  <object type="application/pdf" data="mwe.pdf#toolbar=0"
    class="embed-responsive-item"></object>
</div>

#### <i class="fa-solid fa-e fa-lg"></i> <i class="fa-solid fa-n fa-lg"></i> <i class="fa-solid fa-scroll fa-lg"></i> Simplest example: English articles

I've chosen VS Code as the text editor for easier compilation.

1.  <img src="vscode-icon.svg" alt="VS Code icon" class="small-inline-icons">
    <i class="fa-brands fa-markdown fa-lg"></i>
    <i class="fa-regular fa-file-code fa-lg"></i>
    <i class="fa-regular fa-floppy-disk fa-lg"></i>
    <i class="fa-solid fa-arrow-right fa-lg"></i>
    <i class="fa-solid fa-hard-drive fa-lg"></i>
    Open VS Code and save the file (using `<Ctrl>+S`).

    ~~~~
    ---
    title: Sample Document Title
    author: John Doe
    date: 23rd March, 2023
    ---

    # Section One

    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut aliquam purus sit amet luctus
    venenatis lectus magna. Dictum non consectetur a erat nam at.

    Scelerisque viverra mauris in aliquam sem fringilla ut. Mi quis hendrerit dolor
    magna eget est lorem ipsum. Ullamcorper malesuada proin libero nunc consequat
    interdum varius sit. Ultricies mi quis hendrerit dolor magna eget est.

    # Section Two

    Aliquam sem et tortor consequat. Nisl purus in mollis nunc sed id semper risus
    in. At risus viverra adipiscing at in tellus. Etiam sit amet nisl purus. Lacus
    suspendisse faucibus interdum posuere lorem ipsum dolor sit.

    | left | center | right |
    | :--- | :---: | ---: |
    | first cell | second cell | third cell |
    | abc | def | ghi |

    1. first item
    1. second item

    - item A
    - item B

    > Direct quote: Sit amet tellus cras adipiscing enim eu. Nisl nunc mi ipsum
    > faucibus vitae aliquet. Amet nisl purus in mollis nunc sed id semper.
    > Elementum nisi quis eleifend quam adipiscing vitae. Duis convallis convallis
    > tellus id interdum velit laoreet id donec.
    ~~~~

1.  <i class="fa-solid fa-puzzle-piece fa-lg"></i>
    <i class="fa-brands fa-markdown fa-lg"></i>
    <i class="fa-regular fa-file-code fa-lg"></i>
    <i class="fa-solid fa-arrow-right fa-lg"></i>
    <i class="fa-regular fa-file-pdf fa-lg"></i> /
    <i class="fa-regular fa-file-word fa-lg"></i>
    Compile using plugin [vscode-pandoc][vscode_pandoc].

    1. Press `<F1>` key, then type "pandoc".

        {{< beautifulfigure src="vscode-plugin.png"
        alt="vscode-pandoc screenshot" >}}

    1. Select "pdf"/"docx" either with up ↑/down ↓ or by mouse click.

        {{< beautifulfigure src="vscode-plugin2.png"
        alt="vscode-pandoc screenshot" >}}

1. <i class="fa-regular fa-eye fa-lg"></i>
<i class="fa-regular fa-file-lines fa-lg"></i>
Output files:
    - [<i class="fa-regular fa-file-pdf fa-lg"></i> Output PDF file (click to open in new tab)][pdf_eng]

        <div class="embed-responsive embed-responsive-16by9">
          <object type="application/pdf" data="eng-article.pdf#toolbar=1"
            class="embed-responsive-item"></object>
        </div>

    - [<i class="fa-solid fa-download fa-lg"></i> <i class="fa-regular fa-file-word fa-lg"></i> Downlaod output DOCX file][word_eng]

        <div class="embed-responsive embed-responsive-16by9">
          <object
            data="https://view.officeapps.live.com/op/embed.aspx?src=https://vincenttam.gitlab.io/page/simple-document-templates/eng-article.docx"
            class="embed-responsive-item"></object>
        </div>

    - [<i class="fa-brands fa-html5 fa-lg"></i> <i class="fa-regular fa-file-code fa-lg"></i> Output HTML file (click to open in new tab)][html_eng]

        <div class="embed-responsive embed-responsive-16by9">
          <iframe src="eng-article.html" class="embed-responsive-item"></iframe>
        </div>

#### <i class="fa-solid fa-language fa-lg"></i> <i class="fa-solid fa-scroll fa-lg"></i> Improved example: A4 multilingual articles (without CJK)

1. <i class="fa-solid fa-gear fa-lg"></i> Follow the instructions in
[*Setting additional pandoc options*][vscode_pd_settings] to set the value of
the option `pandoc.pdfOptString` to `--pdf-engine=lualatex`, so that the
document can contain characters like 'é', 'ç', 'à', etc.
1. <i class="fa-brands fa-markdown fa-lg"></i>
<i class="fa-regular fa-file-code fa-lg"></i>
<i class="fa-regular fa-floppy-disk fa-lg"></i>
<i class="fa-solid fa-arrow-right fa-lg"></i>
<i class="fa-solid fa-hard-drive fa-lg"></i>
Save the follow Markdown source code to VS Code.

    ~~~~
    ---
    documentclass: article
    geometry: margin=1in
    papersize: a4
    fontsize: 12pt
    mainfont: Times New Roman
    title: Better Article Sample
    author: John Doe
    date: 23rd March, 2023
    ---

    # Section Ωαβ

    Lôrém ipsùm dolor sit amet, çonsectetur adipiscing elit, sed do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut aliquam purus sit amet luctus
    venenatis lectus magna. Dictum non consectetur a erat nam at.

    Scelerisque viverra mauris in aliquam sem fringilla ut. Mi quis hendrerit dolor
    magna eget est lorem ipsum. Ullamcorper malesuada proin libero nunc consequat
    interdum varius sit. Ultricies mi quis hendrerit dolor magna eget est.

    # Section Two

    Aliquam sem et tortor consequat. Nisl purus in mollis nunc sed id semper risus
    in. At risus viverra adipiscing at in tellus. Etiam sit amet nisl purus. Lacus
    suspendisse faucibus interdum posuere lorem ipsum dolor sit.

    | left | center | right |
    | :--- | :---: | ---: |
    | first cell | second cell | third cell |
    | abc | def | ghi |

    1. first item
    1. second item

    - item A
    - item B

    > Direct quote: Sit amet tellus cras adipiscing enim eu. Nisl nunc mi ipsum
    > faucibus vitae aliquet. Amet nisl purus in mollis nunc sed id semper.
    > Elementum nisi quis eleifend quam adipiscing vitae. Duis convallis convallis
    > tellus id interdum velit laoreet id donec.
    ~~~~

1. <i class="fa-solid fa-puzzle-piece fa-lg"></i>
<i class="fa-brands fa-markdown fa-lg"></i>
<i class="fa-regular fa-file-code fa-lg"></i>
<i class="fa-solid fa-arrow-right fa-lg"></i>
<i class="fa-regular fa-file-pdf fa-lg"></i> /
<i class="fa-regular fa-file-word fa-lg"></i>
Compile using plugin [vscode-pandoc][vscode_pandoc].
1. <i class="fa-regular fa-eye fa-lg"></i>
<i class="fa-regular fa-file-lines fa-lg"></i>
Output files:
    - [<i class="fa-regular fa-file-pdf fa-lg"></i> Output PDF file (click to open in new tab)][pdf_mult]

        <div class="embed-responsive embed-responsive-16by9">
          <object type="application/pdf" data="mult-article.pdf#toolbar=1"
            class="embed-responsive-item"></object>
        </div>

    - [<i class="fa-solid fa-download fa-lg"></i> <i class="fa-regular fa-file-word fa-lg"></i> Downlaod output DOCX file][word_mult]

        <div class="embed-responsive embed-responsive-16by9">
          <object
            data="https://view.officeapps.live.com/op/embed.aspx?src=https://vincenttam.gitlab.io/page/simple-document-templates/mult-article.docx"
            class="embed-responsive-item"></object>
        </div>

    - [<i class="fa-brands fa-html5 fa-lg"></i> <i class="fa-regular fa-file-code fa-lg"></i> Output HTML file (click to open in new tab)][html_mult]

        <div class="embed-responsive embed-responsive-16by9">
          <iframe src="mult-article.html"
            class="embed-responsive-item"></iframe>
        </div>

<i class="fa-brands fa-windows fa-lg"></i>
<i class="fa-solid fa-font fa-lg"></i>
We changed the `mainfont` to `Times New Roman` for Windows.  In fact, it can be
any font installed on the OS.  (You may find them out by hitting the
<i class="fa-brands fa-windows fa-lg"></i> key, then type "font", and hit
<i class="fa-solid fa-font fa-lg"></i> **Fonts**.)  The default version of
[Computer Modern][cm_font] is a bitmapped font.  You may see the printed sample
of this font at the end of the section <i class="fa-solid fa-cat fa-lg"></i>
<i class="fa-regular fa-note-sticky fa-lg"></i> [*Cheatsheets*](#cheatsheets).

#### <i class="fa-solid fa-language fa-lg"></i> <i class="fa-solid fa-scroll fa-lg"></i> CJK articles

1. <i class="fa-solid fa-download fa-lg"></i>
<i class="fa-solid fa-font fa-lg"></i>
<i class="fa-solid fa-box-open fa-lg"></i>
Download an additional $\LaTeX$ package for CJK fonts, say [noto][ctan_noto].
1. <i class="fa-brands fa-markdown fa-lg"></i>
<i class="fa-regular fa-file-code fa-lg"></i>
<i class="fa-regular fa-floppy-disk fa-lg"></i>
<i class="fa-solid fa-arrow-right fa-lg"></i>
<i class="fa-solid fa-hard-drive fa-lg"></i>
Save the follow Markdown source code to VS Code.

    ~~~~
    ---
    documentclass: article
    geometry: margin=1in
    papersize: a4
    fontsize: 12pt
    mainfont: Times New Roman
    CJKmainfont: Noto Sans CJK SC
    title: CJK Article Sample
    author: John Doe
    date: 23rd March, 2023
    header-includes: |
      \ltjsetparameter{jacharrange={-2}}
    ---

    # Section Ωαβ

    Lôrém ipsùm dolor sit amet, çonsectetur adipiscing elit, sed do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut aliquam purus sit amet luctus
    venenatis lectus magna. Dictum non consectetur a erat nam at.

    - 繁體中文
    - 简体中文
    - こんにちは
    - 조선글

    # Section Two

    Aliquam sem et tortor consequat. Nisl purus in mollis nunc sed id semper risus
    in. At risus viverra adipiscing at in tellus. Etiam sit amet nisl purus. Lacus
    suspendisse faucibus interdum posuere lorem ipsum dolor sit.

    | left | center | right |
    | :--- | :---: | ---: |
    | first cell | second cell | third cell |
    | abc | def | ghi |

    1. first item
    1. second item

    - item A
    - item B

    > Direct quote: Sit amet tellus cras adipiscing enim eu. Nisl nunc mi ipsum
    > faucibus vitae aliquet. Amet nisl purus in mollis nunc sed id semper.
    > Elementum nisi quis eleifend quam adipiscing vitae. Duis convallis convallis
    > tellus id interdum velit laoreet id donec.
    ~~~~

1. <i class="fa-solid fa-puzzle-piece fa-lg"></i>
<i class="fa-brands fa-markdown fa-lg"></i>
<i class="fa-regular fa-file-code fa-lg"></i>
<i class="fa-solid fa-arrow-right fa-lg"></i>
<i class="fa-regular fa-file-pdf fa-lg"></i> /
<i class="fa-regular fa-file-word fa-lg"></i>
Compile using plugin [vscode-pandoc][vscode_pandoc].
1. <i class="fa-regular fa-eye fa-lg"></i>
<i class="fa-regular fa-file-lines fa-lg"></i>
Output files:
    - [<i class="fa-regular fa-file-pdf fa-lg"></i> Output PDF file (click to open in new tab)][pdf_cjk]

        <div class="embed-responsive embed-responsive-16by9">
          <object type="application/pdf" data="cjk-article.pdf#toolbar=1"
            class="embed-responsive-item"></object>
        </div>

    - [<i class="fa-solid fa-download fa-lg"></i> <i class="fa-regular fa-file-word fa-lg"></i> Downlaod output DOCX file][word_cjk]

        <div class="embed-responsive embed-responsive-16by9">
          <object
            data="https://view.officeapps.live.com/op/embed.aspx?src=https://vincenttam.gitlab.io/page/simple-document-templates/cjk-article.docx"
            class="embed-responsive-item"></object>
        </div>

    - [<i class="fa-brands fa-html5 fa-lg"></i> <i class="fa-regular fa-file-code fa-lg"></i> Output HTML file (click to open in new tab)][html_cjk]

        <div class="embed-responsive embed-responsive-16by9">
          <iframe src="cjk-article.html"
            class="embed-responsive-item"></iframe>
        </div>

<i class="fa-brands fa-windows fa-lg"></i>
<i class="fa-solid fa-font fa-lg"></i>
We added the `CJKfont: Noto Sans CJK SC` in the header.  You may check the
<i class="fa-regular fa-folder fa-lg"></i> folder
`C:\Users\[your_user_name]\AppData\Local\Programs\MiKTeX\fonts\` (or `fonts/`
under your Mik$\TeX$'s path).

```
$ ls ~/AppData/Local/Programs/MiKTeX/fonts/opentype/public/notocjksc/
NotoSansCJKsc-Black.otf      NotoSansMonoCJKsc-Regular.otf
NotoSansCJKsc-Bold.otf       NotoSerifCJKsc-Black.otf
NotoSansCJKsc-DemiLight.otf  NotoSerifCJKsc-Bold.otf
NotoSansCJKsc-Light.otf      NotoSerifCJKsc-ExtraLight.otf
NotoSansCJKsc-Medium.otf     NotoSerifCJKsc-Light.otf
NotoSansCJKsc-Regular.otf    NotoSerifCJKsc-Medium.otf
NotoSansCJKsc-Thin.otf       NotoSerifCJKsc-Regular.otf
NotoSansMonoCJKsc-Bold.otf   NotoSerifCJKsc-SemiBold.otf
```

<i class="fa-solid fa-info fg-lg"></i> Remarks:
The following two lines come from [my recent $\TeX$.SE question][tex682222].

```
header-includes: |
  \ltjsetparameter{jacharrange={-2}}
```

### <i class="fa-solid fa-cat fa-lg"></i> <i class="fa-regular fa-note-sticky fa-lg"></i> Cheatsheets
Here's a sample template with

- A4 paper (210mm × 297mm)
- font: Times New Roman (also in math mode)
- font size: 10pt
- margin: 0.5cm
- line spacing reduced by 0.25×

1. <i class="fa-brands fa-markdown fa-lg"></i>
<i class="fa-regular fa-file-code fa-lg"></i>
<i class="fa-regular fa-floppy-disk fa-lg"></i>
<i class="fa-solid fa-arrow-right fa-lg"></i>
<i class="fa-solid fa-hard-drive fa-lg"></i>
save the source file using the name `test.md` in any text editor (say Notepad).
You may replace `test` to any name you like.  The extension name `.md` stands
for Markdown.

    ~~~~
    ---
    fontsize: 10pt
    geometry: a4paper, margin=.5cm
    mainfont: Times New Roman
    header-includes:
    - |
      ```{=latex}
      \usepackage{fontspec}
      \usepackage{amsmath}
      \usepackage{mhchem}
      \usepackage{chemfig}
      \usepackage{setspace}
      \renewcommand{\baselinestretch}{.75}
      ```
    ---

    # Section

    Lorem ipsum dolor sit amet, **consectetur adipiscing elit**, sed do *eiusmod*
    tempor incididunt ut labore et dolore magna aliqua. Parturient montes nascetur
    ridiculus mus mauris.

    Diam éµçà enim lobortis scelerisque. Cursus metus aliquam eleifend mi. Eget
    lorem dolor sed viverra ipsum nunc aliquet bibendum. Dui vivamus arcu felis
    bibendum ut tristique. Leo integer malesuada nunc vel risus commodo.


    1. \ce{Hg^2+ ->[I-] HgI2 ->[I-][text below] [Hg^{II}I4]^2-}
    1. \ce{Zn^2+ 
        <=>[+ 2OH-][+ 2H+]
        $\underset{\text{amphoteres Hydroxid}}{\ce{Zn(OH)2 v}}$
        <=>[+ 2OH-][+ 2H+]
        $\underset{\text{Hydroxozikat}}{\ce{[Zn(OH)4]^2-}}$
      }

    # Chemical figures

    See chemfig package for more info.

    | left | right | center |
    | :--- | ---: | :---: |
    | v shape | quick fox | \chemfig{A-[7]B-[1]C} |
    | heptagon | number one two | \chemfig{A*7(-B=C~D>E<F>:G<:)}
    ~~~~

    {{< beautifulfigure src="save.gif"
    title="Save text file to Notepad"
    caption="GIF prepared by (GPL-licensed) LICECap"
    alt="save text file notepad" >}}

1. <span class="fa-stack fa-xs"><i class="far fa-square fa-stack-2x"></i>
<i class="fa fa-terminal fa-stack-1x fa-sm"></i></span>
Open the terminal.
    - <i class="fa-brands fa-windows fa-lg"></i>
    Windows: type <i class="fa-brands fa-windows fa-lg"></i> key, then input
    "cmd" and choose **Command Prompt**.
    - <i class="fa-brands fa-linux fa-lg"></i>
    Linux: press and hold three keys in the sequence `<Ctrl>`, `<Alt>` and `t`,
    then release them.
    - <i class="fa-brands fa-apple fa-lg"></i>
    Mac OS: press `<Ctrl><Space>` and type "terminal" and hit return.
1. <span class="fa-stack fa-xs"><i class="far fa-square fa-stack-2x"></i>
<i class="fa fa-terminal fa-stack-1x fa-sm"></i></span>
<i class="fa-regular fa-folder-open"></i>
**C**hange to the folder/**d**irectory containing the above source file
<i class="fa-brands fa-markdown fa-lg"></i>
<i class="fa-regular fa-file-code fa-lg"></i> by using `cd [target_directory]`,
say

    ```sh
    cd Documents
    ```

    If the path is too long, a more practical way would be to open the file
    explorer and drag the target folder onto the terminal.

    {{< beautifulfigure src="drag2cmd.gif"
    title="Drag folder to Command Prompt"
    caption="GIF prepared by (GPL-licensed) LICECap"
    alt="drag folder to command prompt" >}}

1. <i class="fa-regular fa-keyboard fa-lg"></i> Type this command in the
terminal, then hit enter to run the command.

    ```sh
    pandoc test.md -o test.pdf
    ```

1. [<i class="fa-regular fa-file-pdf fa-lg"></i> Compiled PDF `test.pdf` (click
to open in new tab)][pdf_tmr]

    <div class="embed-responsive embed-responsive-16by9">
      <object type="application/pdf" data="cheatsheet-tmr.pdf#toolbar=1"
        class="embed-responsive-item"></object>
    </div>

Since it's a cheatsheet, one might want a condensed/narrow font, so that one can
put more info inside the document.

This template allows using any system font (found on Windows by typing the
<i class="fa-brands fa-windows fa-lg"></i> key then input "fonts").

{{< beautifulfigure src="lualatex-sys-font.png"
title="LuaLaTeX allows using system fonts"
caption="pandoc generated PDF with Blackadder ITC font"
alt="pandoc generated PDF with Blackadder ITC font" >}}

<div class="embed-responsive embed-responsive-16by9">
  <object type="application/pdf" data="cheatsheet-blackadder.pdf#toolbar=1"
    class="embed-responsive-item"></object>
</div>

[Click here to view <i class="fa-regular fa-file-pdf fa-lg"></i> in new tab][pdf_blackadder]

I've chosen `fontspec` (compiled with Lua$\LaTeX$) over `fontenc` (compiled with
PDF$\LaTeX$) since Lua$\LaTeX$ and Xe$\LaTeX$ allow direct input of characters
like 'é', 'δ', etc.

I've changed the default font (the default version of
[Computer Modern][cm_font]) due to its poor quality when rescaled and printed on
paper.

![Computer Modern font printed](cmr-printed.jpg)

The above photo recalled a webpage that I read a decade ago:
[*Doing LaTeX Right*][cm_pblm].  The cause of such imperfection is that the
default font is a bitmapped font.

### <i class="fab fa-slideshare fa-lg"></i> Slides
Markdown source code file structure

~~~~
% Title
% Author
% Date

# Slide 1 Title

- point 1
- point 2

# Slide 2 Title

- point 1
- point 2
- etc
~~~~

- The official demos ([demo source code][demo_src]) are the simplest.
[pandoc][pandoc] provides several themes for HTML slides.  Slidy is my favorite.

    + <i class="fa-brands fa-html5 fa-lg"></i>
    <i class="fab fa-slideshare fa-lg"></i>
    HTML5 Slides

        <div class="embed-responsive embed-responsive-16by9">
          <iframe src="https://pandoc.org/demo/example16b.html"
            class="embed-responsive-item"></iframe>
        </div>

    + <i class="fa-regular fa-file-powerpoint fa-lg"></i>
    <i class="fab fa-slideshare fa-lg"></i>
    M\$ PowerPoint Slides

        Unluckily, in the [offical demo page][pandoc_demo], there's no link to
        the PowerPoint file.

        [<i class="fa-solid fa-download fa-lg"></i> <i class="fa-regular fa-file-powerpoint fa-lg"></i> Downlaod output PPTX file][demo_pptx]

        <div class="embed-responsive embed-responsive-16by9">
          <object
            data="https://view.officeapps.live.com/op/embed.aspx?src=https://vincenttam.gitlab.io/page/simple-document-templates/demo.pptx"
            class="embed-responsive-item"></object>
        </div>

    + <i class="fa-regular fa-file-pdf fa-lg"></i>
    <i class="fab fa-slideshare fa-lg"></i>
    $\LaTeX$ Beamer Slides

        I embed the beamer slide show referenced in the offical demo page.

        [<i class="fa-regular fa-file-pdf fa-lg"></i> Output PDF file (click to open in new tab)][demo_pdf]

        <div class="embed-responsive embed-responsive-16by9">
          <object type="application/pdf"
            data="https://pandoc.org/demo/example8.pdf#toolbar=1&navpanes=0&view=fit&pagemode=none"
            class="embed-responsive-item"></object>
        </div>

- Here's [the Markdown source for my simplest example slides][exo_3e_jour_src]
(in French).

    <div class="embed-responsive embed-responsive-16by9">
      <iframe src="/exo-3e-jour/index.html"
        class="embed-responsive-item"></iframe>
    </div>

- To showcase the potential of HTML slides (to incorportate custom CSS and/or
JavaScript to provide additional interactivity to the slides), I've publised my
HTML slides template with custom header with
[its Markdown source code][note_2ej_src].

    <div class="embed-responsive embed-responsive-16by9">
      <iframe src="/html-slides-exemplaire/index-en.html"
        class="embed-responsive-item"></iframe>
    </div>

[latex]: https://www.latex-project.org/
[miktex]: https://miktex.org/
[texlive]: https://tug.org/texlive/
[mactex]: https://www.tug.org/mactex/
[vscode]: https://code.visualstudio.com/
[sample_markdown_tutorial]: https://www.markdowntutorial.com/
[wiki_latex_tut]: https://en.wikibooks.org/wiki/LaTeX/Mathematics
[overleaf_latex_tut]: https://www.overleaf.com/learn/latex/Mathematical_expressions
[pandoc]: https://pandoc.org/
[katex_editor]: https://sixthform.info/katex/examples/demo.html
[mhchem]: https://mhchem.github.io/MathJax-mhchem/
[chemfig_tut]: https://www.overleaf.com/learn/latex/Chemistry_formulae
[pdf_mwe]: mwe.pdf
[vscode_pandoc]: https://github.com/dfinke/vscode-pandoc#vscode-pandoc
[pdf_eng]: eng-article.pdf
[word_eng]: eng-article.docx
[html_eng]: eng-article.html
[pdf_mult]: mult-article.pdf
[word_mult]: mult-article.docx
[html_mult]: mult-article.html
[pdf_cjk]: cjk-article.pdf
[word_cjk]: cjk-article.docx
[html_cjk]: cjk-article.html
[tex682222]: https://tex.stackexchange.com/q/682222/126386
[vscode_pd_settings]: https://github.com/dfinke/vscode-pandoc#setting-additional-pandoc-options
[pdf_tmr]: cheatsheet-tmr.pdf
[pdf_blackadder]: cheatsheet-blackadder.pdf
[cm_font]: https://tug.org/FontCatalogue/computermodern/
[cm_pblm]: https://cs.gmu.edu/~sean/stuff/DoingLatexRight/
[ctan_noto]: https://ctan.org/pkg/noto
[demo_src]: https://pandoc.org/demo/SLIDES
[pandoc_demo]: https://pandoc.org/demos.html
[demo_pptx]: demo.pptx
[demo_pdf]: https://pandoc.org/demo/example8.pdf
[exo_3e_jour_src]: https://gitlab.com/VincentTam/exo-3e-jour/-/blob/master/public/index.md
[note_2ej_src]: https://gitlab.com/VincentTam/html-slides-exemplaire/-/blob/master/public/index-en.md
