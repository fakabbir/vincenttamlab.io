---
title: "Visual Studio Code"
subtitle: "An overview of my VSCode keyboard shortcuts"
date: 2023-04-12T22:51:53+02:00
type: page
toc: true
draft: false
---

### <i class="far fa-keyboard fa-fw" aria-hidden></i> Usual keys

I'll only list those I find useful.

#### <i class="fas fa-arrows-alt fa-fw" aria-hidden></i> Navigation

- `<C-Up>` / `<C-Down>`: move up ↑ / down ↓ _without_ changing
<i class="fas fa-i-cursor" aria-hidden></i> cursor position.
- `<C-g>`: go to line number.
- `<C-p>`: go to other files.
- `<C-S-p>`: show [Command Palette][vscsp].
- `<C-b>`: toggle left side bar.
- `<C-j>`: toggle Terminal.

#### <i class="fas fa-search fa-fw" aria-hidden></i> Searching / Replacing

As usual, it supports custom selection range and regular expressions as
expected.  Transformations preserving cases are possible.

- `<F3>`: jump to next matching word.  It also opens the search dialog box with
the previously used search pattern if the search dialog box is not present.
- `<S-F3>`: jump to previous matching word.  It also opens the search dialog box
with the previously used search pattern if the search dialog box is not present.

#### <i class="far fa-edit fa-fw" aria-hidden></i> Editing / Selection

- `<C-Enter>` / `<C-S-Enter>`: insert empty new line below / above current line.
It works like `o` / `O` in [Vim].
- `<Alt-Up>` / `<Alt-Down>`: swap selected lines (or current line) up ↑ / down
↓.
- `<Alt-S-Up>` / `<Alt-S-Down>`: duplicate selected lines (or current line) up ↑
/ down ↓.
- `<C-x>`: cut current line.
- `<C-l>`: select current line and move cursor to the beginning of next line.
- `<C-[>` / `<C-]>`: unindent / indent current line, like `<<` / `>>` in [Vim].
- `<C-/>`: toggle comment.

Multi-cursors:

- `<C-d>`: jump to next occurence of selected content, or select current word.
- `<C-Alt-Up>` / `<C-Alt-Down>`: add caret above / below.

### <i class="fas fa-user-cog fa-fw" aria-hidden></i> User config
#### <i class="fas fa-palette fa-fw" aria-hidden></i> Theme

I've switched to **Solarized Dark Theme** because I like Solarized color scheme.
This can be done by

1. type `<C-S-p>`.
1. input **theme** in the popup text field
1. choose **Preferences: Color Theme**.
1. select your favorite plugin.

### <i class="fas fa-puzzle-piece fa-fw" aria-hidden></i> Plugins

Type [`code --list-extensions`][cmd_list_code] to view the plugin list.

#### <i class="fab fa-markdown fa-fw" aria-hidden></i> vscode-pandoc

For detailed explanation, see [my *Simple Document Templates*][doc_templates].

- `<C-k>` + `p`: choose output file format.

[vscsp]: https://code.visualstudio.com/api/ux-guidelines/command-palette
[Vim]: https://www.vim.org
[cmd_list_code]: /page/bash-commands/#code
[doc_templates]: /page/simple-document-templates/
