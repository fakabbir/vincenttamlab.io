---
title: "TLP Prevents Login on Ubuntu 18.04"
date: 2018-08-15T11:12:59+02:00
type: post
categories:
- technical support
tags:
- Linux
---

It's FOSS's [after-installation guide][1] #11 recommends [TLP][2].  However,
this package stopped my laptop from shutting down and logging in.  I resorted
to remove it through TTY.

[1]: https://itsfoss.com/things-to-do-after-installing-ubuntu-18-04/
[2]: https://linrunner.de/en/tlp/tlp.html
