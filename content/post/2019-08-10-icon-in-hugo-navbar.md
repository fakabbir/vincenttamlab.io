---
title: "Icon in Hugo Navbar"
date: 2019-08-10T14:53:00+02:00
categories:
- blogging
tags:
- Hugo
- icons
- navbar
draft: false
---

### Motivation

The original navbar in words took up half of the horizontal spaces of the
viewport.  I _couldn't_ add new items like "**Tags**" to display the tag list.

### Work

Thanks to the `pre` field in [Hugo Menus][menu], one can easily add the icon
as an HTML element by [some editing on the template file][commit].

```go-html
{{ range .Site.Menus.main.ByWeight }}
  {{ if .HasChildren }}
    <li class="navlinks-container">
      {{ if .Pre }}
        <a class="navlinks-parent" title="{{ .Name }}">{{ .Pre }}</a>
      {{ else }}
        <a class="navlinks-parent">{{ .Name }}</a>
      {{ end }}
      <div class="navlinks-children">
        {{ range .Children }}
          {{ if .Pre }}
            <a title="{{ .Name }}" href="{{ .URL | relLangURL }}">{{ .Pre }}</a>
          {{ else }}
            <a href="{{ .URL  | relLangURL }}">{{ .Name }}</a>
          {{ end }}
        {{ end }}
      </div>
    </li>
  {{ else }}
    <li>
      {{ if .Pre }}
        <a title="{{ .Name }}" href="{{ .URL | relLangURL }}">{{ .Pre }}</a>
      {{ else }}
        <a href="{{ .URL | relLangURL }}">{{ .Name }}</a>
      {{ end }}
    </li>
  {{ end }}
{{ end }}
```

[menu]: https://gohugo.io/content-management/menus/
[commit]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/commit/0083a60e28d896ac7560bc7b9ceb6f44deb77825#6fbe99d7b7ed353c112a4a34023b89b2cc76230b
