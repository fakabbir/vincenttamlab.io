---
title: "Ubuntu 18.04 Installation on Fujitsu AH557"
subtitle: "Ubuntu installation on hybrid system"
date: 2018-08-18T18:39:42+02:00
type: post
categories:
- technical support
tags:
- Fujitsu Lifebook
- Linux
---

### Objective

To get a hands-on experience on Ubuntu installation on a UEFI hybrid system.

### Devices

- 120 GB SSD disk: `/dev/sda`
- 1 TB HDD disk: `/dev/sdb`
- 8 GB USB key: `/dev/sdc`

### Stage 0: precautions

- Ensure that your machine <i class="fa fa-laptop" aria-hidden></i> has enough
  power.
- **Disable Secure Boot in M$ Windows**
- **Disable Quick Boot in boot menu**

### Stage 1: Make your live USB key

I tried Xubuntu's built-in disk writing tool to create a live <i
class="fab fa-linux" aria-hidden></i> USB key from the [official ISO file][1].
Like [my Xubuntu 18.04 installation][2] on Fujitsu LH532 <i class="fa fa-laptop"
aria-hidden></i>, the installer *crashed* during the installation.  After
submitting the generated report to Launchpad, I found out that the cause for
this crash: the *integrity test for my live <i class="fab fa-linux" aria-hidden>
</i> USB key had failed*.

Thanks to [UNetBootin][10], I created a live <i class="fab fa-linux" aria-hidden>
</i> which passed that test.

<i class="fa fa-info-circle" aria-hidden></i> Moral of the story: **verify a <i
class="fab fa-linux" aria-hidden></i> live USB drive before installing <i
class="fab fa-linux" aria-hidden></i> OS**.

### Stage 2: Try a live session before installation

I'm sorry for the blurred photos showing the installer.  I tried using
[G'MIC][3]'s octave sharpening in [GIMP][4], after shrinking them to 800 pixels.
However, the results *didn't* look good.  I found keeping media files *under 500
kB difficult* for an article with a lot of photos.

{{< beautifulfigure src="180729-install-sda.jpg" title="SSD partition table" caption="Partitioned a Fujitsu 120GB SSD drive" alt="SSD partition for Ubuntu install" >}}

Partition   | Type | Mount point | Approximate Size
------------|------|-------------|------------------
free space  |      |             |   1 MB
`/dev/sda1` | efi  |             | 500 MB
`/dev/sda2` | ext4 | `/`         |  30 GB
free space  |      |             | rest of the disk

Device for boot loader installation: `/dev/sda`

{{< beautifulfigure src="180729-install-sdb.jpg" title="HDD partition table" caption="Partitioned a Fujitsu 1TB HDD drive" alt="HDD partition for Ubuntu install" >}}

Partition   | Type | Mount point | Approximate Size
------------|------|-------------|------------------
free space  |      |             |   1 MB
`/dev/sdb1` | ext4 | `/home`     |  20 GB
free space  |      |             | rest of the disk
`/dev/sdb4` | ext4 | `/tmp`      |   4 GB
`/dev/sdb3` | ext4 | `/var`      |   4 GB
`/dev/sdb2` | swap |             |  16 GB

The basic principle of this partitioning scheme is to place files neccessary for
system startup into the SSD, and to keep frequently written files (e.g. system
logs, temporary files, etc) and the home folder in the HDD.  Regarding the swap,
it's twice the size of the RAM and I placed it at the end of the HDD.  (In fact,
the position of the swap *doesn't* matter according to an
[Ask Ubuntu question][8].)

<i class="fa fa-info"></i> The 1 MB free space at the beginning of each disk was
automatically created by the installer.

Since love is a commandment from the [church I went last year][5], I type out
the figures in the above photo so that the visually impaired can read them from
a text browser ([lynx][6], [w3c][7], etc).

> A new commandment I give to you, that you love one another: just as I have loved you, you also are to love one another.

> -- <cite>John 13:34</cite>

{{< beautifulfigure src="180729-ubiquity-err.jpg" title="Ubuntu 18.04 installation process" caption="P" alt="Ubiquity installer" >}}

Despite the error message inside the black-and-white terminal
<span class="fa-stack">
  <i class="fa fa-square fa-stack-2x"></i>
  <i class="fa fa-terminal fa-stack-1x fa-inverse"></i>
</span>
from the installer, it *didn't* pop up in a dialog box.

1. Reboot the machine <i class="fa fa-laptop" aria-hidden></i>.
2. Configure the boot menu (usually by pressing `<F2>` or `<F12>`)
3. Choose **ubuntu** as the first option.
4. **Save and exit**, then wait for reboot.

This allows the UEFI to load the installed Ubuntu OS *without* finding other bootable media.

<i class="fa fa-info-circle" aria-hidden></i> As shown in the previous pictures,
Ubuntu's GRUB bootloader was installed in `/dev/sda` (i.e. the SSD).  As a
result, we see **ubuntu** instead of **SSD**.

{{< beautifulfigure src="180729-uefi.jpg" title="Fubjitsu AH557 UEFI" caption="After boot menu configuration" alt="Fujitsu AH557 UEFI" >}}

### Stage 3: Ubuntu's warm welcome

After login, I was greeted with a series of messages after the installation.

{{< beautifulfigure src="180729-fin.jpg" title="SSD partition table" caption="Partitioned a Fujitsu 120GB SSD drive" alt="SSD partition for Ubuntu install" >}}

![live patch](180729-livepatch.png)

### Technical details

#### In pictures

To boost the loading of this page, I've sacrificed the colors of the screenshot
by adopting the indexed mode.  You may view the original images on
[my Flickr album][9].

![SSD partition scheme](180729-sda.png)  
![HHD partition scheme](180729-sdb.png)  
![USB partition scheme](180729-usb.png)

<i class="fa fa-info" aria-hidden></i> All photos and screenshots were taken on <time datetime="2018-07-29">29th July, 2018</time>.

#### <span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-terminal fa-stack-1x fa-inverse"></i></span> `sudo fdisk -l`

I format the output using tables so that it's easier to read.

##### Disk info

Disk | `/dev/sda` | `/dev/sdb` | `/dev/sdc`
---- | ---------- | ---------- | ----------
Size (GB) | 111.8 | 931.5 | 7.6
Size (B) | 120034123776 | 1000204886016 | 8178892800
#sectors | 234441648 | 1953525168 | 15974400
Units | sectors of 1 * 512 = 512B | sectors of 1 * 512 = 512B | sectors of 1 * 512 = 512B
Sector size (logical/physical): (B/B) | 512 / 512 | 512 / 4096 | 512 / 512
I/O size (minimum/optimal): (B/B) | 512 / 512 | 4096 / 4096 | 512 / 512
Disklabel type | gpt | gpt | dos
Disk identifier | E1CF8D05-E16E-40F2-BA52-21CA2D6CCB75 | 2C8ED1E6-261B-462F-A989-4198F16EDBD4 | 0xdb3ee8de
Partitions in disk order | <i class="far fa-check-square" aria-hidden></i> | <i class="far fa-square" aria-hidden></i> | <i class="far fa-check-square" aria-hidden></i>

##### Partitions on SSD

Device      |  Start |      End |  Sectors |   Size | Type
------------|--------|----------|----------|--------|-------------------
`/dev/sda1` |   2048 |   976895 |   974848 |   476M | EFI System
`/dev/sda2` | 976896 | 60976895 | 60000000 |  28.6G | Linux filesystem

##### Partitions on HDD

Device      |      Start |        End |  Sectors |  Size | Type
------------|------------|------------|----------|-------|-----------------
`/dev/sdb1` |       2048 |   39999487 | 39997440 | 19.1G | Linux filesystem
`/dev/sdb2` | 1921525760 | 1953523711 | 31997952 | 15.3G | Linux swap
`/dev/sdb3` | 1913526272 | 1921525759 |  7999488 |  3.8G | Linux filesystem
`/dev/sdb4` | 1905526784 | 1913526271 |  7999488 |  3.8G | Linux filesystem

##### Partitions on <i class="fab fa-linux" aria-hidden></i> live USB key

Device      | Boot | Start |      End |  Sectors | Size | Id | Type
------------|------|-------|----------|----------|------|----|----------------
`/dev/sdc1` | *    |  2048 | 15974399 | 15972352 | 7.6G |  c | W95 FAT32 (LBA)

[1]: https://www.ubuntu.com/download/desktop
[2]: /post/2018-06-28-xubuntu-dualboot-on-fujitsu-lh532/
[3]: https://gmic.eu/
[4]: https://www.gimp.org/
[5]: https://flic.kr/p/29pQEWg
[6]: https://lynx.invisible-island.net
[7]: https://w3m.sourceforge.net/
[8]: https://askubuntu.com/q/56883/259048
[9]: https://www.flickr.com/photos/sere6703/albums/72157694587632780
[10]: https://unetbootin.github.io/
