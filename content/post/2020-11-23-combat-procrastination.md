---
title: "Combat Procrastination"
date: 2020-11-23T09:06:04+01:00
categories:
- productivity
tags:
- procrastination
draft: false
---

My notes for a [YouTube video about procrastination][video] on one hearing only.

Why?

1. Boring tasks.

    It's necessary?

    - Yes: Make it fun.
        + Get a study partner.
        + Find a more energetic time.
    - No: Get rid of it.

2. Surrounding environment.

    - Identify someone who procrastinates in your circle of influence.
    - Distance yourself from them, or
    - Be aware of what their behavior.

3. Perfectionism

    - Know about yourself.
    - Reality: nothing can be perfect.

Five strategies against procrastination:

1. Decompose a huge task into smaller parts.
2. Analyse the environment causing your procrastination.

    Ill feelings arise after procrastinates.  Get a more positive environment to
    get motivation.

3. Create deadlines and schedules.

    Deadlines create a sense of urgency.  Measure success of a task by looking
    at the respect of the schedule.

4. Find an accountability partner.

    It's a coach who holds you accountable for a task.  It has to be
    respectable.  It can be your parents, your teacher, your spouse, etc.

5. Eliminate procrastination pit stops.

    A pit stop mean a place where a racing car has to stop for maintenance in a
    car race.

    Our pit stops can be smart phones, TV, emails.

    Figure out what's holding you from your tasks, and eliminate them.

[video]: https://youtu.be/zoa4nPgtTsU
