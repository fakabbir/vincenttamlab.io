---
title: Cover Letter Organisation
subtitle: A simple summary
date: 2018-10-02T21:50:02+02:00
type: post
categories:
- English
tags:
- cover letter
draft: false
---

I keep sentences below short and minimal for memory.

1. Sender's contact info at top-right hand corner, followed by receipent's
contact info left-aligned.
2. "I'm ...", "apply for ..., as advertised in ..."
3. Why apply?  Link with the company('s employee)
4. Pastimes (all-rounded person), continual learning (for useful skills)
5. Friendly, polite and to-the-point sign-off
6. Signature followed by sender's name
