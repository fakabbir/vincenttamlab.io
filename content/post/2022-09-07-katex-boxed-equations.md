---
title: "Katex Boxed Equations"
date: 2022-09-07T01:39:20+02:00
categories:
- blogging
tags:
- KaTeX
- CSS
draft: false
---

### Goal

To render a bordered box with multiple equations.

### Background

My [previous article about dot products][pp] contains some boxed math
expressions: some are inline and some take up the whole line.

For inline math, that's simple: either $\fbox{text}$ or $\boxed{a=1}$ will do.
For displayed math, these two KaTeX commands will ignore the newline command
`\\`.

### Solution

A simple Google search '"katex" AND "box"' saved the day.  It led me to
[this relevant Stack Overflow question][so44667064], whose accepted answer
suggested the use of custom CSS.  I copied the CSS class name `boxedmath` for my
blog.

```css
/* boxed math expressions */
.boxedmath {
  border: 0.0625em solid currentcolor;
  padding: 0.5em;
}
```

I appended the above section into `static/css/custom.css`.

[pp]: /post/2022-09-06-dot-products/
[so44667064]: https://stackoverflow.com/q/44667064/3184351