---
title: "Recover Mysql Root Password"
date: 2020-10-07T13:13:08+02:00
categories:
- technical support
tags:
- MySQL
draft: false
---

Just a little linklog to a [relevant page on How to Forge][htf].  Root
privileges are needed.

1. Stop service: `service mysql stop`
2. Start MySQL server w/o password: `mysqld_safe --skip-grant-tables &`
3. Connect to CLI client as root: `mysql -u root`
4. Reset root password.  Here's the syntax for ≥v.5.7.6.

    ```
    mysql> use mysql;
    mysql> SET PASSWORD FOR 'root'@'localhost' = PASSWORD("newpass");
    mysql> flush privileges;
    mysql> quit
    ```
5. Repeat step 1, or `killall mysqld` if it doesn't work.  Output can be
different on different Linux distro.

    ```
    service mysql start
    mysql -u root -p
    ```

In step 2, the ampersand `&` on the right allows the task to be executed in
background.

[htf]: https://www.howtoforge.com/setting-changing-resetting-mysql-root-passwords#recover-lost-mysql-root-password
