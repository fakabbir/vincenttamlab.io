---
title: "Git Warning: LF Will Be Replaced by CRLF"
date: 2022-06-10T20:18:01+02:00
categories:
- technical support
tags:
- Git
- sed
draft: false
---

I'm using Git Bash while writing this post.  I've never tested the commands on
\*nix.

### Background

Some of the tags and titles were written as "LateX"/"latex" in the source files.
I batch corrected them with Git Grep and GNU Sed.

```sh
git grep --name-only -IiE ^-\ latex$'\r'? content/{post,page} | \
xargs -n1 sed -i "s/^- latex/- LaTeX/I"
```

I tried to match

1. a leading `-` using `^-`
2. a whitespace `\ ` escaped by a backslash to avoid wrapping the special
character below with double quotes
3. the string "latex" (case-insensitive)
4. the carriage return `\r`, which is represented by `$'\r'` in bash, for at
most once (`?`)

I observed that wrapping `$'\r'` with `""` would lead to no match.

```
$ echo $'\r' | grep $'\r' && echo matched || echo no match

matched
$ echo $'\r' | grep "$'\r'" && echo matched || echo no match
no match
```

I normally use quotes to handle strings containing whitespaces, but it's clear
that quotes aren't suitable for this kind of notation for special characters.

Options used for Git Grep:

- `--name-only`: get the filenames only.   They'll be piped to `sed`'s input
argument later.
- `-I`: ignore binary files
- `-i`: case-insensitive match
- `-E`: extended mode, so that `^` and `?` can be used

From GNU Sed's manual, both `s/pat/repl/i` and `s/pat/repl/I` mean
case-insensitive match.

```
$ echo "a" | sed 's/A/b/i'
b
$ echo "a" | sed 's/A/b/I'
b
$ echo "a" | sed -n 's/A/b/ip'
b
$ echo "a" | sed -n 's/A/b/Ip'
b
```

However, if we're using `/pat/` for defining a range, `i` can't be used because
it's used for an insertion.

```
$ echo "a" | sed -n '/A/ip'
$ echo "a" | sed -n '/A/Ip'
a
```

### Observed Warning Message

```
$ git diff
warning: LF will be replaced by CRLF in content/post/2019-01-16-mise-a-jour-du-template-de-lettre.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2020-08-28-pandoc-latex-mermaid-on-gitlab-ci.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2020-12-23-ways-to-draw-diagrams-displayed-on-math-se.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-01-latex-code-for-linear-system.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-02-matrix-diagonalisation-and-change-of-basis/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-03-trigonometric-functions-by-unit-circle/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-04-dvisvgm-s-issue-with-fill-pattern/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-04-to-be-improved-normal-curve/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-09-latex-multirow-multicol-table-coloring/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-09-line-equation-in-intercept-form/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-10-latex-table-background-color-for-texit/index.md.
The file will have its original line endings in your working directory
```

Searching the message online would give explanation of the Git config parameter
`core.autocrlf`.  I'm sure that my setup is right:

- `input` for \*nix (CRLF → LF on commit, but not LF → CRLF on checkout)
- `true` for Windows (CRLF → LF on commit, LF → CRLF on checkout)

Using `tail -c 10 {file}.md | od -c` enables us to see that these files have
Unix-style line ending `\n` instead of Windows-style. `\r\n`.

I find the displayed message ambiguous.  When exactly will the replacement take
place?  Where will it be?  In the working tree or the Git binary object store?

### Further observations

From `od -c`'s output, I've found that the above in-place editing `sed -i`
strips `\r` off from a file with CRLF line terminators.  I've made the following
minimal example to illustrate this.

```
$ echo -ne abc$'\r'$'\n'def$'\r'$'\n' > temp1.txt

$ cat temp1.txt 
abc
def

$ file temp1.txt 
temp1.txt: ASCII text, with CRLF line terminators

$ od -c temp1.txt 
0000000   a   b   c  \r  \n   d   e   f  \r  \n
0000012

$ sed -i 's/abc/def/g' temp1.txt

$ od -c temp1.txt
0000000   d   e   f  \n   d   e   f  \n
0000010

$ file temp1.txt
temp1.txt: ASCII text
```

I had committed the changes, those files still had Unix-style line endings.

```
$ git commit -m "Unified LaTeX in tags and titles" content/post
warning: LF will be replaced by CRLF in content/post/2019-01-16-mise-a-jour-du-template-de-lettre.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2020-08-28-pandoc-latex-mermaid-on-gitlab-ci.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2020-12-23-ways-to-draw-diagrams-displayed-on-math-se.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-01-latex-code-for-linear-system.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-02-matrix-diagonalisation-and-change-of-basis/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-03-trigonometric-functions-by-unit-circle/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-04-dvisvgm-s-issue-with-fill-pattern/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-04-to-be-improved-normal-curve/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-09-latex-multirow-multicol-table-coloring/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-09-line-equation-in-intercept-form/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-10-latex-table-background-color-for-texit/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2020-08-28-pandoc-latex-mermaid-on-gitlab-ci.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-01-latex-code-for-linear-system.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-02-matrix-diagonalisation-and-change-of-basis/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-03-trigonometric-functions-by-unit-circle/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-04-dvisvgm-s-issue-with-fill-pattern/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-04-to-be-improved-normal-curve/index.md.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in content/post/2022-06-09-latex-multirow-multicol-table-coloring/index.md.
The file will have its original line endings in your working directory
[master 7f3ef7b] Unified LaTeX in tags and titles
 7 files changed, 7 insertions(+), 7 deletions(-)

$ tail -c 10 content/post/2022-06-01-latex-code-for-linear-system.md | od -c
0000000   l   i   g   n   }  \n   `   `   `  \n
0000012
```

Commits are check-in instead of check-out.  That's why those files in the
working tree didn't change during the commit.

```
$ git show HEAD:content/post/2022-06-01-latex-code-for-linear-system.md | \
tail -c 10 | od -c
0000000   l   i   g   n   }  \n   `   `   `  \n
0000012
```

I checked out to the previous commit then went back to observe what happen to
those files.

```
$ git checkout HEAD~
Note: switching to 'HEAD~'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by switching back to a branch.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -c with the switch command. Example:

  git switch -c <new-branch-name>

Or undo this operation with:

  git switch -

Turn off this advice by setting config variable advice.detachedHead to false

HEAD is now at a2bdb8c Homothety 2: added back GGB activity

$ git checkout master
Previous HEAD position was a2bdb8c Homothety 2: added back GGB activity
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 3 commits.
  (use "git push" to publish your local commits)

$ tail -c 10 content/post/2022-06-01-latex-code-for-linear-system.md | od -c
0000000   g   n   }  \r  \n   `   `   `  \r  \n
0000012

$ git show HEAD:content/post/2022-06-01-latex-code-for-linear-system.md | \
tail -c 10 | od -c
0000000   l   i   g   n   }  \n   `   `   `  \n
0000012

```

Observe that that file in working tree now contains `\r`.
