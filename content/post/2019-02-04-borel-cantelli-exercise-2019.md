---
title: "Borel Cantelli Exercise 2019"
date: 2019-02-04T16:24:40+01:00
type: post
categories:
- math
tags:
- probability
- Math.SE
draft: false
---

I intend to post this for [a Borel-Cantelli lemma exercise][3099953] on
[Math.SE].

> The target event is $\{\exists i_0 \in \Bbb{N} : \forall i \ge i_0, X_i =
> 1\}$, whose complement is
> 
> $$
> \{\forall i_0 \in \Bbb{N} : \exists i \ge i_0, X_i > = 0\}
> = \limsup_i \{X_i = 0\}.
> $$
>
> To apply Borel-Cantelli, one has to determine whether $\sum_i P(X_i =
> 0)<+\infty$.

[3099953]: https://math.stackexchange.com/q/3099953/290189
[Math.SE]: https://math.stackexchange.com
