---
title: "Adjust Git Bash Font and Window Size"
date: 2023-03-04T14:16:58+01:00
categories:
- technical support
tags:
- Git Bash
- Mintty
draft: false
---

### Goal

Make the characters larger and more readable.

### Solution

The option names might be found in the manual.  Sometimes, I find opening
MINGW's settings through GUI helpful.

```
FontHeight=12
Columns=143
Rows=36
```

I've read [Gerald Lee's `.minttyrc`][sample].  I've enlarged the window size so
that in the standard output, one line can contain more info.

[sample]: https://gist.github.com/gerarldlee/b971176550f2f9b44bf6
