---
title: "Beautiful Hugo Improvements"
subtitle: "Some bug fixes, Font Awesome 5, and more ..."
date: 2018-09-12T09:17:39+02:00
toc: true
categories:
- blogging
tags:
- Hugo
draft: false
---

### Fixed issue #142

Two weeks ago, [Google Webmasters][gw] complained about the broken urls in this
blog's [Tags](/tags) page.  This was reported by Joakim Vivas as
[issue&nbsp;#142][bh142] half a year ago.  Pascal had submitted
[pull request&nbsp;#165][bh165] to fix this.  However, he _self-closed_ his PR.

Since the last commit by Michael Romero, [Beautiful Hugo][bh]'s owner, was
_five_ months ago, it seems that he has _abandonned_ his project.  Therefore, I
used [Kaushal Modi's solution][bh142fix] to fix this at commit [ff536782].

### Upgraded to Font Awesome 5.3.1

Icons are good for illustrations since one remembers pictures _much better_ than
text.  However, the later is quicker to type, but harder to read.  If all text
are replaced with icons (say "GitHub" → <i class="fab fa-github"
aria-hidden></i>), then I _won't_ be able to search for a certain word in web
browsers, and I _will_ have to [`grep`][grep] source file(s) for a word---that's
_inconvenient_.

I've choosing the CSS version despite reduced functionalities to minimize the
amount of coding.

1. Link to a _newer_ CSS version, see commit [c8dac925]. (I chose `all.js` and
downloaded a local copy.)
2. Run [Esteban De La Fuente Rubio's update script][fa5_scr] with flags `-e md`
and `-e html`.
3. Some icons were still _broken_.  I applied commit [eb94ada4] to fix this.

<i class="fa fa-info" aria-hidden></i> Most of the icons are brand type, with a
few exceptions, such as
<span class="fa-stack fa-1x">
    <i class="fa fa-circle fa-stack-2x"></i>
    <i class="far fa-envelope fa-stack-1x fa-inverse"></i>
</span>
and
<span class="fa-stack fa-1x">
    <i class="fa fa-circle fa-stack-2x"></i>
    <i class="fas fa-gamepad fa-stack-1x fa-inverse"></i>
</span>.

### Custom CSS

Googling on this, I found [Kevin A. Smith's introduction][mit_tut] to Hugo's
[Academic] theme.  He suggested adding the file name `custom_css =
["custom.css"]` to `config.toml`.  I tried this but it _didn't_ work.

In [another web page on Hugo's forum][custom_css] about the same topic, Digital
Craftsman suggested using the whole relative path `css/custom.css`.  I tried
this at commit [f3380a0c] and this _worked_.

<i class="fa fa-info" aria-hidden></i> Looking back, the reason that adding
only the file name _didn't_ work for me for the first time is the missing folder
`/css` preceeding the filename.  Compare
[George Cushen's working example][workingeg]

{{< highlight go-html-template "linenos=table,hl_lines=2,linenostart=87" >}}
  {{ range .Site.Params.custom_css }}
  <link rel="stylesheet" href="{{ "/css/" | relURL }}{{ . }}">
  {{ end }}
{{</ highlight >}}

with [mine][f3380a0c].

{{< highlight go-html-template "linenos=table,hl_lines=2" >}}
{{ range .Site.Params.custom_css -}}
    <link rel="stylesheet" href="{{ . | absURL }}">
{{- end }}
{{</ highlight >}}

### Chroma syntax highlighting

Syntax highlighting on [Octopress] used to be a headache before my migration to
[Hugo].  The gems for line numbers _didn't_ work well:

1. _no_ support for custom line number start
2. _broken_ line number highlighting

Even though I _can't_ observe the visual effect of the second feature under
[Beautiful Hugo][bh] theme, I'm satisfied with the first one.

Apart from the customizable starting line number in code blocks, [Chroma], as a [Pygment] port to [Go], loads much quicker than [Pygment].

### Convenient inline $\KaTeX$ wrapping

I've chosen [Beatiful Hugo][bh] as the theme for this blog because it comes with
[$\KaTeX$][katex], which loads math much quicker than [MathJax].

In [Beatiful Hugo's math sample page][math_sample], all math expressions are in
_block_ style.  [$\KaTeX$][katex]'s default syntax `\(...\)` for wrapping math
was too _inconvenient_ to use, when compared to [MathJax]'s `$...$`.  To enable
this for [$\KaTeX$][katex], I uesd [Vincent Pace's script][so45301641]
<i class="fab fa-stack-overflow" aria-hidden></i> at commit [79cb0ba0].

{{< highlight go-html-template "linenos=table,hl_lines=8,linenostart=12" >}}
<script>
    renderMathInElement(
        document.body,
        {
            delimiters: [
            {left: "$$", right: "$$", display: true},
            {left: "\\[", right: "\\]", display: true},
            {left: "$", right: "$", display: false},
            {left: "\\(", right: "\\)", display: false}
            ]
        }
        );
    </script>
{{</ highlight >}}

[gw]: https://www.google.com/webmasters/
[bh142]: https://github.com/halogenica/beautifulhugo/issues/142
[bh165]: https://github.com/halogenica/beautifulhugo/pull/165
[bh]: https://github.com/halogenica/beautifulhugo
[bh142fix]: https://github.com/halogenica/beautifulhugo/issues/142#issuecomment-373466084
[ff536782]: https://gitlab.com/vincenttam/vincenttam.gitlab.io/tree/ff536782fecaa641949eeecae76816393a3aae11
[c8dac925]: https://gitlab.com/VincentTam/beautifulhugo/commit/c8dac9253ae3c6de1839cf9ede2fd388bba337de
[fa5_scr]: https://github.com/estebandelaf/scripts/blob/master/fontawesome4to5.sh
[mit_tut]: https://web.archive.org/web/20180719160919/http://www.mit.edu/~k2smith/post/getting-started
[Academic]: https://github.com/gcushen/hugo-academic/
[custom_css]: https://discourse.gohugo.io/t/how-to-override-css-classes-with-hugo/3033/4
[eb94ada4]: https://gitlab.com/VincentTam/beautifulhugo/commit/eb94ada4d829eed78fff8de4b8440ab1502c065e
[f3380a0c]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/commit/f3380a0c3793013bef91861f2d03fe38c0abdf4d
[workingeg]: https://github.com/gcushen/hugo-academic/blob/5115b1fa23e40a097fa90ea6f5884d8bb5751f5b/layouts/partials/header.html#L87-L89
[Octopress]: http://octopress.org/
[Hugo]: https://gohugo.io
[math_sample]: /post/2017-03-05-math-sample/
[katex]: https://github.com/Khan/KaTeX
[MathJax]: https://www.mathjax.org/
[Chroma]: https://github.com/alecthomas/chroma
[Pygment]: https://pygments.org/
[Go]: https://golang.org/
[so45301641]: https://stackoverflow.com/a/45301641/3184351
[grep]: /page/bash-commands/index.html#grep
[79cb0ba0]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/commit/79cb0ba059a045445a8b924efedd270dc8a17b94
