---
title: "Staticman Failure on Tsalikis' Site"
subtitle: "Use Alternative Staticman API server"
date: 2019-01-01T01:21:57+01:00
type: post
categories:
- blogging
tags:
- Staticman
draft: false
---

After reading [_Hugo Comments with Staticman_][s] on Kostas Tsalikis' web site,
I tried to leave the following comment.

> > As far as I know, Staticman can be used only with GitHub, since it works as
a GitHub bot. Obviously, not all static sites are saved in GitHub, and this may
be a showstopper for someone using another service.  Thanks to
eduardoboucas/staticman#219, **Staticman now supports GitLab.**  You may view my
demo Hugo site on *Framagit* at
https://staticman-gitlab-pages.frama.io/bhdemo/posts/my-second-post/
[Source](https://framagit.org/staticman-gitlab-pages/bhdemo)) as a working
example.
>
> ![Staticmanlab accepting new invitations](https://user-images.githubusercontent.com/5748535/50357920-e8f25500-0557-11e9-9cb6-73f8b575c4f0.png)

> Therefore, the URL in the `endpoint` in the site config file in your post will
> _no longer work_ for new theme users.  I quote the notice in
> [Minimal Mistakes](https://mmistakes.github.io/minimal-mistakes/docs/configuration/)
> for reference.

> > **Note**: Please note that as of September 2018, Staticman is reaching
GitHub API limits due to its popularity, and it is recommended by its maintainer
that users deploy their own instances for production (use
`site.staticman.endpoint`).

Unluckily, the form submission had failed, I opened
[tsalik/kostas-tsalikis.com#36][36] to inform the project owner of that.

[s]: https://www.droidship.com/posts/staticman-migration/
[36]: https://github.com/tsalik/kostas-tsalikis.com/issues/36
