---
title: "Laptop Battery Life"
subtitle: "Avoid deep discharges"
date: 2018-09-05T23:13:45+02:00
type: post
categories:
- technical support
tags:
- hardware
- battery
draft: false
---

### <i class="far fa-lightbulb" aria-hidden></i> Main ideas

- <i class="far fa-thumbs-up" aria-hidden></i>
<i class="fas fa-bolt" aria-hidden></i>
<i class="fas fa-store-alt" aria-hidden></i>
<i class="fas fa-battery-quarter" aria-hidden></i> --
<i class="fas fa-battery-three-quarters" aria-hidden></i>
Ideal energy range for storage: 40%--80%
- The biggest enemy of electronic parts
<i class="fas fa-laptop" aria-hidden></i>
<i class="fas fa-cogs" aria-hidden></i> is _heat_
<i class="fas fa-fire" aria-hidden></i>.
- Avoid overchanging and unplug AC adapter when charging is complete.  In
particular, _don't_ leave a fully charged laptop connected to power supply.
This can reduce the lifespan of the battery.
- Avoid _deep discharges_: recharge the laptop battery when its enery level
drops to 40%.
- Reduce unnecessary programs running in background.
- If your hardware vendor supports Linux drivers for batteries, set the charging
thershold to 80%.
    + Some [Lenovo Thinkpads][thinkpad] can benefit from those drivers.
    + Other vendors like [Fujitsu] offers such drives for M$ Windows
    <i class="fab fa-windows" aria-hidden></i> only.
    <i class="far fa-frown-open" aria-hidden></i>

### <i class="fas fa-link" aria-hidden></i> External URLs

1. [Life Hacker][1]
2. [Omg! Ubuntu!][2]

[thinkpad]: https://www.lenovo.com/us/en/laptops/thinkpad/thinkpad-x/ThinkPad-X1-Carbon-5th-Gen/p/22TP2TXX15G
[Fujitsu]: https://www.fujitsu.com/
[1]: https://lifehacker.com/5875162/how-often-should-i-charge-my-gadgets-battery-to-prolong-its-lifespan
[2]: https://www.omgubuntu.co.uk/2013/12/check-battery-life-health-ubuntu-linux
