---
title: "LaTeX Multirow Multicol Table Coloring"
date: 2022-06-09T00:45:55+02:00
categories:
- technical support
tags:
- LaTeX
- table
draft: false
---

### Inspiration

Number of two-digit numbers not containing the digit '8'.

### Code

- The `standalone` class can't be used with `\caption`.
- `*{8}{c}` means 8 `c`.
- `>{\columncolor{cyan}}c` gives the column color.  It
`\usepackage[table]{xcolor}`.
- `\rowcolor{cyan}` overrides the column colors.
- `\cellcolor{white}` overrides the above two commands.  It has to be carried
out one-by-one without any fancy package.
- `\multicolumn{n}{<alignment>}{content}` defines a multi-column cell.
- `\multirow{n}{*}{content}` defines a multi-row cell.  It
`\usepackage{multirow}`.  It can't contain `\multicolumn`.
- `\cline{i-j}` draws a horizontal line above the row from column `i` to column
`j`.
- No fancy way of coloring multirow cells without fancy packages.
- `\multirow{-9}` at the last row creates a cell spanning nine rows from bottom
to above, so that its content won't be covered by colors of former rows.
- `\rotatebox[origin=[clr]]{angle}{content}`: rotate `content` by `angle`
anticlockwise with `[clr]` as the rotation center.

```tex
\documentclass[12pt]{article}
\usepackage{multirow}
\usepackage[table]{xcolor}
\usepackage{tikz}
\begin{document}
\begin{table}[htbp]
\centering
\caption{Sample \LaTeX{} multirow table}
\vspace*{1ex}
\label{multiprogram}
\begin{tabular}{|c|c|*{8}{c}>{\columncolor{cyan}}cc|}
\hline
\multicolumn{2}{|c|}{\multirow{2}{*}{}} & \multicolumn{10}{c|}{unit digit} \\\cline{3-12}
\multicolumn{2}{|c|}{} & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\\hline
& 1 & &  &  &  &  &  &  &  &  &  \\
& 2 & &  &  &  &  &  &  &  &  &  \\
& 3 & &  &  &  &  &  &  &  &  &  \\
& 4 & &  &  &  &  &  &  &  &  &  \\
& 5 & &  &  &  &  &  &  &  &  &  \\
& 6 & &  &  &  &  &  &  &  &  &  \\
& 7 & &  &  &  &  &  &  &  &  &  \\
\rowcolor{cyan} \cellcolor{white}
& 8 & &  &  &  &  &  &  &  &  &  \\
\multirow{-9}{*}{\rotatebox[origin=c]{90}{tens digit}}
& 9 & &  &  &  &  &  &  &  &  &  \\\hline
\end{tabular}
\end{table}
\end{document}
```

Result:

![latex multirow mutlicol table with color](table.svg)

### Adaptations for MathBot on Discord

Unluckily, this bot doesn't allow `multirow` and `xcolor` in tables.  The same
code might fail another time that you try it.  Discord has gray background, so
the black foreground color isn't quite visible.  The table touches one boundary
by default.

After some searching, I've chosen the TikZ method: draw a long line above and
below the table, so that there would be enough space around the table.  It's
important that

1. we apply a `[scale=0.9, draw=none]` so that the dimensions are too large for
the compiler.  Otherwise, the bot would fail.  We added `draw=none` so that the
long line won't show up.  After some trial and error, the scale factor `0.9`
gives optimal results.
2. there's a pair of empty lines to separate the table with the two
`tikzpicture`.

I have to abandon all instances of `\multirow` and hard-code the vertical text
in the leftmost column.

```tex
\pagecolor{white}
\begin{tikzpicture}[scale=0.9, draw=none]
\draw (0,0) -- (\textwidth,0);
\end{tikzpicture}
```

![latex multicol table with MathBot](latex.png)
