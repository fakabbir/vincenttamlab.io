---
published: false
---

% Functions
% [Vincent Tam](https://git.io/vtam)
% 4th Sept., 2022

# Settings

- People mountain people sea
- *P* = {🧕,👩‍⚕️,👷,💂‍♀️,🕵️‍♂️,👨‍🌾,👩‍🍳,...}
- belongs to (∈)

    1. 👨‍🔬 ∈ *P*
    2. 🐢 ∉ *P*

# What is function?

+-------------------------------------+------------------------------------------+
| ![wife function](wife-fct.svg)      | ![concubine function](concubine-fct.svg) |
+-------------------------------------+------------------------------------------+
| ![length function](length-fct.svg)  | - need better words to describes things  |
|                                     | - what about inverse functions?          |
+-------------------------------------+------------------------------------------+

# Name list

| Z | M | S | G |
|:---|:---|:---|:---|
| 姓 | 名 | surname | given name |
| 王 | 八旦 | Wong | Eight Eggs |
| 李 | 崔牛 | Li | Tsui Ngau |
| 牛 | 柏葉 | Ngau | Albert |
| 丁 | 一叮 | Ting | One |
| 李 | 鹵味 | Lee | Braised Dishes |
| 李 | 老竇 | Li | Lo Dull |
| 黃 | 土地 | Wong | Land |
| 李 | 老表 | Li | Low Bill |
| 李 | 腦細 | Lee | Old boss |
| 袁 | 尼姑 | Yuen | Nun |
| 容 | 海龜 | Yung | Hoi Kwai |
| 李 | 老友 | Lee | No Friend |
| 李 | 老板 | Lee | Low Ban |
| 任 | 何人 | Yam | Ho Yan |
| 歐陽 | 李 | Au Yeung | Lee |
| 容 | 海歸 | Yung | Hoi Kwai |
| 楊 | 祖 | Yeung | Joe |
| 麥 | 麥宋 | Mak | Mac Delivery |
| 狄 | 狄尼 | Dick | Disney |

# Two ways to write sets

Goal: To write a set of all persons with surname "李"

- List:

    *L* = {(李,崔牛), (李,鹵味), (李,老竇), (李,老表), (李,腦細), (李,老友), (李,老板)}

- Condition:

    *L* = {🏃‍♂️ ∈ *P* | 🏃‍♂️ with surname "李"}

- ordered pair: (*a*,*b*)

    (歐陽,李) ≠ (李,歐陽)

- **subset** (⊆/⊂): *L* ⊆ *P*

    *L* is contained in *P*.  
    ℹ️ I prefer using '⊆'.

- **superset** (⊇/⊃): *P* ⊇ *L*
- **empty set** (∅): a set that has no element.

# ETV

- {🐢,🦆,😸,🐷,😸,🐢,🦆} = {🐢,🦆,😸,🐷}

<div class='embed-container'><iframe src='https://www.youtube.com/embed/3DpNeD0jP7Y'
frameborder='0' allowfullscreen></iframe></div>

# set operations

<div class="floatR">![venn diagram 2 circles](venn2.svg) \ </div>

## union (∪)
- *L*₁ = {🏃‍♂️ ∈ *P* | 🏃‍♂️ with surname "Li"}
- *L*₂ = {🏃‍♂️ ∈ *P* | 🏃‍♂️ with surname "Lee"}
- *L*₁ ∪ *L*₂ = {🏃‍♂️ ∈ *P* | 🏃‍♂️ ∈ *L*₁ or 🏃‍♂️ ∈ *L*₂} = *L*
- Observation: *L*₁ ⊆ *L* and *L*₂ ⊆ *L*
- Exercise: write another set union using the surname "Wong"

## intersection (∩)
- *J* = {(袁,尼姑), (李,老師), (容,海歸), (李,老板), (李,腦細)}
- *L* = {🏃‍♂️ ∈ *P* | 🏃‍♂️ with surname "李"}
- *J* ∩ *L* = {🏃‍♂️ ∈ *P* | 🏃‍♂️ ∈ *J* and 🏃‍♂️ ∈ *L*}
= {(李,老師), (李,老板), (李,腦細)}
- Observation: *J* ∩ *L* ⊆ *J* and *J* ∩ *L* ⊆ *L*

## complement (<sup>*c*</sup>)
- *L*<sup>*c*</sup> = {🏃‍♂️ ∈ *P* | 🏃‍♂️ ∉ *L*}
= {🏃‍♂️ ∈ *P* | 🏃‍♂️ whose surname is NOT "李"}

## difference (\\)
- *J* \\ *L* = {🏃‍♂️ ∈ *P* | 🏃‍♂️ ∈ *J* and 🏃‍♂️ ∉ *L*}
= {(袁,尼姑), (容,海歸)}
- *L*<sup>*c*</sup> = *P* \\ *L*
- Observation: *J* = (*J* ∩ *L*) ∪ (*J* \\ *L*)

# Cartesian product

<div class="bigImg">![cartesian product](axb.svg) \ </div>

# Logical AND (∧)

Example: Save
(<kbd>Ctrl</kbd> + <kbd>S</kbd>)

| <kbd>Ctrl</kbd> | <kbd>S</kbd> | <kbd>Ctrl</kbd> + <kbd>S</kbd> |
| :---: | :---: | :---: |
| ✓ | ✓ | ✓ |
| ✓ | ✗ | ✗ |
| ✗ | ✓ | ✗ |
| ✗ | ✗ | ✗ |

| *p* | *q* | *p* ∧ *q* |
| :---: | :---: | :---: |
| T | T | T |
| T | F | F |
| F | T | F |
| F | F | F |

# Logical OR (∨)

Example: digit key
(e.g. <kbd>1</kbd> in numpad or above alphabets)

| numpad <kbd>1</kbd> | alphabet <kbd>1</kbd> | enter <kbd>1</kbd> |
| :---: | :---: | :---: |
| ✓ | ✓ | ✓ |
| ✓ | ✗ | ✓ |
| ✗ | ✓ | ✓ |
| ✗ | ✗ | ✗ |

| *p* | *q* | *p* ∨ *q* |
| :---: | :---: | :---: |
| T | T | T |
| T | F | T |
| F | T | T |
| F | F | F |

# Implication (⟹)
## Example 1

| entered Univ through JUPAS | attended secondary school | entered Univ through JUPAS ⟹ attended secondary school | example |
| :---: | :---: | :---: | :--- |
| ✓ | ✓ | ✓ | me |
| ✓ | ✗ | ✗ | who? |
| ✗ | ✓ | ✓ | IB, GCE A Level |
| ✗ | ✗ | ✓ | 沈詩鈞 |

# Implication (⟹)
## Example 2

Promise: win Mark Six (六合彩) ⟹ treat you to dinner in a seafood restaurant

| *P* | bought Mark Six? | bill on me? | promise kept? |
| :---: | :---: | :---: | :---: |
| 👩‍🍳 | ✓ | ✓ | ✓ |
| 👮 | ✓ | ✗ | ✗ |
| 👨‍🌾 | ✗ | ✓ | ✓ |
| 👨‍💻 | ✗ | ✗ | ✓ |

👨‍💻: I don't buy Mark Six, so I don't have to spring for your dinner. 😛

set of persons who kept their promise
= {🏃‍♂️ ∈ *P* | 🏃‍♂️ kept his/her promise}
= {👩‍🍳, 👨‍🌾, 👨‍💻}

# Implication (⟹) and subsets

| *p* | *q* | *p* ⟹ *q* |
| :---: | :---: | :---: |
| T | T | T |
| T | F | F |
| F | T | T |
| F | F | T |

Recall:

- *L*₁ = {🏃‍♂️ ∈ *P* | 🏃‍♂️ with surname "Li"}
- *L* = {🏃‍♂️ ∈ *P* | 🏃‍♂️ with surname "李"}
- Observation:
    + *L*₁ ⊆ *L*
    + 🏃‍♂️ ∈ *L*₁ ⟹ 🏃‍♂️ ∈ *L*
- Exercise: show that for any set *A*, ∅ ⊆ *A*.

# Implication (⟹)
## To prove a "⟹" statement
### Direct way

1. Assume the **premise** *p* is true.
2. Show that the **conclusion** *q* is true.

# Implication (⟹)
## To prove a "⟹" statement
### By contradiction

Example: solve Sudoku (數獨) by guessing.

<div class="tbvtop">
+---------------------------------------+---------------------------------------+---------------------------------------+
| <div class="sudoku">                  | <div class="sudoku">                  | <div class="sudoku">                  |
| +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ |
| | 7 | 4 |   |   | 6 | 9 | 5 | 2 | 3 | | | 7 | 4 | 18| 18| 6 | 9 | 5 | 2 | 3 | | | 7 | 4 |   |   | 6 | 9 | 5 | 2 | 3 | |
| +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ |
| | 6 | 9 | 5 | 2 |   |   | 7 |   | 8 | | | 6 | 9 | 5 | 2 | 34| 13| 7 | 14| 8 | | | 6 | 9 | 5 | 2 |   |   | 7 |   | 8 | |
| +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ |
| | 2 | 3 |   | 7 |   | 5 | 6 | 9 |   | | | 2 | 3 |   | 7 |   | 5 | 6 | 9 | 14| | | 2 | 3 |   | 7 |   | 5 | 6 | 9 |   | |
| +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ |
| | 8 | 2 | 3 |   | 5 | 7 |   | 6 | 9 | | | 8 | 2 | 3 | 14| 5 | 7 | 14| 6 | 9 | | | 8 | 2 | 3 |   | 5 | 7 |   | 6 | 9 | |
| +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ |
| | 5 | 6 | 9 |   | 2 |   | 8 | 7 |   | | | 5 | 6 | 9 |   | 2 |   | 8 | 7 | 14| | | 5 | 6 | 9 |   | 2 |   | 8 | 7 |   | |
| +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ |
| | 1 | 7 | 4 | 6 | 9 | 8 | 2 | 3 | 5 | | | 1 | 7 | 4 | 6 | 9 | 8 | 2 | 3 | 5 | | | 1 | 7 | 4 | 6 | 9 | 8 | 2 | 3 | 5 | |
| +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ |
| | 3 |   | 6 |   | 7 | 4 |   | 8 | 2 | | | 3 |   | 6 |   | 7 | 4 |   | 8 | 2 | | | 3 |   | 6 |   | 7 | 4 |   | 8 | 2 | |
| +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ |
| | 4 |   | 2 |   |   | 6 |   |   | 7 | | | 4 |   | 2 |   |   | 6 |   |   | 7 | | | 4 |   | 2 |   |   | 6 |   |   | 7 | |
| +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ |
| | 9 | 8 | 7 |   | 1 | 2 |   |   | 6 | | | 9 | 8 | 7 |   | 1 | 2 |   |   | 6 | | | 9 | 8 | 7 |   | 1 | 2 |   |   | 6 | |
| +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ | +---+---+---+---+---+---+---+---+---+ |
|                                       |                                       |                                       |
| : *p* : known grids                   | : some posibilities                   | : ~*q* : wrong guess                  |
| </div>                                | </div>                                |                                       |
|                                       |                                       | - guess: R1C3 = 8                     |
|                                       |                                       | - [→←][contradiction_symb]: R2C6 = ø  |
|                                       |                                       |                                       |
|                                       |                                       | </div>                                |
+---------------------------------------+---------------------------------------+---------------------------------------+
</div>

1. *p* True
2. *p* ∧ ~*q* False
3. so ~*q* False (i.e. *q* True)

<small>[Sudoku source][sudoku_src]</small>

# Wiki's Function definition

- ∀: for all
- ∃: there exists

A [**function**][wiki_fct_def] *f*: <span class="dom">*A*</span> →
<span class="codom">*B*</span> is a subset of <span class="dom">*A*</span> ×
<span class="codom">*B*</span> such that

<div class="tbdef">

----------------------------------------------------  --------------------------------------
right-unique (用情專一)<br>                            ![no concubine](no-concubine-fct.svg)
🧑❤🦸‍♀️ ∧ 🧑❤🦹 ⇒ 🦸‍♀️ = 🦹<br>
∀ *a* ∈ *A*, ∀ *b* ∈ *B*, ∀ *b*' ∈ *B*,<br>
((*a*,*b*) ∈ *f* ∧ (*a*,*b*') ∈ *f*) ⟹ *b* = *b*'

total (冇單身狗)<br>                                   ![no bachelor](no-bachelor.svg)
∀ 🧑 ∈ *A*, ∃ 👩  ∈ *B* : 🧑❤👩<br>
∀ *a* ∈ *A*, ∃ *b* ∈ *B* : (*a*,*b*) ∈ *f*
----------------------------------------------------  --------------------------------------

</div>

The **domain** of *f* is <span class="dom">*A*</span>.

# Domain, codomain and range

<div class="floatR">![domain and range](domain-range.svg) \ </div>

In the picture,

- the **domain** of *f* is <span class="dom">*A* = {👷,👨‍🌾,👨‍💻,👨‍💼,👨‍⚖️}</span>
- the **codomain** of *f* is <span class="codom">*B* = {🧕,👮‍♀️,👩‍🎤,👩‍🏫,👩‍✈️,👸}</span>
- the **range** of *f* is <span class="rng">*f*\[*A*\] = {🧕,👩‍🎤,👩‍🏫,👩‍✈️}</span>.

# Injective functions and Surjective functions

<div class="tbdef1">

---------------------------------------------------------  --------------------------------------
injective function (女版用情專一)<br>                       ![left-unique](inj.svg)
👩‍💻❤👩 ∧ 👷❤👩 ⇒ 👩‍💻 = 👷<br>
∀ *a* ∈ *A*, ∀ *a*' ∈ *A*, ∀ *b* ∈ *B*,<br>
((*a*,*b*) ∈ *f* ∧ (*a*',*b*) ∈ *f*) ⟹ *a* = *a*'<br>
<br>
📝 To prove that a function *f* is injective,<br>
we assume that *f*(*a*) = *f*(*a*'),<br>
then we show that *a* = *a*'.

surjective function (冇單身狗乸)<br>                        ![no spinster](subj.svg)
∀ 👩 ∈ *B*, ∃ 🧑 ∈ *A* : 🧑❤👩<br>
∀ *b* ∈ *B*, ∃ *a* ∈ *A* : (*a*,*b*) ∈ *f*<br>
<br>
📝 To prove that a function *f* is surjective,<br>
we pick an arbitrary element *b* in the codomain *B*,<br>
then we try to find an element *a* in the domain *A* such
that *f*(*a*) = *b*.
---------------------------------------------------------  --------------------------------------

</div>

# Bijective functions and inverse functions

A function is **bijective** is it is both injective and surjective.

1. surjective function (冇單身狗乸)

    Given 👩 ∈ *B*.  You can find a 🧑 ∈ *A* so that 🧑❤👩.

2. injective function (女版用情專一)

    You can't have two different 🧑 & 👦 such that 🧑❤👩 & 👦❤👩.
    They must been the same person.  (represented by 🧑)

If we have a bijective function *f* : *A* → *B* defined by *f* : 🧑 ↦ 👩,
then we can define an inverse function *f*⁻¹ : 👩 ↦ 🧑.

# Comments and critiques

Field medalist Shing-Tung Yau (丘成桐) has emphasized the importance of asking good
questions (not in the textbook).

<!-- https://embedresponsively.com/ -->
<div class='embed-container'><iframe src='https://www.youtube.com/embed/4WNKU37aWrE?start=816'
frameborder='0' allowfullscreen></iframe></div>

[sudoku_src]: https://www.createclassicsudoku.com/solvesudoku_guessing.jsp
[contradiction_symb]: https://math.stackexchange.com/a/160040
[wiki_fct_def]: https://en.wikipedia.org/wiki/Function_(mathematics)
