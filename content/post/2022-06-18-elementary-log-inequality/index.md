---
title: "Elementary Log Inequality"
subtitle: "TikZ figures for some graphical proofs of this inequality"
date: 2022-06-18T20:38:41+02:00
categories:
- math
tags:
- LaTeX
draft: false
---

### Goal

Show that

<div>
$$ \ln (x) \le x - 1 $$
</div>

for all $x > 0$.

### Proof by inverse functions

Think about their reflection along the line $y = x$ (i.e. their inverse
function).  Then we get

<div>
$$e^x \ge x + 1$$
</div>

for all $x \in \Bbb{R}$, which is obviously true.

### Proof by definite integrals

Here's a second proof using definite integrals.

{{< beautifulfigure src="case1.svg" title="Case 1: x > 1"
title="Case 1: x > 1" alt="PGF Plot for log inequality" >}}

The rectangle $[1,x] \times [0,1]$ contains the region under the graph of the
reciprocal $y = 1/x$ in the domain $[1,x]$.

```tex
\newcommand\myT{2.75}
\begin{tikzpicture}
\begin{axis}[
    axis equal,
    axis lines=center,
    legend style={at={(1,1)},
    anchor=north east,
    fill=none},
    xlabel={$t$},
    ylabel={$y$},
    xmin=0,
    xmax=4,
    xtick=\empty,
    ytick=\empty,
    extra x ticks={1, \myT},
    extra x tick labels={$1$, $x$},
    extra y ticks={1},
    title={Proof of this inequality with $\log(x) := \int_1^x \frac1t \, \mathrm{d}t$},
]
\addplot[domain=1:\myT, pattern=north east lines, fill opacity=0.3] {1/x}
    \closedcycle;
\addlegendentry{$y = \frac{1}{t}$};
\addplot[dashed] coordinates {
(\myT,0)
(\myT,1)
(0,1)
};
\end{axis}
\end{tikzpicture}
```

{{< beautifulfigure src="case2.svg" title="Case 1: x < 1"
title="Case 2: x < 1" alt="PGF Plot for log inequality case 2" >}}

The rectangle $[x,1] \times [0,1]$ is contained in the region under the graph of
the reciprocal $y = 1/x$ in the domain $[x,1]$.  Consider their area and
multiply both sides by $-1$ to get the desired inequality.

```tex
\newcommand\myT{0.618}
\begin{tikzpicture}
\begin{axis}[
    axis equal,
    axis lines=center,
    legend style={at={(1,1)},anchor=north east,fill=none},
    xlabel={$t$},
    ylabel={$y$},
    xmin=0,
    xmax=4,
    xtick=\empty,
    ytick=\empty,
    extra x ticks={\myT,1},
    extra x tick labels={$x$,$1$},
    extra y ticks={1},
    title={Proof of this inequality with $\log(x) := \int_1^x \frac1t \, \mathrm{d}t$}
]
\addplot[domain=\myT:1, pattern=north east lines, fill opacity=0.3]
{1/x} \closedcycle;
\addlegendentry{$y = \frac{1}{t}$};
\addplot[dashed] coordinates {
(1,0)
(1,1)
(0,1)
};
\addplot[dashed] coordinates {
(\myT,0)
(\myT,1)
};
\end{axis}
\end{tikzpicture}
```
