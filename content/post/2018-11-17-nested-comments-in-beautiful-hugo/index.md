---
title: "Nested Comments in Beautiful Hugo"
date: 2018-11-17T15:12:50+01:00
type: post
categories:
- blogging
tags:
- Hugo
- Staticman
- jQuery
draft: false
---

### Quick links

1. [A minimal demo site on GitLab][demo] ([Source][demo_src])
2. [Beautiful Hugo pull request 222][8]
3. [Pre-release notes for this pull request][9]

### Motivation

For the mathematical ones, please see [my previous post][1].

As a math student, it's _inefficient_ to reinvent the wheel like engineering
students.  Thanks to three existing examples, I had convinced myself that I
could bring this to the theme [Beautiful Hugo][2].

- [Zongren's Hexo theme][3] (worked best)
- [Made Mistakes][4] Jekyll theme
- [Network Hobo's customization of Beautiful Hugo][5] (inspired by the second
one, but contains a logic error)

    {{< beautifulfigure src="networkhobo.png" title="Logic error in Network Hobo's commenting system" caption="Everybody replies to the same person?" alt="staticman nested comment logic error" >}}

All three of them follow the
[double-loop structure suggested by Eduardo Bouças][6].

### Difficulties

The names can be easily confused.  For example, `parentID` can mean the ID of
the comment to which another comment is replying, as well as the ID of the
[subscribed comment][7].  Choosing the right word that brings about readable
code _isn't_ easy.

### Discussion

To increase the chance that [my Beautiful Hugo pull request][8] get merged, I've
explained the code change in details, and written a
[pre-release note for this PR][9].

#### Preliminary step to solve the problem

Variable names like `reply_to`, `replyTarget`, etc, _don't_ convey the data
type, whereas `fields[parent]` can be

1. confused with `options[parent]` in the Hugo theme's partial layout for
Staticman (`layouts/partials/staticman-comments`), and
2. confusing as the [jQuery][10] method `parents()` returns multiple parents.

It took me hours to come up with three appropriate words describing the problem:
"main comment", "reply target" and "comment reply".

{{< beautifulfigure class="mysvg" src="drawing.svg" alt="problem OO sketch" title="Nested comments data fields" caption="A sketch of the object models of the comment classes" size="600x200" >}}

With these clear keywords, it's much easier to decouple the logic of the double
loop presented in [this Hugo + Staticman nested comments guide][11] on Network
Hobo.

#### Failed partial layout overloading

I tried passing variables from one partial layout to another.

{{< highlight go-html-template >}}
{{ partial "comment-replies" (dict "entryId_parent" $entryId "SiteDataComments_parent" $.Site.Data.comments "parentId" ._id "parentName" .name "context" .) }}
{{< /highlight >}}

However, `{{ $.Scratch.Get "varName"}}` had _failed_ to work.  After several
hours of trying, I've given up the idea of breaking main comments and comment
replies into two _separate_ partial layouts when I saw the
[relevant source code][12] in Network Hobo's GitHub repo.

#### Suboptimal code in Beautiful Hugo's native Staticman support

##### Go-HTML Template code

When I was trying to rewrite the double-loop in the above linked page on GitHub,
I observed that

1. Network Hobo's loop variable `$comments` is shorter and better than Beautiful
Hugo's one `$index, $comments`, and
2. Network Hobo and Beautiful Hugo use `reply_to` and `parent` to mean the
_same_ thing.

By successive use of `git blame`, I've found out that the confusing variable
name `parent` was introduced by Bruno Adele at commit [d68cf2e0][13].  It's
quite clear that he's following the double-loop method, but with the inner loop
_missing_.  By tracing back to the _first_ version of `layouts/partials
/staticman-comments.html`, I confirm that `parent` here means a "main comment",
instead of `options[parent]` in the HTML form in [Staticman issue 42][7].

##### CSS code

I opened [Beautiful Hugo issue 221][14] after testing my system with a
definition list.

{{< beautifulfigure src="bh221a.png" alt="beautiful hugo issue 221" title="Beautiful Hugo issue 221" caption="The paragraph and the definition list weren't properly aligned." >}}

The cause of the problem is the selector `p` at line 10 in
[`layouts/partials/staticman-comments.html`][15].

{{< highlight css "linenos=inline,hl_lines=6,linenostart=5" >}}
.staticman-comments .comment-content{
  border-top: 1px solid #EEEEEE;
  padding: 4px 0px 30px 0px;
}

.staticman-comments .comment-content p {
  padding: 5px 0px 5px 0px;
  margin: 5px 58px 0px 58px;
}
{{< /highlight >}}

### My proposed changes

1. HTML form: added three fields
    - `replyThread`: ID of the main comment (a.k.a. start of the thread)

        I've preferred this over `threadID` because a main post has empty reply
        target.  That's backward compatible since missing data fields in data
        files _won't_ cause an error in [Hugo][16].

        A submitted POST request would be read like `replyThread: 123...xyz`.
        This resembles our natural language: this POST request `reply` to the
        `Thread` with ID `123...xyz`.

    - `replyID`: ID of the reply target, for backward anchor to reply target in
    comment reply
    - `replyName`: author of the reply target
    - "Reset" button: reset every field values to the empty string
    - "Submit" button: button text changes to "Submit reply" when the link
    button "Reply to XYZ" is clicked.  This can be undone by clicking the
    "Reset" button.
2. Comment display Go-HTML template code
    - simplified the start of the loop as `{{ range $comments }}`
    - renamed `parent` as `replyThread`.
    - added a Bootstrap link button for reply under each comment
        - `id`: comment ID, transferable to `replyID` when the button is clicked
        - `title`: ID of its main comment, transferable to `replyThread` when
        the button is clicked
        - for a main comment, these two keys have the _same_ value
        - `href`: jumps to the HTML form
        - translation of UI text "Reply to" _in progress_
    - each comment reply is an HTML _sibling_ of a main comment
    - stores values main comment ID as the `Scratch` variable `ThreadID` for the
    inner loop (It won't throw an error despite empty value.)
    - inserted an inner loop
        - iterates over comment reply with `{{ range $comments }}`
        - operates on the condition that the main comment ID matches
        `replyThread`
        - the rest of the inner loop the _same_ as the origin loop, except the
        inclusion of an anchor to its reply target.
            - icon <i class="fas fa-reply"></i> for "reply to" for i18n.
            - a smaller heading between reply comment author and creation date
        - Bootstrap link button under comment reply: set above for explanation
3. [jQuery][10] code in `static/js/staticman.js` to handle the logic discussed
above
    - an offset which is equal to the height of the `navbar` is set, so that the
    `navbar` _won't_ cover the anchor target

        The `animation` serves as scrolling to an element with a _different_
        `id` specified in the "reply to" button's `href` attribute.

    - when the "reset" button is clicked, the three hidden fields in (1) will be
    set to the empty string
    - _no_ offset for the HTML form since it's at the bottom
4. CSS code
    - fixed [the above comment rendering error](#css-code) by replacing `p` with
    `*`
    - a left margin of 2em for each comment reply
    - a left margin of 58px for each comment timestamp

        {{< beautifulfigure src="bh221b.png" alt="beautiful hugo issue 221 resolved" title="Beautiful Hugo issue 221 resolved" caption="The paragraph and the definition list are now properly aligned." >}}

        The gravatar is of height and width 48px with a right margin of 10px
        floating on the left-hand side.  The height of the gravatar is
        approximate that of two lines (author name and timestamp).

        In each comment reply, an _additional_ line (anchor to reply target is
        inserted in between these two lines.  As a result, the timestamp would
        be left-aligned with the gravatar instead of the two preceeding lines of
        text.

        Expected behavior:

        > &nbsp;&nbsp;&nbsp;&nbsp; Comment author  
        > &nbsp;&nbsp;&nbsp;&nbsp; Anchor to reply target  
        > &nbsp;&nbsp;&nbsp;&nbsp; Comment timestamp

        Observed behavior:

        > &nbsp;&nbsp;&nbsp;&nbsp; Comment author  
        > &nbsp;&nbsp;&nbsp;&nbsp; Anchor to reply target  
        > Comment timestamp

        [Disqus][17]'s solution:

        > &nbsp;&nbsp;&nbsp;&nbsp; Comment author ↷ Anchor to reply target  
        > &nbsp;&nbsp;&nbsp;&nbsp; Comment timestamp  

        However, since the comment author is wrapped by a `h4` element with
        class name `comment-author`, I'm afraid that changing this would break
        other parts.  Since I'm _not_ expert in web design/programming, it's
        better to leave other experts' work untouched, and add a little bit of
        basic CSS code to get this fixed.

        Even though the intended target of this additional left-margin of 58px
        is the comment timestamp of the comment replies, since the gravatar is
        floating on the left-hand side, applying this to main comments _won't_
        cause any addition right shift of the comment timestamp in a main
        comment.

    - reduced the bottom padding from 30px to 4px to accommodate the Bootstrap
    "reply to" link button

        I've played with [Firefox][18]'s CSS box model in Developer Tools and
        done the math to find out the suitable dimensions.

### Lessons learnt

#### English

Code needs appropriate variable names to be readable.  This requires accurate
use of vocabulary.

#### OML

This project should have been finished in one day if I had treated comment data
as objects and drawn OML diagrams for them.

Got lost while reading other's work, it took me three days to fix
[Network Hobo's template code][11] and adapt it to [my pull request][8].

#### jQuery

[jQuery][10] provides plenty of convenient functions to accelerate web
scripting.

- basic selection: `$(this)`, `$('#id')`, `$('.class')`, `$('elem')`
- intermediate selection:
  - `$('#id[key="value"]')`
  - `$('.class :first-child')`
  - `.parent()`: immediate parent
  - `.parents()`: a chain of parents
  - `.parents(selector)`: select parent(s) according to `selector`
  - `.siblings()`: works in a similar way as `.parents()`, but for tags directly
  under the _same_ `.parent()`
  - descendant selector: `$('#my_parent .children1')`
- specific selection: `$('input[name="fields[replyID]"]')`, `$('a[href^="#"]')`,
`$(p[title_="foo"])`
- attribute/value getter & setter: `.text()`, `mybtn.val('key')`,
`mylink.attr('href')`
- scrolling: `scrollTop(ycoord)`
- get/set an element's position relative to the document: `myTag.offset()`
returns an object with properties `top` and `left`.
- to jump to a different `href` when clicking an anchor, use either one of the
following methods with `a_delay` of at least 200 milliseconds.
  1. `animate({scrollTop: ycoord}, a_delay)`: smooth transition
  2. JavaScript's built-in `setTimeout(function(){...}, a_delay)`
  3. `delay(a_delay)` has to be used in series, like
  `action1(...).delay(a_delay).action2(...)`, so it's more restrictive.

    I'll be glad to know any CSS way to solve this, since a CSS way should be
    much simpler.

- strict equality `===` (`!==` if negated) for type equality
  - `"0" == 0`: true
  - `"0" === 0`: false
- `null` vs empty string `""`
  - `null`: reference doesn't even exist, `null` has _neither properties nor
  methods_
  - `""`: reference exists, gives a value of `""`
  - `null.length` gives a type error; `"".length` returns zero.

#### CSS

- specify dimensions: `margin: top right bottom left`
- `clear` vs `float`: clear horizontal space or not
- descendant selector: there's also a space separating the parent and the
children
- universal selector: use a wildcard `*` to select _everything_.  This can be
useful to select all children in a block canvas.
- margin vs padding: always use margin except for spaces between border and
content
  - margin _doesn't_ belong to the element (relevant for click events)
  - padding _belongs_ to the element
  - apart from imagining the picture of CSS box model, take two vertically
  aligned elements.  If their margin is 1px, their separation is 1px;
  if their padding is 1px, their separation is 2px.

[1]: /post/2018-11-17-interactive-blog-on-static-web-host/
[2]: https://themes.gohugo.io/beautifulhugo/
[3]: https://zongren.me/
[4]: https://mademistakes.com/
[5]: https://networkhobo.com/
[6]: https://github.com/eduardoboucas/staticman/issues/35
[7]: https://github.com/eduardoboucas/staticman/issues/42
[8]: https://github.com/halogenica/beautifulhugo/pull/222
[9]: https://github.com/VincentTam/beautifulhugo/releases/tag/reply-rc1.1
[10]: https://jquery.com/
[11]: https://networkhobo.com/2017/12/30/hugo-staticman-nested-replies-and-e-mail-notifications/
[12]: https://github.com/dancwilliams/networkhobo/blob/6d0d84b449dc2b0bc0be27c53459aadb6d87c21a/layouts/partials/post-comments.html#L23,L60
[13]: https://github.com/halogenica/beautifulhugo/commit/d68cf2e04dd2d64efa0c8e6312aa271402fb06c0
[14]: https://github.com/halogenica/beautifulhugo/issues/221
[15]: https://github.com/halogenica/beautifulhugo/blob/2d97ca60766f1de2d968aec4d4f161fb06bf2b86/static/css/staticman.css#L5,L13
[16]: https://gohugo.io/
[17]: https://disqus.com
[18]: https://www.mozilla.org/fr/firefox/
[demo]: https://vincenttam.gitlab.io/bhdemo
[demo_src]: https://gitlab.com/vincenttam/bhdemo
