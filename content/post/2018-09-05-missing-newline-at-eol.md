---
title: "Missing Newline at EOL"
subtitle: "Bash detection for dirty EOL and batch editing"
date: 2018-09-05T00:31:02+02:00
type: post
categories:
- blogging
draft: false
---

### Background

Despite my experience in [Vim], the multi-cursor functionality in
[Sublime Text&nbsp;3][st3] has seduced my to change my editor.

Unlike [Vim], a nonempty new line at the end of [Sublime Text&nbsp;3][st3]
file buffer causes the file to end _without_ a newline character.  In fact,
it's a _POSIX standard_ to include a newline character at the EOL (end of
file).  (c.f. [No Newline at End of File][bot])

### Problem

_Unaware_ of the above POSIX standard and [Sublime Text&nbsp;3][st3] convention,
I have edited many lines of code in the repository for this blog and the one for
my custom [Beautiful Hugo][bh].  These edited files <span class="fa-stack"><i
class="far fa-file fa-stack-2x fa-flip-horizontal" aria-hidden></i><i class="fas
fa-pencil-alt fa-stack-1x" aria-hidden></i></span> were _almost everywhere_ in
these repositories, and they _polluted_ their remote counterparts on GitLab <i
class="fab fa-gitlab" aria-hidden></i>.

### Solution

1. List files from Git index or the file tree.
2. Omit binary files (especially image files <i class="far fa-file-image"
aria-hidden></i>).
3. Look into the file and append a newline if necessary.

#### Core part

It's possible to write shell script which appends a newline provided that the
last character `tail -c 1 $1`  matches an empty string '', but I prefer a
oneline solution.

#### `find` the way out?

Since the folders `public`, `themes` and `.git` have to be pruned, the pattern
`-path [DIR] -prune -o` has to be repeated _three times_.  This pattern is found
in the man page of `find`, so there's no other simpler way of `find`ing files
with some `[DIR(S)]` excluded.

In addition, `-exec` waits for a command instead of an `if-else` statement.
Therefore, there's no way to bring the above part into `-exec`ution.

#### A low-level attempt

[Git <i class="fab fa-git" aria-hidden></i>][git] has a low-level command `git
ls-files` to list the files.  However, this _didn't_ work well with my `grep`
command for omitting image files <i class="far fa-file-image" aria-hidden></i>
due to the presence of the submodule for the theme.

    $ for f in `git ls-files`; do if grep -Iq '' $f; then
    for then> echo "$f is an ASCII file"; fi; done
    .gitignore is an ASCII file
    .gitlab-ci.yml is an ASCII file
    ...
    static/css/print.min.css is an ASCII file
    static/google191a8de293cb8fe1.html is an ASCII file
    grep: themes/beautifulhugo: Is a directory

<i class="fas fa-info-circle" aria-hidden></i> Even though the installation
instructions of [Beautiful Hugo][bh] suggest cloning _independently_, doing so
will lead to failure during GitLab CI/CD.  Therefore, one has better follow the
official [Hugo]'s approach: grab the theme as a Git submodule.

#### Solution for my custom contents and themes

    $ pwd
    /home/vin100/quickstart

    $ for f in `git grep --cached -Il ''`; do if [ "$(tail -c 1 $f)" != '' ]; then
    for then> echo >> $f; fi; done
    git status
    On branch master
    Your branch is ahead of 'origin/master' by 1 commit.
    (use "git push" to publish your local commits)

    Changes not staged for commit:
    (use "git add <file>..." to update what will be committed)
    (use "git checkout -- <file>..." to discard changes in working directory)

          modified:   content/page/bash-commands/index.md
          modified:   content/post/2018-07-07-upgraded-to-linux-mint-19/index.md
          modified:   content/post/2018-07-14-hugo-image-path-refactoring.md
          modified:   content/post/2018-07-23-fujitsu-lh532-keyboard-cleaning/index.md
          modified:   content/post/2018-07-26-web-image-optimisation-with-gimp/index.md
          modified:   content/post/2018-08-15-tlp-prevents-login-on-ubuntu-18-04.md
          modified:   content/post/2018-08-23-brighten-image-with-gimp/index.md
          modified:   layouts/partials/footer_custom.html
          modified:   layouts/partials/head_custom.html
          modified:   static/css/custom.css
          modified:   static/css/print.min.css
          modified:   static/google191a8de293cb8fe1.html

I used to thinking that `git grep` operates on the index by default.  In fact,
an _additional_ `--cached` flag needs to be passed to obtain this behavior.

#### Solution for my enhanced Hugo theme

I had taken special care with the assets in `static`: exclude this folder also.
However, I _couldn't_ think of a better solution _without_ digging deeper into
"bash inside bash".  Therefore, I resorted to a basic solution.

1. First loop through all text files and perform necessary modifications.

        $ for f in `git grep -lq ''`; do
        for> if [ "$(tail -c 1 $f)" != '' ]; then
        for then> echo >> $f
        for then> fi
        for> done

2. Then undo the previous step for some files, say the minified CSS and JS files
in `./static`.

        $ git checkout -- static

    Since `git reset --hard` _doesn't_ accept double-hyphen `--` on the
    right-hand side, one has to run the above commands a few times against
    different subfolders to obtain the desired result.

### Prevent making the same mistake again

To err is human, but repeating the _same_ error _isn't_ wise.  However, it's _inefficient_ to manually inspect the end of file for _each_ edited files.

    $ for f in "$(git diff --name-only)"; do echo $f && tail -c 1 $f | od -c; done

To prevent making this mistake in [Sublime Text&nbsp;3][st3], I added one simple
line to the user preferences.

{{< highlight json >}}
"ensure_newline_at_eof_on_save": true
{{</ highlight >}}

[Vim]: https://www.vim.org
[st3]: https://www.sublimetext.com/3
[bot]: https://robots.thoughtbot.com/no-newline-at-end-of-file
[bh]: https://github.com/halogenica/beautifulhugo
[git]: https://git-scm.com
[Hugo]: https://gohugo.io/themes/installing-and-using-themes
