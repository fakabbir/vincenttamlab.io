---
title: "Simpler Diagram for Trigonometric Functions With Unit Circle"
date: 2022-06-15T16:15:33+02:00
categories:
- math
tags:
- LaTeX
draft: false
---

### Background

In my [previous post about unit circle and trigonometric functions][pp], I
included a graph with three trigonometry functions.  I'm quite satisfied with my
TikZ picture.

### Problem

Unluckily, a secondary school student found that my diagram was too complicated.

### Solution

```tex
\documentclass[tikz,border=2pt]{standalone}
\usepackage{pgfplots}
\pgfplotsset{compat=1.18}
\usetikzlibrary{calc}
\newcommand\mytheta{110} %angle theta
\begin{document}
\begin{tikzpicture}[scale=2]
\coordinate[label=left:$O$] (O) at (0,0);
\coordinate[label=above left:${A = (\cos\theta, \sin\theta)}$] (A) at (\mytheta:1);
\coordinate[label=below left:${B = (\cos(-\theta), \sin(-\theta))}$] (B) at (-\mytheta:1);
\coordinate[label=right:$E$] (E) at (1,0);
\draw (O) circle (1);
\draw (A) -- (O)  node [midway, left] {$1$}
    -- (E);
\draw (B) -- (O);
\draw[-stealth] ($(O)!0.3!(E)$) arc (0:\mytheta:0.3) node[midway, above] {$\theta$};
\draw[-stealth] ($(O)!0.25!(E)$) arc (0:{-\mytheta}:0.25) node[midway, below] {$-\theta$};
\end{tikzpicture}
\end{document}
```

![unit circle sine cosine](unit-circle.svg)

Skills learnt:

- `\coordinate[label=above left:$O$] (O) at (0,0);` for labelling a point.
- Inside the `$...$`, if I put `A = (\cos \theta, \sin \theta)`, the system will
complain.  We have to surround this with a pair of curly braces `${...}$`.
- To produce the convex combination of two points `($(O)!.3!(E)$)`, we have to
wrap the whole expression with a pair of parenthesis.

[pp]: /post/2022-06-03-trigonometric-functions-by-unit-circle/
