---
title: "Huginn Theme With Staticman"
date: 2019-05-19T19:26:28+02:00
categories:
- blogging
tags:
- Staticman
- Hugo
draft: false
---

### Goal

To provide [Staticman] support to the [Hugo] theme [Huginn].

### Motivation

A [Framagit] user tried [using the public GitLab instance][st3_rm] but failed.
Finally, he removed [Staticman] from his site and his [Hugo theme][Huginn].

If I had been notified, I would have explained that that was due to the
constraint of `gitlabBaseUrl`, which could only take _one_ GitLab instance.

In response to demand for [Staticman] from [Framagit] users, I set up
[another GitLab instance][stf] of Staticman API and forked some Hugo/Jekyll repo
under the project page [Staticman et GitLab Pages][sgp].

To keep this project alive, I've recently ported the Hugo code from
[Beautiful Hugo PR 222][pr222] to [Huginn].

### Demo site

You may view the demo site at https://staticman-gitlab-pages.frama.io/huginn.

[Staticman]: https://staticman.net/
[Hugo]: https://gohugo.io/
[Huginn]: https://framagit.org/Bridouz/hugo-theme-huginn
[Framagit]: https://framagit.org/
[st3_rm]: https://framagit.org/Bridouz/bridouz.frama.io/commit/f01aca4e3267d87d15057ca596554b3a190bd45f
[stf]: https://staticman-frama.herokuapp.com/
[sgp]: https://framagit.org/staticman-gitlab-pages
[pr222]: https://github.com/halogenica/beautifulhugo/pull/222
