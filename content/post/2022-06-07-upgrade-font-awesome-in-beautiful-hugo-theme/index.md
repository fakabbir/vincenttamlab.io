---
title: "Upgrade Font Awesome in Beautiful Hugo Theme"
subtitle: "Technical adjustment to Font Awesome's official guide"
date: 2022-06-07T13:02:33+02:00
categories:
- blogging
tags:
- Font Awesome
- Hugo
draft: false
---

### Background

[Beautiful Hugo][bh] comes with Font Awesome icons.  However, it seems that it's
no longer maintained.

### Goal

To upgrade Font Awesome to the current stable version (6.1.1).

### Problem

The [official guide for self-hosting Font Awesome Web Fonts + CSS][guide]
suggests users to place the following files

| filepath | role |
| :--- | :--- |
| `/fontawesome/webfonts/` | Free Web Fonts |
| `/fontawesome/css/all.min.css` | CSS file for using Free Web Fonts |

into somewhere in my theme.  The left column of the diagram features the folder
`assets/`.

![Font Awesome Web Fonts + CSS self host setup](fa-assets-setup.png)

After moving them into `assets/`, I saw broken Font Awesome icons were broken
during a local preview.

### Discussion

The search "gohugo assets vs static" led me to Hugo's page about its
[Directory Structure][fund].  I found the official explanation a bit technical
for outsiders.

- `assets/`: files to be parsed by the site generator.

    example: `assets/js/staticman.js` contains the Hugo variable ``{{ .api }}``
    (to be used in the site config file) for the JS variable ``url``.

- `static/`: files to be kept as-is without changes.

    example: site icon/logo, most CSS and JS files (jQuery, KaTeX, Bootstrap,
    etc)

Therefore, Font Awesome's Free Web Fonts should be placed under `static/`
instead.

While writing this post, I realised that I had overlooked the white text at the
bottom of the above image.  It suggests users to place these resources under a
suitable location in my theme.

### Other observations

The fonts in this theme have been put into several paths.  Wishing to get back
to my math and LaTeX learning, I have no desire to touch that.

[Multiple line Markdown links are possible if the newline character in the\
super long link text is preceeded by an antislash.][so44667064]  However, the
trailing antislash doesn't look smart.  I fear the possible troubles for
maintaining stuff with special characters in the future.

The command for locally testing a Hugo theme is

    hugo server -s exampleSite --themesDir=../..

[bh]: https://github.com/halogenica/beautifulhugo/
[guide]: https://fontawesome.com/docs/web/setup/host-yourself/webfonts#add-font-awesome-files-to-your-project
[fund]: https://gohugo.io/getting-started/directory-structure/
[so44667064]: https://stackoverflow.com/a/c/3184351
