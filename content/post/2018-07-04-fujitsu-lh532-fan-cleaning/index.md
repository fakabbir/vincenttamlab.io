---
title: "Fujitsu LH532 Fan Cleaning"
subtitle: "Let's reduce e-waste!"
date: 2018-07-04T20:52:37+02:00
type: post
categories:
- technical support
tags:
- Fujitsu Lifebook
- LH532
- hardware
- cleaning
---

### Problem

Two months ago, the Win\* 10 installed on my old laptop crashed with
blue screen on startup.  Two weeks ago, I created a [Xubuntu 18.04][1]
LTS 64-bit Live USB and set up the [dualboot][2].  However, the fan
was still giving out *consideralbe noise and heat* under Xubuntu 18.04
(installed in `/dev/sda7`).

### Many end users' reaction

They'll simply buy a new PC.  This had given rise to industrial parks
in Guiyu (貴嶼), one of [the world's largest e-waste village][3].  I'm
so shocked to see that Hong Kong appears on the top of the linked
article.

### My own way

> 是以聖人常善救人，故無棄人；常善救物，故無棄物。是謂襲明。故善人者，不善人之師；不善人者，善人之資。不貴其師，不愛其資，雖智大迷，是謂要妙。
>
> -- <cite>《老子》襲明章第二十七</cite>

Open the lid and clean the fan.  Here's a list of YouTube videos I
watched before actually grabbing my screwdriver.

1. [Ming's demo][4]
2. [Geekerwan's video][5]

I opened the lid and cleaned its inner side.  The amount of dust on
the fan blades was as expected.

{{< beautifulfigure src="IMG-20180701-WA0004.jpg" alt="dusty fan" >}}

However, I was shocked by the dust which had been blocking the
exhaust.  This had caused the heat to build up inside the case.

{{< beautifulfigure src="IMG-20180701-WA0006.jpg" alt="exit blocked" >}}

I had seriously thought about replacing the thermal paste due to Win*
failure.  However, having read [Ofey's article][6], I've decided to
*leave it untouched*.

{{< beautifulfigure src="IMG-20180701-WA0009.jpg" alt="warning" >}}

### Result

After two hours of cleaning, my Fujitsu Lifebook now runs
[Xubuntu 18.04][7] quieter and cooler.

{{< beautifulfigure src="IMG-20180701-WA0011.jpg" alt="after cleaning" >}}

### Safety precautions and remarks

A small amount of electrostatic charges from the door knob or a
blanket can put a tiny electronic component into intensive care.  To
avoid this, grounding the working place is recommneded.  Nonetheless, I
*didn't* use the following tools since I *couldn't* bought them.

- an antistatic mat
- an antistatic wrist strap (use a *wired* one)

Use a precision screwdriver set for disassembling your computers since
screws of difference types and sizes are found in a computer.

You may see original photos (with higher resolution licensed under CC-BY) in
[my Flickr album][8].

[1]: https://xubuntu.org/release/18-04/
[2]: /post/2018-06-28-xubuntu-dualboot-on-fujitsu-lh532/
[3]: https://owlcation.com/stem/15-of-the-Worlds-Largest-Landfills
[4]: https://youtu.be/k_8SjnQqjco
[5]: https://youtu.be/1sFuS2y1B1o
[6]: https://bit.ly/2w7V439
[7]: https://xubuntu.org/release/18-04/
[8]: https://flic.kr/s/aHsmoXrCEG
