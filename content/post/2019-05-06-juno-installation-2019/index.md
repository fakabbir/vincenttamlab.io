---
title: "Juno Installation 2019"
subtitle: "Log of system-wide config"
date: 2019-05-06T13:47:19+02:00
categories:
- technical support
tags:
- Juno
- Atom
- Julia
draft: false
toc: true
---

# Goal

To install [Juno] for convenient development in [Julia].

# Problem

After [Juno] installation, I received the following error.

![Juno error](noju.png)

# Analysis

From the error message, the system had tried to find an executable file `julia`,
which would then be executed by the shell `/bin/sh`.  Nonetheless, it's _absent_
from the system's environment `$PATH`, so [Juno] _couldn't_ find it.

I've only changed my `$PATH` in ZSHRC, which is user-specific.

# Solution

This Stack Overflow [question about `$PATH` setting on \*nix][so_path] has
provided various solution.  As I was in a hurry to get [Julia] run on [Juno],
I've taken the advice from the best answer.  I prefer creating a _separate_
shortcut under `/usr/bin`, rather than editing system-wide config files.

    $ cd /usr/bin
    $ sudo ln -s $(which julia) julia

# Upgraded Julia

I was surprised to learn that my old laptop was running version 1.0.1.  To
cleanly upgrade [Julia] to the currect stable version (v&nbsp;1.1.0), I removed
the old one and replaced it with the downloaded tarball.

![Julia in Juno](ju.png)

[Juno]: https://junolab.org/
[Julia]: https://julialang.org/
[so_path]: https://stackoverflow.com/q/14637979/3184351
