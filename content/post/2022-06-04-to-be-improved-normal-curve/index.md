---
title: "To Be Improved Normal Curve"
subtitle: "First function plot with pattern fill"
date: 2022-06-04T13:27:33+02:00
categories:
- math
tags:
- LaTeX
draft: false
---

Adaptations for `standalone` documents:

- `\usetikzlibrary{pattern}` before `\begin{document}`
- `\pgfplotsset{compat=1.6}`

```tex
\begin{tikzpicture}
\begin{axis} [axis lines=center,legend style={at={(0.7,0.7)},anchor=south west}]
\addplot [domain=-3:3, thick, smooth, yellow] { 1/sqrt(2*pi)*exp(-x^2/2) };
\addlegendentry{$y = \tfrac{1}{\sqrt{2\pi}} e^{-x^2/2}$};
\addplot [dashed, yellow] coordinates {(1.5,0) (1.5,0.14)};
\addlegendentry{99th percentile};
\addplot[domain=-3:1.5, pattern=north east lines,draw=none, fill opacity=0.3]
{ 1/sqrt(2*pi)*exp(-x^2/2) } \closedcycle;
\end{axis}
\end{tikzpicture}
```

![tikz function shaded region](shaded-region.svg)
