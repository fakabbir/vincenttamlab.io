---
title: "Another Math.SE Double Integral Using Polar Coordinate"
date: 2021-01-10T16:40:17+01:00
categories:
- math
tags:
- calculus
---

I wanted to post the following answer to a [question on double integral][mseq]
on Math.SE, but someone had submitted his work before I finished typing.  As a
result, I'm posting this on my personal blog.

Let $r = \sqrt{x^2+4y^2}$ and $t = \begin{cases} \tan^{-1}(2y/x) &\text{
if } x > 0 \\\\ \pi/2 &\text{ if } x = 0. \end{cases}$  Then $\begin{cases}
x &= r \cos t \\\\ y &= (r \sin t)/2 \end{cases}$ and $D = \{ (r,t) \mid r \ge
0, t \in [\pi/4, \pi/2] \}$.  Calculate the Jacobian

<div>
$$
\begin{vmatrix}
\dfrac{\partial x}{\partial r} & \dfrac{\partial x}{\partial t} \\
\dfrac{\partial y}{\partial r} & \dfrac{\partial y}{\partial t}
\end{vmatrix}
= \begin{vmatrix} \cos t & -r \sin t \\ (\sin t) / 2 & (r \cos t) / 2
\end{vmatrix} = r/2.
$$
</div>

<div>
$$\therefore \iint_D e^{-x^2-4y^2} \, dxdy = \int_{\pi/4}^{\pi/2} dt
\int_0^\infty e^{-r^2} \frac12 r \, dr = \frac{\pi}{4} \cdot \frac12 \cdot
\frac12 \cdot 1 = \frac{\pi}{16}$$
</div>

[mseq]: https://math.stackexchange.com/q/3979996/290189
