---
title: Algo avancée 2e journée
author: <a href="https://git.io/vtam">Vincent Tam</a>
date: 18 avril 2023
published: false
---

# Template `ExoN.java`

```java
import java.util.Locale;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.Objects;

/*
 * Method description
 *
 * @author vtam
 */
public class ExoN {
    public static void main(String[] args) {
        // get console input for array, adjust array's type if needed
        int arrayLength = getIntInput("Entrez la taille du tableau : ");
        double[] array = new double[arrayLength];
        double target = 0;
        for (int i = 0; i < array.length; i++) {
            array[i] = getDoubleInput("Entrez l'élément du tableau à l'indice " + i + ": ");
        }

        // write your logic here

        // display each array element in an entier line
        for (int i = 0; i < array.length; i++) {
            System.out.println("tableau[" + i + "] = " + array[i]);
        }
    }

    // auxiliary functions to obtain user input and to handle
    // InputMismatchException
    public static void displayError(String message) {
        System.err.println(message);
        System.exit(-1);
    }

    public static int getIntInput(String message) {
        return getIntInput(message, "Erreur de saisie.");
    }

    public static int getIntInput(String message, String errorMessage) {
        System.out.print(message);
        Scanner input = new Scanner(System.in).useLocale(Locale.US);
        try {
            return input.nextInt();
        } catch (InputMismatchException e) {
            displayError(errorMessage);
        }
        return -1; // useless return to pass the compiler
    }

    public static double getDoubleInput(String message) {
        return getDoubleInput(message, "Erreur de saisie.");
    }

    public static double getDoubleInput(String message, String errorMessage) {
        System.out.print(message);
        Scanner input = new Scanner(System.in).useLocale(Locale.US);
        try {
            return input.nextDouble();
        } catch (InputMismatchException e) {
            displayError(errorMessage);
        }
        return -1; // useless return to pass the compiler
    }
}
```

# Remarques pour le template

- C'est trop compliqué de mettre le type souhaité dans le paramètre.  La
générique ne va pas bien avec les types primitifs.
- C'est encore plus compliqué avec les types de retour.
- Je n'ai pas utiliser `printf()` car mes camarades n'ont pas vu ça.
- `new Scanner(Scanner.in).useLocale(Locale.US)` permet la saisie d'une valueur
décimale avec un point `.` à la place d'une virgule `,`.
- J'ai ajouté des commentaires pour indiquer la région que l'on va chercher à
éditer.

# Exo 4 : somme des éléments dans le tableau

```
<TypeNumérique> FONCTION somme(T : tableau[max] de type numérique)
DEBUT
  VAR i, somme : ENTIER
  somme ← T[0]
  POUR i ALLANT DE 1 à max-1 PAR PAS DE 1 FAIRE
    somme ← somme + T[i]
  FINPOUR
  RETOURNER somme
FIN
```

Bon usage :

- préférer l'anglais
- préférer `i` pour la variable de boucle puisque c'est concis.
- code le plus concis possible
- j'ai essayé le codage en Java avec les génériques mais
[il n'y a pas d'intérêt][so58390115] de faire ça.

# Exo 5 : tri par sélection

- rechercher le plus petit élément dans le tableau et l'échanger avec l'élément
d'indice 0.
- rechercher le second plus petit élément et l'échanger d'indice 1.
- continuer la telle façon jusqu'au tri complet du tableau.

Difficultés :

- [Scanner la saisie décimale avec la virgule `,`][so17150627].

# Exo 5 : code Java

```java
int arrayLength = getIntInput("Entrez la taille du tableau : ");
double[] array = new double[arrayLength];
double target = 0;
for (int i = 0; i < array.length; i++) {
    array[i] = getDoubleInput("Entrez l'élément du tableau à l'indice " + i + ": ");
}

double curMin = 0;
double temp = 0;
int curMinIdx = 0;

// loop on diminishing tails' head
for (int i = 0; i < array.length - 1; i++) {
    curMin = array[i]; // min in beheaded tail
    temp = 0; // temp var for swapping
    curMinIdx = 1; // index for curMin
    for (int j = i + 1; j < array.length; j++) {
        if (array[j] < curMin) {
            curMin = array[j];
            curMinIdx = j;
        }
    }
    // swap if head > curMin in tail
    if (curMinIdx > 1) {
        temp = array[i];
        array[i] = array[curMinIdx];
        array[curMinIdx] = temp;
    }
}

// display each array element in an entier line
for (int i = 0; i < array.length; i++) {
    System.out.println("tableau[" + i + "] = " + array[i]);
}
```

# Exo 6 : tri par insertion

```java
int arrayLength = getIntInput("Entrez la taille du tableau : ");
double[] array = new double[arrayLength];
double target = 0;
for (int i = 0; i < array.length; i++) {
    array[i] = getDoubleInput("Entrez l'élément du tableau à l'indice " + i + ": ");
}

// loop on diagonal element array[i]
double diagonal = 0;
int insIdx = 0;
for (int i = 1; i < array.length; i++) {
    diagonal = array[i];
    insIdx = i;
    // find insertion element in growing heads
    while (insIdx > 0 && diagonal < array[insIdx - 1]) {
        array[insIdx] = array[insIdx - 1];
        insIdx--;
    }
    // insert diagonal element in the "hole" array
    array[insIdx] = diagonal;
}

// display each array element in an entier line
for (int i = 0; i < array.length; i++) {
    System.out.println("tableau[" + i + "] = " + array[i]);
}
```

[so58390115]: https://stackoverflow.com/questions/58390115/is-there-a-prevalent-technique-to-sum-the-elements-of-an-array-having-generic-ty#comment103127046_58390115
[so17150627]: https://stackoverflow.com/a/17150679/3184351
