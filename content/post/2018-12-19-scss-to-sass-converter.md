---
title: "SCSS to SASS Converter"
date: 2018-12-19T01:56:58+01:00
type: post
categories:
- technical support
tags:
- SASS
- SCSS
draft: false
---

### Background

While [porting Minimal Mistakes and Beautiful Hugo's Staticman support][port]
to [Introduction][intro], I searched for "SCSS to SASS" on [DuckDuckGo][ddg].
The search engine returned results on "CSS to SASS/SCSS" or _vice versa_.  The
[first theme][mm] has Staticman code in SCSS.  That would fit into the
[third theme][intro]'s directory structure, which puts SASS files under
`assets/sass`.  However, SASS _doesn't_ have curly braces `{}`.  I feared that
after hours of tedious manual replacement, the code would fail to run.  As a
consequence, I conducted such Internet search.

### Solution

Scared by the error message in [the recent failed GitLab CI build][bjci] of
a _clone_ of [Beautiful Jekyll][bj], I _don't_ want to install
[Jekyll SASS Converter][gem] as a Ruby gem.

I clicked on the link to [an online CSS to SASS converter][webapp1].  I
_guessed_ there's an analogue for SCSS to SCSS, so I replaced `css2sass` with
`scss2sass` in the previous link, and [**this worked**][webapp2]!

I've finally obtained [a desired converted SASS file][sassfile].  However, I
_don't_ know why this file _only works locally_.

[port]: https://gitlab.com/VincentTam/introduction/commit/d44709cf1106c5ea4282234555a47c90961ae877
[intro]: https://github.com/vickylai/hugo-theme-introduction
[ddg]: https://start.duckduckgo.com
[mm]: https://mademistakes.com/work/minimal-mistakes-jekyll-theme/
[bjci]: https://gitlab.com/VincentTam/beautiful-jekyll/-/jobs/134051594
[bj]: https://deanattali.com/beautiful-jekyll
[gem]: https://rubygems.org/gems/jekyll-sass-converter/versions/1.5.2
[webapp1]: https://css2sass.herokuapp.com/
[webapp2]: https://web.archive.org/web/20181010075526/http://scss2sass.herokuapp.com/
[sassfile]: https://gitlab.com/VincentTam/introduction/commit/9666cba3df9ea20f74fb6f9dda6242477b94f914#6a33ebc96fe34ba07fb0c4c4badfc095f811f9d5
