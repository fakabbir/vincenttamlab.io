---
title: "LaTeX Table Background Color for TeXit"
date: 2022-06-10T11:54:47+02:00
categories:
- technical support
tags:
- LaTeX
- TeXit
draft: false
---

### Problem

In [my previous post about LaTeX tables][pp], I used `\cellcolor{white}` to
override the `\rowcolor{cyan}` in a cell in the leftmost column, which was the
column type name.

On Discord, that cell became black because TeXit inverted black and white.

![LaTeX table with TeXit problem](pblm.png)

### Discussion

I first tried blending black and white, and I found that `black!27` and
`black!28` give a good approximation to Discord's background color.

Thanks to this [*Discord Color Palette*][discord_colors] and a bot command
channel on a server, I've observed that the desired color is RGB(54,57,62).

### Solution

To use a RGB color in LaTeX, it's better to use the packages `color` and
`xcolor`.

| option | range |
| :--: | :-- |
| `{RGB}` | ⟦ 0,255 ⟧ |
| `{rgb}` | [0,1] |

![LaTeX table with TeXit](soln.png)

The code for TeXit can be copied from the previous post, with the following
adaptations:

1. `\definecolor{mybg}{RGB}{54,57,62}`: you may change `mybg` to any string.
This line should appear before `\begin{table}`.
2. `\cellcolor{mybg}`: use the previously defined color.  The `mybg` here should
match your custom name defined in (1).

[pp]: /post/2022-06-09-latex-multirow-multicol-table-coloring/
[discord_colors]: https://www.color-hex.com/color-palette/28549
