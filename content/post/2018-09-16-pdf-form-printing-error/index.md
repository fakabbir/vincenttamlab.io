---
title: "PDF Form Printing Error"
subtitle: "How to avoid wasting paper?"
date: 2018-09-16T01:57:12+02:00
type: post
categories:
- technical support
tags:
- PDF
- printing
draft: false
---

### Background

{{< beautifulfigure src="/post/2018-07-23-fujitsu-lh532-keyboard-cleaning/marais-small.jpg" link="/post/2018-07-23-fujitsu-lh532-keyboard-cleaning/20180711_215200.jpg" title="Le Temple du Marais" caption="\"Eglise verte\" means \"green church\"." alt="Le Temple du Marais" >}}

My churches are going <span class="green">green</span>.

### Problem

<i class="fas fa-download fa-fw" aria-hidden></i>
<i class="far fa-file-pdf fa-fw" aria-hidden></i>
<i class="far fa-save fa-fw" aria-hidden></i> →
<i class="fab fa-usb fa-fw" aria-hidden></i>
<i class="fas fa-print fa-fw" aria-hidden></i>
I downloaded [a PDF][pdf_form] from Haute-Garonne's government site, filled in
the form and saved it on a USB key. Then I printed it at a Konica Minolta bizhub
photocopier <time datetime="2018-09-15 17:00">yesterday</time>.

<span class="fa-stack fa-2x fa-pull-left">
  <i class="fas fa-language fa-stack-1x"></i>
  <i class="fas fa-ban fa-stack-2x red"></i>
</span>
However, _only_ the radio buttons <input type="radio"> and checkboxes
<input type="checkbox"> were printed.  The blanks <input type="text" readonly>
were left _blank_!  I opened my PDF and double-checked that I had input the text
in <input type="text" value="all text fields" readonly>.

{{< beautifulfigure link="prerr.jpg" thumb="-s" title="Printed output" caption="Printed by a Konica Monilta bizhub photocopier" alt="PDF file printed" >}}

How can I get back the missing texts _printed_ on paper?

### Environmental impact

Before any technical discussions, let's imagine that the
<i class="far fa-file-pdf" aria-hidden></i> form created by
[LibreOffice&nbsp;5.0][LO5] contains a lot of pages.

![PDF created by LibreOffice 5.0](LO5.png)

Such an error would trash the majority of the printed pages, unless someone is
willing to give a helping hand to our planet Earth by copying the text from
<input type="text" value="all text fields" readonly> to paper.

### Discussion

As the new term has already started, I've _no_ interest to look into the cause
of the error.  Out of curiosity, I tried
[`qpdf`](/page/bash-commands/index.html#qpdf) to extract the first three pages
of the <i class="far fa-file-pdf" aria-hidden></i> file.

```
$ qpdf --empty --pages input.pdf 1-3 -- output.pdf
```

In the output <i class="far fa-file-pdf" aria-hidden></i> file, the
<input type="text" readonly>'s were _missing_!  It _seemed_ that
[`qpdf`](/page/bash-commands/index.html#qpdf) could detect the aforementioned
problem.

### Solution

The actual solution sounds ridiculous, but trust me, it works!

> <i class="fas fa-quote-left fa-lg fa-pull-left"></i>
> Print the PDF file to file.
> <i class="fas fa-quote-right fa-lg fa-pull-right"></i>

![Evince print dialog](print-dialog.png)

Verify the newly generated <i class="far fa-file-pdf" aria-hidden></i> file by

1. opening it with a PDF viewer
2. extract some pages from it

    ```
    $ qpdf --empty --pages input.pdf 1-3 -- output.pdf
    ```

3. open the extracted pages with a PDF viewer

### Final remark

1. Print from <i class="far fa-file-pdf" aria-hidden></i> document viewer (e.g.
[Evince], [Sumatra&nbsp;PDF][sumatra]).  _Don't_ print from a web browser so
that the fonts _won't_ be rendered in an odd way.
2. If possible, preview the printing job.
3. Print first few pages for testing.

[pdf_form]: https://web.archive.org/web/20180517065538/http://www.haute-garonne.gouv.fr/content/download/25422/176183/file/FormulaireEtudiantsEtrangers.pdf
[LO5]: https://www.libreoffice.org/
[Evince]: https://wiki.gnome.org/Apps/Evince
[sumatra]: https://www.sumatrapdfreader.org/free-pdf-reader.html
