---
title: Staticman Invitation is Case Sensitive
subtitle: "A recent invitation error under Staticman v3"
date: 2018-12-19T16:08:19+01:00
type: post
categories:
- blogging
tags:
- Staticman
- GitHub
---

HTML and URL links are _case insensitive_.  For example, GOOGLE.COM and
google.com give the _same_ address.  As a result, after creating
[my fork of Beautiful Jekyll][bjfork], I invited [@staticmanlab][slab]
to join my GitHub repo by firing the URL

    https://staticman3.herokuapp.com/v3/connect/github/vincenttam/beautiful-jekyll

but I got

{{< gallery >}}
  {{< beautifulfigure thumb="_s400" link="invite1.png" title="Failed Staticman invitation" caption="The case for GitHub user name doens't match" alt="failed Staticman Lab invitation" >}}
  {{< beautifulfigure thumb="_s400" link="invite2.png" title="Successful Staticman invitation" caption="The case for GitHub user name match" alt="successful Staticman Lab invitation" >}}
{{< /gallery >}}

After matching `vincenttam` with the case of my user name displayed in the link
for each of [GitHub] repos,

    https://staticman3.herokuapp.com/v3/connect/github/VincentTam/beautiful-jekyll

{{< beautifulfigure src="invite3.png" title="Collaborator successfully added" caption="Screenshots compressed by https://tinypng.com/" alt="github collaborator" >}}

[bjfork]: https://github.com/VincentTam/beautiful-jekyll/
[slab]: https://github.com/staticmanlab
[GitHub]: https://github.com
