---
title: "Quels schémas à tester ?"
date: 2019-04-11T11:49:01+02:00
type: post
categories:
- math
tags:
- stage
- numerical analysis
- simulation
draft: false
---

Pour simuler les trajectoires d'une équation différentielle, le schéma le plus
facile est l'un découvert par Euler.

Cependant, en espérant mettre ce que j'ai vu au cours de ma licence en pratique,
j'ai cherché un peu sur des méthodes numériques pour les EDS sur Google, et je
suis tombé sur [un article écrit par Brian D. Ewald][1].

Les différences entre les schémas « forts » et « faibles » seraient-ils
pertinents pour mon stage ?  On va voir.

[1]: https://www.math.fsu.edu/~ewald/006-WeakVersionsOfStochastiAdamsBashforthAndSemiImplicitLeapfrogSchemes.pdf
