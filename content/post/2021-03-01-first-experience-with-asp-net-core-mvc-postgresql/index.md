---
title: "First Experience with ASP.NET Core MVC & PostgreSQL"
date: 2021-03-01T17:29:50+01:00
categories:
- technical support
tags:
- CSharp
- PostgreSQL
- Entity Framework
---

### Goal

To build an ASP.NET Core 5 MVC web app linked with a PostgreSQL.

### Motivation

1. SQL Server is proprietary.
2. [SQLite used in Microsoft's ASP.NET Core 5 MVC tutorial][mstut] isn't made
for web apps.
3. [MySQL doesn't perform well with concurrent read-writes][dgo].  It's
dual-licensed like GitLab.
4. [Some users find PostgreSQL cost-effective][reddit].

### Useful tutorials

1. MS's tutorial in item 2 above.
2. [Wes Doyle's YouTube video][yt] goes through the steps
3. [MS's tutorial for Razor Pages with EF Core migrations][mstut2]

### Steps

0. Create a superuser in the database.

    ```sh
    sudo -u postgres psql
    ```

    By default the user `postgres` is the admin of the DBMS.

    ```sql
    CREATE USER mydev WITH SUPERUSER;
    ALTER USER mydev WITH PASSWORD 'my0eV';
    ```

    Then we go to pgAdmin and grant super user `mydev` all privileges.

1. `dotnet mvc -o Foo`
2. Create framework-independent model `Models/Movie.cs`.

    ```cs
    using System;
    using System.ComponentModel.DataAnnotations;

    namespace Foo.Models
    {
        public class Movie
        {
            public int Id { get; set; }
            public string Title { get; set; }

            [DataType(DataType.Date)]
            public DateTime ReleaseDate { get; set; }
            public string Genre { get; set; }
            public decimal Price { get; set; }
        }
    }
    ```

3. Add necessary NuGet Packages.

    ```sh
    dotnet tool install --global dotnet-ef
    dotnet add package Microsoft.EntityFrameworkCore.Tools
    dotnet add package Microsoft.EntityFrameworkCore.Tools.Dotnet
    dotnet add package Npgsql.EntityFrameworkCore.PostgreSQL
    ```

    The `Foo.csproj` file will automatically get updated.

4. Create a database context class `Data/MovieContext.cs`.  It's responsible for
connecting the database with the app.

    ```cs
    using Microsoft.EntityFrameworkCore;
    using Foo.Models;

    namespace Foo.Data
    {
        public class MvcMovieContext : DbContext
        {
            public MvcMovieContext (DbContextOptions<MvcMovieContext> options)
                : base(options)
            {
            }

            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder
                .UseNpgsql("Host=localhost;Port=5432;Database=MvcMovie;Username=mydev;Password=my0eV;Integrated Security=true;Pooling=true")
                .UseSnakeCaseNamingConvention();

            public DbSet<Movie> Movies { get; set; }
        }
    }
    ```

    The `UseSnakeCaseNamingConvention` method converts `ReleaseDate` in the
    model class to `release_date` in the final database.

    `DbSet<TE> TEs` is a space to hold instances of `TEs`.

    I got stuck by mistakingly putting the `OnConfiguring` method inside the
    constructor.

5. Startup settings.  In the `Startup.ConfigureServices` method, register the
database context with the service during app startup.

    ```cs
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllersWithViews();

        services.AddDbContext<MvcMovieContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("MvcMovieContext")));
    }
    ```

    At the top of `Startup.cs`, we add two packages.

    ```cs
    using Microsoft.EntityFrameworkCore;
    using Foo.Data;
    ```

    There's no need to import `Npgsql.EntityFrameworkCore.PostgreSQL`.  The
    package import in the `Foo.csproj` file in step 3 will do.

6. Add the `ConnectionStrings` in `appsettings.json`.  In the YouTube videos,
it appears as `Server` and `User Id`, but after a code search in the package's
GitHub repo, I'm convinced that the documented connection strings parameters are
valid.

    ```json
    "AllowedHosts": "*",
      "ConnectionStrings": {
        "MvcMovieContext": "Host=localhost;Port=5432;Database=MvcMovie;Username=mydev;Password=my0eV;Integrated Security=true;Pooling=true"
    }
    ```

7. Execute the following two commands.  The build should succeed in seconds.

    ```sh
    dotnet ef migrations add InitialCreate
    dotnet ef database update
    ```

    {{< beautifulfigure src="210301-dep.png" alt="command tool dotnet-ef succeeded" >}}

    I forgot to install `dotnet-ef`, so those two didn't run at first.

8. A new database `MvcMovie` is generated.  The table `movies` is empty.
Observe that the columns are in snake case: `ReleaseDate` in the model class
file is converted to `release_date`.  That's a common practice in PostgreSQL.

    {{< beautifulfigure src="210301-mg.png" alt="EFMigrationHistory table shows migration details" >}}

    {{< beautifulfigure src="210301-res.png" alt="The generated database with column in snake case" >}}

### Source code

View the [related commit][11b9].

[mstut]: https://docs.microsoft.com/aspnet/core/tutorials/first-mvc-app/adding-model?view=aspnetcore-5.0&tabs=visual-studio-code
[dgo]: https://www.digitalocean.com/community/tutorials/sqlite-vs-mysql-vs-postgresql-a-comparison-of-relational-database-management-systems
[reddit]: https://www.reddit.com/r/dotnet/comments/8alghr/anyone_using_postgres_or_mysql_with_net_core/
[yt]: https://youtu.be/md20lQut9EE
[mstut2]: https://docs.microsoft.com/en-us/aspnet/core/data/ef-rp/migrations?view=aspnetcore-5.0&tabs=visual-studio-code
[11b9]: https://gitlab.com/VincentTam/samplemvcapps/-/commit/11b939fb
