---
title: Mesurabilité des réalisations trajectorielles
subtitle: "$X: \\omega \\mapsto X(\\cdot, \\omega) \\in \\mathcal{M}((\\Omega, \\mathcal{A}), (\\CO(\\Bbb{T},\\R), \\Bor{\\CO}))$"
date: 2018-10-11T23:20:37+02:00
type: post
categories:
- math
tags:
- probability
draft: false
---

### Notations

Supposons _toutes_ les notations dans [_Espace de trajectoires_][pp].

### Problématique 

La mesurabilité de l'application dans le sous-titre est basée sur l'égalité
suivante. 

<div>
$$
\Bor{\R}{\OXT} \cap \CO = \Bor{\CO}
$$
</div>

J'ai passé _quatres heures_ pour comprendre

- pourquoi ça entraîne la mesurabilité ?
- pourquoi l'égalité elle-même est vraie ?

### Réponses
#### Mesurabilité de la trace sur $\CO$ de $\Bor{\R}{\OXT}$

A la première lecture, je ne connaisais même pas la définition de la _trace_
d'une tribu sur un emsemble.  En effet, c'est une définition _universaire_ sur
des ensembles, selon [une question sur la trace][mathse-trace] sur Math.SE.

J'ai médité la structure de $\Bor{\R}{\OXT}$ afin de comprendre la mesurabilité
de l'application « $\mapsto$ ».  En fait, c'est due à la définition d'une
fonction _aléatoire_ réelle continue.

> fonction aléatoire réelle continue
> : une fonction aléatoire réelle $X : (t, \omega) \mapsto X(t, \omega) \in \R$
> telle que « tous les trajectoires sont continues (par rapport à $t$) ».

Plus précisément, la réponse est cachée dans le mot « aléatoire », ce qui permet
à l'image réciproque $X^{-1}[B]$ d'un borélien $B \in \Bor{\R}$ d'entréer dans
$\mathcal{A}$.  Les élements dans $\Bor{\R}{\OXT}$ est juste des ensembles
engendrés par des « produits directs/cylindres de dimensions finies ».  Comme
chauqe borélien composant (indexé par $t \in \Bbb{T}$, appartenant à $\Bor{\R}$)
entre facilement dans $\mathcal{A}$, on voit que le membre à gauche dans
l'égalité au-dessus entraîne la mesurabilité de l'application « $\mapsto$ ».

#### Vérité de l'égalité
##### Inclusion directe

La démonstration commence par

> « pour tout $t$, l'application coordonnée $x\mapsto x(t)$ ... est continue ... »

Je ne pouvais pas comprendre son raisonnement : pourquoi commencer par une
application coordonnée ?  Alors, la suite de cette partie ne me paraît
compréhensible.

J'ai cherché [le polycopié par Ron Peled][poly], mais je n'ai pas pu en sortir
une indication.  Mon prof m'a dit d'imaginer une fonction $x \in \CO$ comme un
vecteur indexé par l'instant $t \in \Bbb{T}$.  Néanmoins, j'y avais du mal car
selon la définition de l'application « $\mapsto$ », c'est la trajectoire
continue $x$ qui bouge, alors que l'instant $t$ est fixé.

Après avoir passé une heure de méditation sur les cylindres ouverts de
dimensions finies, je me suis rappelé des _projections_ dans un espace produit
muni de _la topologie produit_, grâce à ma lecture de la preuve du
[théorème de Tychonov][tyt] par Nicolas Bourbaki présentée dans le 2e chap. du
livre d'analyse réelle et de proba de Dudley.

<div>
$$
\begin{aligned}
\left(\prod_{i \in I} X_i,\mathcal{T}\right) & \xrightarrow{\text{projection } \pi_i}
(X_i,\mathcal{T}_i) \\
x & \xmapsto{\pi_{i_1}} x(i_1) \\
x & \xmapsto{\pi_{i_2}} x(i_2)
\end{aligned}
$$
</div>

La topologie produit $\mathcal{T}$ est _la plus petite topologie rendant toutes
les projections $(\pi\_i)\_{i \in I}$ continues_.  Autrement dit, on fixe les
projections $(\pi\_i)\_{i \in I}$ et les topologies $(\mathcal{T}\_i)\_{i \in
I}$ dans les espaces composants $(X\_i, \mathcal{T}\_i)\_{i \in I}$.
Construisons une prébase topologique sur l'espace produit $\prod\_I X$ par
l'image réciproque des ouverts dans $X\_i$ par $\pi\_i$ pour $i \in I$.  Cette
prébase engendre $\mathcal{T}$.  S'il existe une topologie $\mathcal{T}^\prime$
sur $\prod\_I X$ telle que pour tout $i \in I$, la projection $\pi\_i: \left(
\prod\_I X, \mathcal{T}^\prime \right) \to (X\_i, \mathcal{T}\_i)$ est continue,
on aura $\mathcal{T} \subseteq \mathcal{T}^\prime$.

Quitte à remplacer

- $i \in I$ par $t \in \Bbb{T}$
- « ouverts » par « boréliens »
- $(\prod_{i \in I} X_i, \mathcal{T}^\prime)$ par $(\CO, \Bor{\CO})$
- $(X_i, \mathcal{T}_i)$ par $(\R, \Bor{\R})$

on aura l'inclusion souhaitée car $\Bor{\R}{\OXT}$ joue un rôle similaire à
$\mathcal{T}$ au paragraphe précédent.

##### Inclusion réciproque

J'ai mal interprêté « les boules » dans cette partie car les auteurs ne
précisent pas où elles demeurent.  Mon prof m'a dit qu'ils avaient préféré les
boules fermées $B(y, \epsilon) \in \Bor{\CO}$ puisque la limite d'une suite dans
une boule fermée ne sortait pas de la boule.

Puis, ils ont décomposé la boule fermée en l'intersection dénombrable de
cyclindres ouverts unidimensionels dans $\Bor{\R}{\OXT}$.

<div>
$$
\begin{aligned}
B(y, \epsilon)
&= \bigcap_{t\in\Bbb{T}} \{ x \in \CO \bigm\vert |x(t)-y(t)| \le \epsilon \} \\
&= \bigcap_{n>0} \bigcap_{t \in \Bbb{T} \cap \Q}
\left\{ x \in \CO \biggm\vert |x(t)-y(t)| < \epsilon + \frac1n \right\}
\end{aligned}
$$
</div>

La dernière transition est justifiée par la densité de $\Q$ dans $\R$.  Pour
trouver des cylindres qui engendrent $\Bor{\R}{\OXT}$, décrivons l'ensemble
derrière les signes $\bigcap$ au dernier membre en mots : pour une trajectoire
continu $y \in \CO$ et un instant $t \in \Bbb{T}$, cet ensemble désigne tous les
trajectoires continues $x \in \CO$ qui est proche à $\epsilon + 1/n$ près de $y$
à l'instant $t$.  On ne regarde $x$ qu'à l'instant $t$, et on le traite encore
comme un vector indexé par $t^\prime \in \Bbb{T}$.  (Dans ce paragraphe, $t$ est
_fixé_, donc je mets un trait pour désigner $t^\prime$ qui _bouge_ dans
$\Bbb{T}$.)  Ce dernier ensemble s'écrit comme

<div>
$$
\left\{ x \in \CO \biggm\vert \pi_t^{-1}\left(\left[ y(t)-\epsilon-\frac1n,
y(t)+\epsilon+\frac1n \right]\right) \right\}
= \prod_{t^\prime \in \Bbb{T}} A_{t^\prime},
$$
</div>

où $\pi\_t : \CO \to \R$ désigne la projection canonique comme dans la partie,
et

<div>
$$
A_{t^\prime} =
\begin{cases}
\left[ y(t)-\epsilon-\frac1n, y(t)+\epsilon+\frac1n \right]
&\quad \text{if } t^\prime = t \\
\R &\quad \text{otherwise}
\end{cases}
$$
</div>

### Référence

1. Le livre de calcul stochastique par Comets et Meyre
2. [Le polycopié sur la tribu pour des mouvements browniens par Ron Peled][poly]

<i class="fas fa-exclamation-triangle fa-lg fa-pull-left" aria-hidden></i>
_La projection canonique_ d'un espace produit $\prod_{i \in I} X_i$ vers un espace composant
$X_i$ _n'est forcément pas mesurable_, selon
[la question sur Math.SE de Ethan][mathse].

[pp]: /post/2018-09-28-espace-de-trajectoires/
[mathse-trace]: https://math.stackexchange.com/q/2945805/290189
[poly]: http://www.math.tau.ac.il/~peledron/Teaching/Brownian_motion/Sigma_algebra_notes.pdf
[tyt]: https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Tykhonov
[mathse]: https://math.stackexchange.com/q/78628/290189
