% Algo avancés
% [Vincent Tam](https://git.io/vtam)
% 17 avril 2023

# Métier du dév

analyste programmeur → développeur

- analyste ≠ programmeur
- maintenant appelé « développeur »

# Phases d'un projet en schéma

1. besoin client (client)
1. cahier de charges (MOA, AMOA)
1. SF (MOA, AMOA)
1. SFD (MOA, AMOA)
1. ST (CPI, AMOE)
1. STD (CPI, AMOE)
1. algo (dév, AMOE)
1. prog exe'b (dév, AMOE)
1. texts : tests unitaires, tests d'intégration
1. recettes (client)
1. …
1. livrason (e.g. inauguration d'un pont)

- pas de règle sur le nombre de pages des livrables (`S[TF](D?)`)
- cycle en V existe en génie civil avant l'informatique
- dans l'agile, SFD -- STD → user stories (PO = product owner)
    + scrum master
    + daily standing meetup de 15mn

# Projet exemplaire : moteur de recherche

1. besoin : mot clé → { liens = sites }
1. SF : site → mots clés → score
1. ST : site → mots clés : **parsing** (analyse sémantique) → BD

# Exemples de langages descriptifs

Langage qui décrit, qui ne donne pas d'instruction procédurale.

1. CSS
1. HTML
1. $\LaTeX$

Questions :

1. descriptifs vs déclaratif : ?
1. CSS anim, interfaces fonctionnelles dans $\LaTeX{}3$? : regarder les bases
1. SQL ? : il y a de la syntaxe procédurale dedans

# Exos : fct factorielle

Voir l'[exo du 3e jour][exo_3e_jour]

Comparaison entre les deux versions :

- itérative : plus opérative
- récursive : plus mathématique et jolie

Exos en binôme : j'étais en charge de la partie itérative.

# Exos : fct factorielle (cont.)

```java
import java.util.Scanner;
import java.util.InputMismatchException;

public class IterativeFactorial {
    public static void main(String[] args) {
        System.out.print("Entrez un entier positif : ");
        Scanner input = new Scanner(System.in);
        try {
            int n = input.nextInt();
            if (n < 0) {
                displayError();
                return;
            }
            long result = 1;
            for (int i = 2; i <= n; i++) {
                result *= i;
            }
            System.out.println(String.format("%d! = %d", n, result));
        } catch (InputMismatchException e) {
            displayError();
        }
    }

    private static void displayError() {
        System.err.println("Erreur de saisie.  Veuillez entrez un entier positif.");
    }
}
```

# Exos : nombre triangulaire

Ecrire une fonction telle que $v_1 = 1$ et $v_n = v_{n-1} + n$ pour $n > 1$.

```
FONCTION nomber_triangulaire(n)
DEBUT
  SI (n < 0) ALORS
    erreur mismatch
    SORT DE LA FONCTION
  FIN
  résultat ← 0
  POUR i ALLANT DE 1 A n PAR PAS DE 1 FAIRE
    résultat + i
  FINPOUR
  RETOURNER résultat
FIN
```

Observations :

- si `n = 0`, on n'entre pas dans la boucle `POUR`.

# Exo : nombre triangulaire (cont.)

```java
import java.util.Scanner;
import java.util.InputMismatchException;

public class TriangularNumber {
    public static long triangularNumber(int n) {
        if (n < 0) {
            displayError();
        }
        long result = 0;
        for (int i = 1; i <= n; i++) {
            result += i;
        }
        return result;
    }

    private static void displayError() {
        System.err.println("Erreur de saisie.  Veuillez entrez un entier positif.");
        System.exit(-1);
    }

    private static int getIntInput() {
        System.out.print("Entrez un entier positif : ");
        Scanner input = new Scanner(System.in);
        try {
            return input.nextInt();
        } catch (InputMismatchException e) {
            displayError();
        }
        return -1; // useless return to pass the compiler
    }

    public static void main(String[] args) {
        int n = getIntInput();
        System.out.println(String.format("v_{%d} = %d", n, triangularNumber(n)));
    }
}
```

# Tableau

|  |  |  |  |  |  | ↓ |  |  |  |  |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| 7 | 3 | 5 | 1 | 5 | 5 | 4 | 3 | 9 | 8 | 7 |

Exo 1 :

Ecrire une fonction qui initialise un tableau de 10 éléments à la valeur
précisée en paramètre.

```
FONCTION tableau10(ENTIER i)
DEBUT
  TABLEAU résultat ← TABLEAU VIDE DE TYPE ENTIER DE TAILLE 10
  POUR compteur ALLANT DE 1 A 10 PAR PAS DE 1 FAIRE
    résultat[compteur] ← i
  FINPOUR
  RETOURNER résultat
FIN
```

# Tableau (cont.)

```java
import java.util.Scanner;
import java.util.InputMismatchException;

public class Tableau10 {
    public static void tableau10(int n, int[] result) {
        for (int i = 0; i < result.length; i ++) {
            result[i] = n;
        }
    }

    public static void main(String[] args) {
        int n = getIntInput("Entrez un entier positif : ",
              "Erreur de saisie.  Veuillez entrez un entier positif.");
        int[] tableauDix = new int[10];
        tableau10(n, tableauDix);
        for (int i = 0; i < tableauDix.length; i ++) {
            System.out.printf("%d → %d\r\n", i, tableauDix[i]);
        }
    }

    // auxiliary functions to obtain user input and to handle
    // InputMismatchException
    private static void displayError(String message) {
        System.err.println(message);
        System.exit(-1);
    }

    private static int getIntInput(String message) {
        return getIntInput(message, "Erreur de saisie.");
    }

    private static int getIntInput(String message, String errorMessage) {
        System.out.print(message);
        Scanner input = new Scanner(System.in);
        try {
            return input.nextInt();
        } catch (InputMismatchException e) {
            displayError(errorMessage);
        }
        return -1; // useless return to pass the compiler
    }
}
```

# Tableau Exo 2

Ecrire une fonction qui renvoie la valeur minimum d'un tableau entier.

```
ENTIER FONCTION minimum(T : tableau[max] d'entier)
DEBUT
  VAR i, min : ENTIER
  min ← T[0]
  POUR i ALLANT DE 1 à max-1 PAR PAS DE 1 FAIRE
    SI (T[i] < min)
      min ← T[i]
    FINSI
  FINPOUR
  RETOURNER min
FIN
```

# Tableau Exo 3 : recherche d'un élément

```java
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.Objects;

public class Exo3 {
    public static <T> boolean hasElementIterative(T[] array, T target) {
        for (T entry : Objects.requireNonNull(array)) {
            boolean test = target == null ? entry == null : target.equals(entry);
            if (test) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int arrayLength = getIntInput("Entrez la taille du tableau : ");
        Object[] array = new Object[arrayLength];
        Object target = new Object();
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < array.length; i++) {
            System.out.print("Entrez le contenu de l'élément du tableau : ");
            array[i] = input.nextLine();
        }
        System.out.print("Entrez le contenu de l'élément cible : ");
        target = input.nextLine();

        if (hasElementIterative(array, target)) {
            System.out.printf("L'élément « %s » trouvé dans le tableau.", target);
        } else {
            System.out.printf("L'élément « %s » non trouvé dans le tableau.", target);
        }
    }

    // auxiliary functions to obtain user input and to handle
    // InputMismatchException
    private static void displayError(String message) {
        System.err.println(message);
        System.exit(-1);
    }

    private static int getIntInput(String message) {
        return getIntInput(message, "Erreur de saisie.");
    }

    private static int getIntInput(String message, String errorMessage) {
        System.out.print(message);
        Scanner input = new Scanner(System.in);
        try {
            return input.nextInt();
        } catch (InputMismatchException e) {
            displayError(errorMessage);
        }
        return -1; // useless return to pass the compiler
    }
}
```

[exo_3e_jour]: https://vincenttam.gitlab.io/exo-3e-jour/
