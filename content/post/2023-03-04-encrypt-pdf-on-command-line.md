---
title: "Encrypt PDF on Command Line"
date: 2023-03-04T14:45:05+01:00
categories:
- technical support
tags:
- PDF
- QPDF
draft: false
---

### Background

On M\$ Win\*, I don't have dedicated PDF readers like [Adobe][apr].

During visioconference, one might want to share some downloaded PDFs.  Those
documents can be part of an ebook or notes.  In some situations, they can
provide context to a discussion.

### Problem

1. Under the same folder, you might have
  - `downloaded_ebook.pdf`
  - `personal_info.pdf`
  - `other_personal_stuff.pdf` that you don't want to expose while streaming.

However, each downloaded PDF usually comes with a name set by others.  It can be
tedious to rename them according to your own habits.  To protect users' privacy,
we can

1. encrypt PDFs with a password, and
1. prepend `.enc` before `.pdf` (i.e. `foobar.enc.pdf`) to indicate that the
  file is password protected.

[QPDF] is a free (as in "freedom") command-line utility that has implemented
this functionality.

```
$ qpdf --encrypt upw opw 256 -- input.pdf output.enc.pdf
$ qpdf --password=upw --decrypt output.enc.pdf output.pdf
```

Example copied and paraphrased from [Ask Ubuntu][qpdf_cmd].

It's claimed that `-` can replace user and owner passwords (`upw` and `opw`) in
the command arguments to avoid leaving a trace in the bash history.  Unluckily,
that's not working on Git Bash.

### Workaround

With the help of [Bing ChatGPT][bing_chatgpt], I've explored the possibility of

1. starting a command with a whitespace character, and
1. set the shell variable `HISTCONTROL=ignorespace` temporarily,

so that the command whose first character is a whitespace won't be recorded into
the history, say `~/.bash_history`.  In addition, using the arrow keys `↑` and
`↓` won't return that command.

To end this post, here's the practical implementation.

```
$ HISTCONTROL=ignorespace
$ PDFS='foo1 bar2 doc3'
$ for f in $PDFS; do qpdf --encrypt upw opw 256 -- $f.pdf $f.enc.pdf; done
```

In practice, you might want to change `upw` and `opw` to your user password (for
reading) and your owner password (for editing) respectively.

Remarks:

1. I have never tried `export HISTCONTROL=ignorespace`.
1. I have never tried `HISTCONTROL=` to restore the default value.  I simply
  close and reopen Git Bash.

[apr]: https://www.adobe.com/acrobat/pdf-reader.html
[QPDF]: https://qpdf.sourceforge.io/
[qpdf_cmd]: https://askubuntu.com/a/906399/259048
[bing_chatgpt]: https://www.bing.com/new
