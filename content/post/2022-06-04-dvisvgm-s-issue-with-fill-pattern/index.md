---
title: "Dvisvgm's Issue With Fill Pattern"
date: 2022-06-04T20:56:57+02:00
categories:
- technical support
tags:
- LaTeX
- dvisvgm
- SVG
draft: false
---

### Goal

Generate SVG file for TikZ graphics.

### Method

1. TeX → PDF with PDFLaTeX
2. PDF → SVG with `dvisvgm -P filename`

Example: I generated a PGF plot from the source code on
[my personal template][1].

![working SVG][2]  
![working PDF][3]  
![Git Bash for PDF][4]  
![Git Bash for SVG][5]

### Problem

When I applied this method to the following TeX code in my previous post,
`dvisvgm -P` returns a blank SVG.

```tex
\documentclass[tikz,border=2pt]{standalone}
\usepackage{pgfplots}
\pgfplotsset{compat=1.6}
\usetikzlibrary{patterns}
\begin{document}
\begin{tikzpicture}
\begin{axis}[axis lines=center,legend style={at={(0.7,0.7)},anchor=south west}]
\addplot [domain=-3:3, thick, smooth, yellow] { 1/sqrt(2*pi)*exp(-x^2/2) };
\addlegendentry{$y = \frac{1}{\sqrt{2\pi}} e^{-x^2/2}$};
\addplot [dashed, yellow] coordinates {(1.5,0) (1.5,0.14)};
\addlegendentry{99th percentile};
\addplot[domain=-3:1.5, pattern=north east lines,draw=none, fill opacity=0.3]
  { 1/sqrt(2*pi)*exp(-x^2/2) } \closedcycle;
\end{axis}
\end{tikzpicture}
\end{document}
```

This code passes the compiler.

![pattern PDF][6]  
![empty SVG][7]  
![same cmd in Git Bash][8]

### Technical info

- OS: Windows 10
- MikTeX Console 4.7
- dvisvgm 2.11.1

```
$ dvisvgm -V1
dvisvgm 2.11.1
--------------
brotli:      1.0.9
clipper:     6.2.1
fontforge:   20160721
freetype:    2.10.3
Ghostscript: 9.25
kpathsea:    MiKTeX 22.3
potrace:     2.11.1
xxhash:      0.8.1
zlib:        1.2.11
```

### Related GitHub Issue

[mgieseki/dvisvgm#158][9]: OP used `pattern=bricks` in his LaTeX code.

- failed: TeX → DVI → SVG
- succeeded: TeX → DVI → PS → PDF

He claimed that that's reproducible with TeX Live 2021.

The project owner said that that's a known issue with version 2.11.1, and that's
fixed in later version.

However, I would wait until MikTeX includes this update to avoid compatibility
issues.

  [1]: /page/tikz-templates/
  [2]: XPN8M.png
  [3]: iekQN.png
  [4]: PjQhe.png
  [5]: H3F3b.png
  [6]: SmQu2.png
  [7]: 2McnD.png
  [8]: fNIqT.png
  [9]: https://github.com/mgieseki/dvisvgm/issues/158
