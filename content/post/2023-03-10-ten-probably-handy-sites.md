---
title: "Ten Probably Handy Sites"
date: 2023-03-10T09:34:13+01:00
categories:
- technical support
tags:
- useful sites
draft: false
---

A brief summary of [*10 Incredibly Useful Websites*][src] on [Medium].

[Upword.AI][up] (paid)
: summarise stuff

[Gamma][gamma] (free trial for the moment)
: get professional slides in min, not open source

[IMGCreator][imgai] (free, point-based)
: text/img → img, chatGPT powered

[Popsy][popsy] (free to create, paid to publish)
: no-code website buider

[Visual Capitalist][vc] (free)
: explanatory charts

[TypeLit.io][typelit] (free for some œuvres, paid for own book)
: type classics out to train typing

[Zorp.one][zorp] (free for trial, paid for deploy)
: no-code app generator for businesses, e.g. delivery, store management, doc
collection

[Kialo][kialo] (free)
: repository for debate arguments, with discussion topology

[Nomad List][nomadlist] (free limited trial, paid content)
: good for remote workers, find remote work opportunities, insights into cities,
dating app (that I don't recommend)

[SuperMeme][supermeme] (paid)
: AI meme generator

[src]: https://medium.com/illumination/10-incredibly-useful-websites-that-even-the-best-informed-internet-user-wouldnt-know-b04c8e73efa4
[Medium]: https://medium.com/
[up]: https://www.upword.ai/
[gamma]: https://gamma.app/
[imgai]: https://imgcreator.zmo.ai/ 
[popsy]: https://popsy.co/
[vc]: https://www.visualcapitalist.com/
[typelit]: https://www.typelit.io/
[zorp]: https://www.zorp.one/
[kialo]: https://kialo.com/
[nomadlist]: https://nomadlist.com/
[supermeme]: https://www.supermeme.ai/
