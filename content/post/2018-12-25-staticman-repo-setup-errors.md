---
title: "Staticman Repo Setup Errors"
subtitle: "Common Staticman Config Erorrs in Git repos"
type: post
categories:
- blogging
tags:
- Staticman
date: 2018-12-25T17:07:02+01:00
draft: false
---

### Background

Idem to [*Staticman on Framagit*][pp].

Besides, the build time for [my Staticman + Beautiful Hugo demo][bhdemo] on
[Framagit][time-frama] is _half_ of that for the _same project_ on
[GitLab.com][time-gl].

### Problem

After the setup, it's possible that the theme shows "Sorry!  There's an error
during submission."  To get an insight into this error, one has to open
**Web Developer Tools** → **Network** and select the row corresponding to the
POST request sent to the Staticman API.  A side pane will pop out on the right.
Select **Response** to view the API's response in JSON.

- Missing required fields: the "missing fields" error will occur.
- In case of an _incorrect_ `GIT***_TOKEN` on the _server-side_, or that the
associated GitHub/GitLab account has _not_ been invited as a
"**Collaborator/Developer**", the API will respond with an "unauthorized" error.
- In case of _incorrect_ scopes of the `GIT***_TOKEN` on the _server-side_, the
API will repond with a 404 error `GITHUB_CREATING_PR`.

    ```json
    {
      "JSON": {
        "success": false,
        "rawError": {
          "message": "{\"message\":\"Not Found\",\"documentation_url\":\"https:\/\/developer.github.com\/v3\/git\/refs\/#create-a-reference\"}",
          "statusCode": 404
        },
        "errorCode": "GITHUB_CREATING_PR"
      },
      "Response payload": {
        "EDITOR_CONFIG": {
          "text": "{\"success\":false,\"rawError\":{\"message\":\"{\\\"message\\\":\\\"Not Found\\\",\\\"documentation_url\\\":\\\"https:\/\/developer.github.com\/v3\/git\/refs\/#create-a-reference\\\"}\",\"statusCode\":404},\"errorCode\":\"GITHUB_CREATING_PR\"}",
          "mode": "application\/json"
        }
      }
    }
    ```

- In case of an _incorrect_ user/repo name, a 404 error will be observed on the
left of the entry in **Web Developer Tools** → **Network**.
- In case of an _incorrect_ branch/property name, a "read file" error will be
thrown.
- If the `branch` parameter in `staticman.yml` _doesn't_ match that one
specified in the POST URL, a "branch mismatch" error will occur.
- The most difficult case is "500: Internal server error" with `{"success":
false}`.  This gives _no_ information about what's going wrong.  After two days
of experiments with `staticman.yml` while setting up
[Staticman-powered GitLab Pages on Framagit][frama-grp], I have discovered
several possible causes for such error.  They are all due to incorrect config
parameters in `staticman.yml`.
  + `allowedFields` _doesn't_ match the name of the input fields in the POST
  request.  (i.e. blog theme template)
  + _incorrect_ `reCaptcha` parameters: if `enabled` is set to `false`, one has
  to empty the `siteKey` and `secret` with the empty string `""`.  One _can't_
  directly start a newline after the colon `:`.

  While testing for my new public Staticman instance for Framagit, I had
  forgotten that my another instance for GitLab.com was using a _different_
  private RSA key for the encryption endpoint.Therefore, the reCaptcha `secret`
  copied from my demo repos on GitLab.com _would_ not work.

[pp]: /post/2018-12-24-staticman-on-framagit/
[bhdemo]: https://vincenttam.gitlab.io/bhdemo/
[time-frama]: https://framagit.org/staticman-gitlab-pages/bhdemo/-/jobs/
[time-gl]: https://gitlab.com/VincentTam/bhdemo/-/jobs
[frama-grp]: https://framagit.org/staticman-gitlab-pages/
