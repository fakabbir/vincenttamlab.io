---
title: Better Hugo ToC Fix
subtitle: A JavaScript free way to improve default ToC
date: 2018-09-27T18:56:40+02:00
type: post
categories:
- blogging
tags:
- Hugo
draft: false
---

### Background

I applied [a fix to Hugo's ToC][pp] ten days ago.

### Drawbacks

To make the script non-render blocking, one has to place it in the footer.
As a result, it takes about 0.2 seconds to remove the excess `<ul>` tag.

### Solution

Thanks to [Beej126's Hugo template code][soln], this site delivers table of
contents processed by [Hugo] during [GitLab]'s continuous deployment.

[pp]: /post/2018-09-17-fix-hugo-table-of-contents/
[soln]: https://git.io/fAjiL
[Hugo]: https://gohugo.io/
[GitLab]: https://gitlab.com/
