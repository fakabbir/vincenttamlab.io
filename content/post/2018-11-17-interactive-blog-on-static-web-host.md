---
title: "Interactive Blog on Static Web Host"
date: 2018-11-17T10:09:12+01:00
type: post
categories:
- blogging
tags:
- Hugo
- Staticman
- GitLab
draft: false
---

### Vision

- gain autonomy: freedom is the basis of moral actions.  No freedom, no
morality.
- transcend ourselves: change/improve our lives through free thoughts

### Goal

Convert our free thoughts into free code.

Free code allows users around the world to run and/or improve them.  This would
bring real enhancement to our tools.

For example, beautiful math writing used to be a complicated process.  A decade
ago, this required the installation of a typesetting engine called $\LaTeX$.
Thanks to freely available scripts like [MathJax][1] and [$\KaTeX$][2], it's now
possible to write math viewable by any modern web browser by writing the content
in the middle.

{{< highlight html "linenos=inline">}}
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Your title goes here</title>
</head>
<body>
Write $\LaTeX$ code here.

<!-- End of contents, keep the code below untouched -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0/dist/katex.min.css" integrity="sha384-9eLZqc9ds8eNjO3TmqPeYcDj8n+Qfa4nuSiGYa6DjLNcv9BtN69ZIulL9+8CqC9Y" crossorigin="anonymous">

<!-- The loading of KaTeX is deferred to body to speed up page rendering -->
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0/dist/katex.min.js" integrity="sha384-K3vbOmF2BtaVai+Qk37uypf7VrgBubhQreNQe9aGsz9lB63dIFiQVlJbr92dw2Lx" crossorigin="anonymous"></script>

<!-- To automatically render math in text elements, include the auto-render extension: -->
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0/dist/contrib/auto-render.min.js" integrity="sha384-kmZOZB5ObwgQnS/DuDg6TScgOiWWBiVt0plIRkZCmE6rDZGrEOQeHM5PcHi+nyqe" crossorigin="anonymous"
    onload="renderMathInElement(document.body);"></script>  <script>

<!-- Custom LaTeX like macros -->
document.addEventListener("DOMContentLoaded", function() {
  renderMathInElement(document.body, {
    delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "\\[", right: "\\]", display: true},
    {left: "$", right: "$", display: false},
    {left: "\\(", right: "\\)", display: false}
    ]
  });
});
</script>
</body>
{{</ highlight >}}

P.S. My reason for leaving the code at the end is to allow the content to appear
ASAP, so that users feel that they are capable of creating new content by
changing the code.  The default delimiters are *different* from the usual ones
in $\LaTeX$, which are the *standard* ones used by mathematicians, but the
former can be easily customized to suit the later.  This provides beautiful math
at the expense of an attachment---there's *no* free lunch---one can go further
with Markdown, but mathematicians, one of the most busiest people in the world,
*aren't* so keen to put aside their math and try new technologies.

This serves as an example of how coding can improve our lives.  This is the
collaborate effort of users around the world.  They are granted the rights to
use and improve others' code thanks to free software licenses.

### Problem
#### Dynamic sites

The exchange of ideas played a vital role in the development of tools.  In the
past, we rely on emails, forums and social media.

Obviously, a personal blog is much better for sharing individual ideas.  With a
commenting system, others can exchange ideas with the blog owner.  New ideas
emerges from such exchange.  Comment storage usually require a database, which
*isn't* at the disposal of a *static* web server, which *isn't* supposed to
handle logic from clients' requests.

Well-known third-party solutions like [WordPress.com][3], [Facebook][4] and
[Disqus][5] store comments in external databases that make *comment retrieval
difficult*.  It's difficult to manipulate the contents inside to render math.
WordPress.com do allow math in comments, but the math support is *limited*: no
display style equations cancel much of the convenience provided by the platform.

#### Mathematics Stack Exchange

[Math.SE][6] is a great source questions.  However, it's *not* well-suited for
individual sharing.  The reason is three-pronged.

First, the whole SE network serves as a *knowledge database* in the form of *Q&A
sites*.  A knowledge database stores knowledge, which *isn't* necessarily in the
form of Q&A.  Take [Terence Tao's career advice][7] as an example.  There's no
question, but the information in the post should be useful for math students.

Second, *over 500&nbsp;questions* (mostly from new users) *are closed and/or
deleted every week* since they lack efforts from the question asker, and thus,
are considered to be "off-topic" and "missing context".  Many of them are
*excellent* (calculus) exercises.  I'll illustrate this with
[a question in integration by substitution][8].

> I have tried to solve this
>
> <div>
> $$\int_0^{2\pi} \frac{\cos2x}{5+ 4\cos x} \,{\rm d}x$$
> </div>
>
> without using complex analysis but with no success.  I tried tangent
> half-angle substitution but it became too complicated. Is there a way?

There're two *fierce* wars arised in this flag: an edit/rollback war and a
close/reopen + delete/undelete war.

Third, this site is faced with a plethora of new users who refuses to use
[MathJax][1].  There's [a relatively high-rep user][9] who *doesn't* use
$\LaTeX$ at all.  With a *marginal* reward of two reputation points, plenty of
users under 2,000&nbsp;reputation are willing to offer their time and effort to
improve the readability of the posts written by those users.

### Motivation

I believe that by popularizing the mathematical markup language $\LaTeX$, one is
more willing to incorporate math into their daily communications.  A personal
blog is a useful tool to host the math, and a *static* blog is more stable in
case of huge web traffic because a *static web host* only repond with the
"constant" page.

### Proposal

- Static blog engine & theme with math rendering scripts incorporated:
  [Hugo][10] + [Beautiful Hugo][11]
- Free & open source Git web host: [GitLab][15]
- Static comments: store HTML form data into data files and push to Git repo
  through API: [Staticman][12]

See [my Hugo+Staticman on GitLab guide][13] as welll as
[my recent Beautiful Hugo pull request][14] to get these things done.

[1]: https://www.mathjax.org/
[2]: https://katex.org/
[3]: https://www.wordpress.com/
[4]: https://www.facebook.com/
[5]: https://www.disqus.com/
[6]: https://math.stackexchange.com/
[7]: https://terrytao.wordpress.com/career-advice/
[8]: https://math.meta.stackexchange.com/a/29071/290189
[9]: https://math.stackexchange.com/revisions/2326932/1
[10]: https://gohugo.io/
[11]: https://themes.gohugo.io/beautifulhugo/
[12]: https://staticman.net/
[13]: https://vincenttam.gitlab.io/post/2018-09-16-staticman-powered-gitlab-pages/1/
[14]: https://github.com/halogenica/beautifulhugo/pull/222
[15]: https://gitlab.com
