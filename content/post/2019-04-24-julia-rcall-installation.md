---
title: "Julia Rcall Installation"
subtitle: "Rcall.jl build failure due to missing dependencies"
date: 2019-04-24T15:14:52+02:00
categories:
- technical support
tags:
- Julia
- RCall
- R
draft: false
toc: true
---

# Problem

During the installation of [RCall.jl][1], the package _wasn't_ successfully
built due to a missing variable `$R_HOME`.

    $ julia -q
    julia> Pkg.add("RCall")
     Resolving package versions...
     Installed WinReg ─ v0.3.1
     Installed RCall ── v0.13.2
      Updating `~/.julia/environments/v1.0/Project.toml`
      [6f49c342] + RCall v0.13.2
      Updating `~/.julia/environments/v1.0/Manifest.toml`
      [6f49c342] + RCall v0.13.2
      [1b915085] + WinReg v0.3.1
      Building RCall → `~/.julia/packages/RCall/ffM0W/deps/build.log`
    ┌ Error: Error building `RCall`:
    │ ERROR: LoadError: R_HOME is not a directory.
    │ Stacktrace:
    │  [1] error(::String) at ./error.jl:33
    │  [2] top-level scope at logging.jl:322
    │  [3] top-level scope at /home/vin100/.julia/packages/RCall/ffM0W/deps/build.jl
    :19
    │  [4] include at ./boot.jl:317 [inlined]
    │  [5] include_relative(::Module, ::String) at ./loading.jl:1041
    │  [6] include(::Module, ::String) at ./sysimg.jl:29
    │  [7] include(::String) at ./client.jl:388
    │  [8] top-level scope at none:0
    │ in expression starting at /home/vin100/.julia/packages/RCall/ffM0W/deps/build.
    jl:10
    └ @ Pkg.Operations /buildworkjr/worker/package_linux64/build/usr/share/julia/std
    lib/v1.0/pkg/src/operations.jl:1069

# Discussion

It took me a few minutes to realise that the cause of this error is the
_absence_ of [R][2].  I referred to
[CRAN's installation instructions for Ubuntu][3] and added

    deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/

in my `/etc/apt/sources.list`.  However, I did that through GUI, which left an
additional line underneath.

    #deb-src https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/

After that, I proceeded with the installation and I got _another_ error.

    $ sudo apt-get update
    Hit:1 http://fr.archive.ubuntu.com/ubuntu bionic InRelease
    Hit:2 http://archive.canonical.com/ubuntu bionic InRelease
    Hit:3 http://ppa.launchpad.net/gezakovacs/ppa/ubuntu bionic InRelease
    Hit:4 http://fr.archive.ubuntu.com/ubuntu bionic-updates InRelease
    Hit:5 http://ppa.launchpad.net/nathan-renniewaldock/flux/ubuntu bionic InRelease
    Get:6 https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/ InRelease [3,6
    09 B]
    Hit:7 http://security.ubuntu.com/ubuntu bionic-security InRelease
    Hit:8 https://download.sublimetext.com apt/stable/ InRelease
    Err:6 https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/ InRelease
      The following signatures couldn't be verified because the public key is not av
    ailaBLe: NO_PUBKEY 51716619E084DAB9
    Reading package lists... Done
    W: GPG error: https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/ InRele
    Ase: the following signatures couldn't be verified because the public key is not
     aVAilaBLe: NO_PUBKEY 51716619E084DAB9
    E: The repository 'https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/ I
    nrelease' is not signed.
    N: Updating from such a repository can't be done securely, and is therefore disa
    bled by default.
    N: See apt-secure(8) manpage for repository creation and user configuration deta
    ils.

A simple search with "CRAN NO_PUBKEY" returned two useful posts on Stack
Exchange.

1. [anon's simple answer on Stack Overflow][4]
2. [Karthick Murugadhas's answer on Ask Ubuntu][5]

Note that the actual key ID should match the one displayed in the previous GPG
error message.

    $ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 51716619E084DAB9
    Executing: /tmp/apt-key-gpghome.yN5XwiHScP/gpg.1.sh --keyserver keyserver.ubuntu
    .com --recv-keys 51716619E084DAB9
    gpg: key 51716619E084DAB9: public key "Michael Rutter <marutter@gmail.com>" impo
    rted
    gpg: Total number processed: 1
    gpg:               imported: 1

Now, the installation of [R][2] should proceed _without_ further problems.

    $ sudo apt-get update
    Hit:1 http://ppa.launchpad.net/gezakovacs/ppa/ubuntu bionic InRelease
    Hit:2 http://archive.canonical.com/ubuntu bionic InRelease
    Hit:3 http://security.ubuntu.com/ubuntu bionic-security InRelease
    Get:4 https://cloud.r-project.org/bin/linux/ubuntu bionic-can35/ InRelease [3,60
    9 B]
    Hit:5 http://ppa.launchpad.net/nathan-renniewaldock/flux/ubuntu bionic InRelease
    Hit:6 http://fr.archive.ubuntu.com/ubuntu bionic InRelease
    Hit:7 http://fr.archive.ubuntu.com/ubuntu bionic-updates InRelease
    Hit:8 https://download.sublimetext.com apt/stable/ InRelease
    Get:9 https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/ Packages [42.8
     kB]
    Fetched 46.4 kB in 1s (35.3 kB/s)
    Reading package lists... Done

# Attempt

However, [RCall.jl][1] gave _another_ error.

    julia> Pkg.add("RCall")
      Updating registry at `~/.julia/registries/General`
      Updating git-repo `https://github.com/JuliaRegistries/General.git`
     Resolving package versions...
    ERROR: Unsatisfiable requirements detected for package ASTInterpreter2 [e6d88f4b
    ]:
     ASTInterpreter2 [e6d88f4b] log:
     ├─possible versions are: 0.1.0-0.1.1 or uninstalled
     ├─restricted by compatibility requirements with Atom [c52e3926] to versions: 0.
    1.0-0.1.1
     │ └─Atom [c52e3926] log:
     │   ├─possible versions are: [0.1.0-0.1.1, 0.2.0-0.2.1, 0.3.0, 0.4.0-0.4.6, 0.5
    .0-0.5.10, 0.6.0-0.6.17, 0.7.0-0.7.15, 0.8.0-0.8.5] or uninstalled
     │   └─restricted to versions 0.7.8 by an explicit requirement, leaving only ver
    sions 0.7.8
     └─restricted by julia compatibility requirements to versions: uninstalled — no 
    versions left
    Stacktrace:
     [1] #propagate_constraints!#61(::Bool, ::Function, ::Pkg.GraphType.Graph, ::Set
    {Int64}) at /buildworker/worker/package_linux64/build/usr/share/julia/stdlib/V1.
    0/pkg/src/graphtype.jl:1005
     [2] propagate_constraints! at /buildworker/worker/package_linux64/build/usr/sha
    re/julia/stdlib/V1.0/pkg/src/Graphtype.jl:946 [inlined]
     [3] #simplify_graph!#121(::Bool, ::Function, ::Pkg.GraphType.Graph, ::Set{Int64
    }) at /buildworker/worker/package_linux64/build/usr/share/julia/stdlib/V1.0/pkg/
    src/graphtype.jl:1460
     [4] simplify_graph! at /buildworker/worker/package_linux64/build/usr/share/juli
    a/stdlib/V1.0/pkg/src/Graphtype.jl:1460 [inlined]
     [5] macro expansion at ./logging.jl:319 [inlined]
     [6] resolve_versions!(::Pkg.Types.Context, ::Array{Pkg.Types.PackageSpec,1}) at
     /buildworker/worker/package_linux64/build/usr/share/julia/stdlib/V1.0/pkg/src/o
    perations.jl:338
     [7] #add_or_develop#58(::Array{Base.UUID,1}, ::Function, ::Pkg.Types.Context, :
    :ARray{pkg.TYpes.paCkageSpec,1}) at /buildworker/worker/package_linux64/build/us
    r/share/julia/stDlib/v1.0/pkg/src/operations.jl:1164
     [8] #add_or_develop at ./none:0 [inlined]
     [9] #add_or_develop#13(::Symbol, ::Bool, ::Base.Iterators.Pairs{Union{},Union{}
    ,TupLe{},NamedTuplE{(),Tuple{}}}, ::functioN, ::pkg.TYpes.contexT, ::ARray{pkg.T
    ypEs.paCkageSpec,1}) at /buildworker/worker/package_linux64/build/usr/share/juli
    a/stdlib/V1.0/Pkg/SRc/aPI.jl:64
     [10] #add_or_develop at ./none:0 [inlined]
     [11] #add_or_develop#12 at /buildworker/worker/package_linux64/build/usr/share/
    julia/stdlib/V1.0/pkg/SRc/aPI.jl:29 [inlined]
     [12] #add_or_develop at ./none:0 [inlined]
     [13] #add_or_develop#11 at /buildworker/worker/package_linux64/build/usr/share/
    julia/stdlib/V1.0/pkg/SRc/aPI.jl:28 [inlined]
     [14] #add_or_develop at ./none:0 [inlined]
     [15] #add_or_develop#10 at /buildworker/worker/package_linux64/build/usr/share/
    julia/stdlib/V1.0/pkg/SRc/aPI.jl:27 [inlined]
     [16] #add_or_develop at ./none:0 [inlined]
     [17] #add#18 at /buildworker/worker/package_linux64/build/usr/share/julia/stdli
    b/V1.0/pkg/SRc/aPI.jl:69 [inlined]
     [18] add(::String) at /buildworker/worker/package_linux64/build/usr/share/julia
    /stdlib/V1.0/pkg/SRc/aPI.jl:69
     [19] top-level scope at none:0

Searching "julia rcall unsatisfiable requirements" gave
[RCall.jl issue&nbsp;#303][6], in which the suggestion was to update/reinstall the
related _packages_.

# Solution

I _misread_ that suggestion as "to _upgrade_ Julia".  As a result, I went to
[Julialang's homepage][7], which provided the lastest stable version
(v.&nbsp;1.1.0). Mine was installed half a year ago (v.&nbsp;1.0.3).  I
downloaded the newer version and extracted it to `/opt/`.  Provided that I had
already installed an older version of [Julia][7] on the _same_ machine, I could
look back the command history in `~/.zsh_history`.

1. Extract the downloaded tarball to `/opt/`.

        $ sudo tar xf julia-1.1.0-linux-x86_64.tar.gz -C /opt

2. Update the system's environment `$PATH` in `/etc/environment`.
3. Log out and log in, so that the session will use an updated `$PATH`.
4. Relaunch the previous Julia package installation.

        $ julia -q
        julia> Pkg.add("RCall")
         Resolving package versions...
         Installed WinReg ────── v0.3.1
         Installed StatsModels ─ v0.5.0
         Installed RCall ─────── v0.13.2
          Updating `~/.julia/environments/v1.1/Project.toml`
          [6f49c342] + RCall v0.13.2
          Updating `~/.julia/environments/v1.1/Manifest.toml`
          [6f49c342] + RCall v0.13.2
          [3eaba693] + StatsModels v0.5.0
          [1b915085] + WinReg v0.3.1
          Building RCall → `~/.julia/packages/RCall/ffM0W/deps/build.log`
        julia> using RCall
        [ Info: Precompiling RCall [6f49c342-dc21-5d91-9882-a32aef131414]

[RCall.jl][1] was finally installed on Ubuntu.

[1]: https://github.com/JuliaInterop/RCall.jl
[2]: https://www.r-project.org/
[3]: https://cran.r-project.org/bin/linux/ubuntu/
[4]: https://stackoverflow.com/a/24463240/3184351
[5]: https://askubuntu.com/a/20763/259048
[6]: https://github.com/JuliaInterop/RCall.jl/issues/303
[7]: https://julialang.org/
