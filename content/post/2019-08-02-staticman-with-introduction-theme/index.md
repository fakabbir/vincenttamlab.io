---
title: "Staticman With Introduction Theme"
date: 2019-08-02T19:01:07+02:00
categories:
- blogging
tags:
- Staticman
- Hugo
- Introduction
toc: true
draft: false
---

### Goal

To port [Huginn]'s [Staticman] integration to [Introduction].

### Difficulties

I've used some class names from [Minimal Mistakes][MM] since the
[modals in the original code][modal] clashes with [Introduction]'s mobile
responsive card display for projects.  If I had know the practice of prepending
a CSS class to avoid overiding the CSS properties of other components of a web
site, I wouldn't have mixed this Jekyll theme with my
[template for nested comments][nestedcmt].

Due to my existing work on [my SASS file for Staticman + Introduction][mysass],
I had decided to continue working with the class names in this SASS file.
Changing the CSS class names in the JS script was easier than doing so in the
SASS file since I was more familiar with the former.

In [Minimal Mistakes][MM], each comment is contained a big container that uses
`float: left`.  The width of each comment has to be accurately calculated so
that each comment occupied one whole row.

In [Huginn], the datetime of the comment floats on the right, so put it into the
`<h3>` tag for the comment author.  However, putting them in two seperate lines
seems to suit [Introduction] better.  I've made some adaptations to my reply
button's event handler, which makes a simple use of delegated event to simplify
the loading of `.threadID`, `.replyID` and `.replyName` into the comment form.

I've learnt to use [Font Awesome 5][fa5] with (S)CSS so that the arrow between
the comment author and the reply target name is not linked.  The
`text-decoration: none` property of an anchor can be used to disable underlining
of a link when `mouseOn`.

[SASS variables][sassvar] helps me a lot in labeling figures with comprehensible
words like `$avatar-width`.  This helps the calculations of the comments' width.
Another useful basic skill is to the border.  This helps debugging code.

I was stuck as two silly errors during the setup.

1. Inserted three underscores: `comment___content`, while there's only two in
[Minimal Mistakes][MM], in the main comment but _not_ the comment reply.  This
tricked me to track the precedence of style rules with [Firefox].  In fact, it's
just a typo.
2. Confusion of class names: it took me an hour to find out that I'd omitted
`-form` in `$('.page__comments-form').submit(...)`.  This caused the submission
of `GET` requests when the "Submit" button was clicked, and a 404 Not Found
error.  Hard-coding the `type` of the AJAX request would result in a 500
Internal Server Error.  Finally, it took me another hour to resolve conflicts
and to solve the mysterious `{"success": false}` error.

Voilà the updated PR: [victoriadrake/hugo-theme-introduction#119][pr119].

![Introduction + Staticman UI](ismui190802.png)

[Huginn]: https://lstu.fr/hg
[Staticman]: https://staticman.net
[Introduction]: https://lstu.fr/ism0
[MM]: https://github.com/mmistakes/minimal-mistakes/
[modal]: https://github.com/halogenica/beautifulhugo/blob/8f3b0c3296ebb04fdb76b95d7af23787657d8ea8/static/js/staticman.js#L11-L39
[nestedcmt]: /post/2018-11-17-nested-comments-in-beautiful-hugo/
[mysass]: https://github.com/victoriadrake/hugo-theme-introduction/blob/37c7cf12b8b24f1022cbd343d1781fb00d824a26/assets/sass/_staticman.sass
[fa5]: https://fontawesome.com/
[sassvar]: https://sass-lang.com/documentation/variables
[Firefox]: https://www.mozilla.org/fr/firefox/new/
[pr119]: https://github.com/victoriadrake/hugo-theme-introduction/pull/119
