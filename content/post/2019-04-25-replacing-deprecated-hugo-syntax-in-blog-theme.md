---
title: "Replacing Deprecated Hugo Syntax in Blog Theme"
subtitle: "Fixing Beautiful Hugo issue #261"
date: 2019-04-25T12:45:08+02:00
categories:
- blogging
tags:
- Hugo
- Staticman
draft: false
---

# Problem

Since the recent Hugo upgrade to v0.55.3, the following messages popped up after
each local site regeneration with `hugo server`.

```
WARN 2019/04/24 18:07:00 Page's .URL is deprecated and will be removed in a futu
re release. Use .Permalink or .RelPermalink. If what you want is the front matte
r URL value, use .Params.url.
WARN 2019/04/24 18:07:00 Page's .Hugo is deprecated and will be removed in a fut
ure release. Use the global hugo function.
WARN 2019/04/24 18:07:00 Page's .RSSLink is deprecated and will be removed in a 
future release. Use the Output Format's link, e.g. something like: 
    {{ with .OutputFormats.Get "RSS" }}{{ .RelPermalink }}{{ end }}.
Total in 136 ms
```

[GitLab's online runner][1] also gave the _same_ type of warnings.  This was
reported in [Beautiful Hugo issue #261][5].

# Goal

To suppress the above warning messages.

# Cause

The above warning messages are self-explanatory.

| Deprecated | New | Source |
| --- | --- | --- |
| `{{ .Hugo.Generator }}` | `{{ hugo.Generator }}` | [.Hugo.Generator は廃止されるので hugo.Generator を使おう][9] |
| `{{ .RSSLink }}` | `{{ with .OutputFormats.Get "RSS" }}{{ . RelPermalink }}{{ end }}` | [Hugo Introduction theme pull request #146][10] |
| `{{ .Hugo.Version }}` | `{{ .Site.Hugo.Version }}` | [My question on Hugo Discourse][3] |
| `{{ .URL }}` | `{{ .Permalink }}`/`{{ .RelPermalink }}` | [Hugo Introduction theme at commit `8c258bbd`][11] |

For the last row, I've _preferred_, in general, the _former_ over the _later_
unless for the link "[Read more]" and for [Staticman][12]'s page `$slug`, which
is actually the post's _relative_ permalink with slashes `/` stripped off.

# Difficulties

After some trial and error, I've observed that:

1. `{{ .URL }}` in `layouts/partials/nav.html` _doesn't_ need to be replaced
since it represents the _site_'s URL.
2. `{{ hugo.Version }}` works well in Go-HTML template files, _but not in YAML_
files.  I posted a [question on Stack Overflow][2], but it got quickly drowned
by an array of newer questions.  Therefore, I wrote another question on Hugo
Discourse.  Thanks to [@bep's solution][3] (`{{ .Site.Hugo.Version }}`), I've
completed this replacement of deprecated Hugo syntax on the
[`fix-dotHugo-deprecated`][4] branch.

# Aftermath

1. I've created [pull request #269][6] to solve [issue #261][5].
2. I've [merged `fix-dotHugo-deprecated` against `dev` branch][7].  There's a
merge conflict due to [my tweaks on the theme for nested Staticman comments][8].

[1]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/-/jobs/201796830
[2]: https://stackoverflow.com/q/55836908/3184351
[3]: https://discourse.gohugo.io/t/show-hugos-version-number-in-yaml-file/18288?u=vincenttam
[4]: https://gitlab.com/VincentTam/beautifulhugo/tree/fix-dotHugo-deprecated
[5]: https://github.com/halogenica/beautifulhugo/issues/261
[6]: https://github.com/halogenica/beautifulhugo/pull/269
[7]: https://gitlab.com/VincentTam/beautifulhugo/commit/4d3dda4089ed2712a528f2cb9b2226fd2de25bf3
[8]: /post/2018-11-17-nested-comments-in-beautiful-hugo/
[9]: https://qiita.com/peaceiris/items/b6d611e184b2f28cc0ab
[10]: https://github.com/victoriadotdev/hugo-theme-introduction/pull/146
[11]: https://github.com/victoriadotdev/hugo-theme-introduction/commit/8c258bbd0700e62be2feb5e516a4e6efe47e5776
[12]: https://staticman.net/
