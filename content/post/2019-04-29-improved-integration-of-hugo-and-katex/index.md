---
title: "Improved Integration of Hugo and $\\KaTeX$"
date: 2019-04-29T09:48:32+02:00
categories:
- blogging
tags:
- KaTeX
- Mmark
- Hugo
draft: false
toc: true
---

### Problem

There's _no_ [custom $\KaTeX$ macro][1] in local preview since I've merged some
recent commits from the upstream of this blog's theme, in particular,
[Beautiful Hugo][8]'s pull requests [#246][9] and [#255][10], which allowed
_self-hosting the theme's static JS, CSS and font files_.  This self-hosted
option is particularly useful in case of _slow response from [Cloudflare][11]'s
CDN_.

![hugo local preview no KaTeX macro](preview.png)

![public Hugo blog KaTeX auto-renderer error](public1.png)

Even they _do_ appear on the public GitLab site, the final rendered Markdown +
$\TeX$ code would be succumb to syntax errors due to their absence in the
preview process.

### Solution
#### General method

Searching "Hugo KaTeX", I've found
[Edwin Kofler's blog post about Hugo + $\KaTeX$][2] useful.

Just [choose Mmark][3], which is a fork of [Blackfriday][4], a popular Markdown
processor for [Go][5], as the post's Markdown parser by either

1. adding the option `markup: mmark` in the content's front matter; or
2. changing the content file's extension name to `.mmark`.

After that, Hugo recognised the inline and display math by a pair of surrounding
`$$`. It _wouldn't_ try to interpret the underscores `_` inside a pair of `$$`.
The difference between inline and display math is whether `$$` occupies one
entire line.

#### Difficulties
##### Repeated custom $\KaTeX$ delimiters

I've added [Showdown][6] for static comment preview in
`static/js/staticman-preview.js` in
[my _tweaked_ Beautiful Hugo theme][7].  This JS file also contains lines that
convert `$$ ... $$` into math expresions.

```js
$( document ).ready(function() {
  var myKaTeXOptions = {
    // LaTeX like math delimiters
    delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "\\[", right: "\\]", display: true},
    {left: "$", right: "$", display: false},
    {left: "\\(", right: "\\)", display: false}
    ],
    throwOnError: false
  };

  ...
});
```

##### My custom $\KaTeX$ macros _not_ recognised

I've created [some custom $\KaTeX$ macros][1] to simplify the math writing
process.  However, they _won't_ work _without_ the lines containing `$` in the
`delimiters` in `static/js/katex-macros.js`.  As a result, I have to put back
these two lines at [commit `9e640f21`][12].  In case of custom macros, I still
need to rely on those two lines and the surrounding `<div>` tag, but I can put
`markup: mmark` in the posts' front matter.

##### Existing $\TeX$ inline math markup

I prefer $\TeX$'s way of inline math markup `$ ... $` over its [Blackfriday][4]
counterpart `$$ ... $$`.  _Systematically_ replacing the former by the later
would be a formidable task.  I've given that up in favour of one _single_ line
`$` in `delimiters`.

##### Chores: Format the math

The `content/` changes at [commit `9e640f21`][12] can be categorized into:

- inline math: unescaped underscores `_`.

    ```
    Lorem ipsum $$ s_1 = a_2 \times p_3 $$.
    ```

- display math: surrounding `$$`'s and empty lines needed.  Unwrapped `<div>`
  for normal block equations.

    ```
    Lorem ipsum.

    $$
    s_1 = a_2 \times p_2
    $$
    ```

- _manual_ custom macro detection and review

#### Results

[My custom macros][1] have finally come back!

![hugo local preview with KaTeX macro](preview2.png)

[1]: /post/2018-09-27-custom-katex-macros/
[2]: //eankeen.github.io/blog/render-latex-with-katex-in-hugo-blog/
[3]: //gohugo.io/content-management/formats/#use-mmark
[4]: //github.com/russross/blackfriday
[5]: //golang.org/
[6]: //demo.showdownjs.com/
[7]: //gitlab.com/VincentTam/beautifulhugo/
[8]: //github.com/halogenica/beautifulhugo/
[9]: //github.com/halogenica/beautifulhugo/pull/246
[10]: //github.com/halogenica/beautifulhugo/pull/255
[11]: //www.cloudflare.com/
[12]: //gitlab.com/VincentTam/vincenttam.gitlab.io/commit/9e640f21df6a7fa5724c52311d0751671e4a16db#794aa83d54c8ad0a76fb7f5b8a0b243c15940c33
