---
title: "Matrix Diagonalisation and Change of Basis"
date: 2022-06-02T14:24:19+02:00
categories:
- math
tags:
- LaTeX
draft: false
---

Here's the $\LaTeX$ code of my diagram for matrix diagonalisation to be used on
Discord.

Why do matrix diagonalisation on square matrix $P$?

If we can find a diagonal matrix $D$ and a square matrix $Q$ such that
$P = QDQ^{-1}$, then we can easily compute $(P + \lambda I)^n$ for any scalar
$\lambda$ and integer $n$ because $D^n$ is easy to compute.

```tex
\[\begin{tikzcd}
    {{}} & {{}} & \cdots & {} \\
    {{}} & {{}} & \cdots & {{}}
    \arrow["P", from=1-1, to=1-2]
    \arrow["{Q^{-1}}"', from=1-1, to=2-1]
    \arrow["D"', from=2-1, to=2-2]
    \arrow["Q"', from=2-2, to=1-2]
    \arrow["P", from=1-2, to=1-3]
    \arrow["D"', from=2-2, to=2-3]
    \arrow["P", from=1-3, to=1-4]
    \arrow["D"', from=2-3, to=2-4]
    \arrow["Q"', from=2-4, to=1-4]
\end{tikzcd}\]
```

After viewing the power of the Discord bot $\TeX{}$it, which renders $\LaTeX$
code on Discord, I gave up spending more time on exploring more functionalities
of $\KaTeX$ (say, commutative diagrams) because Discord and $\LaTeX$ spread
math knowledge much better than a static blog for basic math: the former allows
instant feedback from the reader.  The later is better for taking notes.  To
display more complicated graphics, I can compile to PDF first, then use
[`dvisvgm`][dvisvgm] with `-P` for `--pdf`.  (The small `-p` selects
`--page=ranges`.)

On Discord, the above code block is to be typeset as plain $\LaTeX$ code.

- MathBot: `=texp`
- $\TeX{}$it: `,tex`

To get the PDF then SVG, we need to get rid of the surrounding `\[\]` since
we're going to use the `standalone` mode.  It would be nice to have a `border`
of `2pt`.

```tex
\documentclass[tikz, border=2pt]{standalone}
\usepackage{tikz-cd}
\begin{document}
\begin{tikzcd}
...
\end{tikzcd}
\end{document}
```

![matrix diagonalisation](matrix-diag.svg)

The above diagram is rendered with

- `-P`: from PDF
- `-o`: specify output file name
- `-T "S 2.5"`: transform by a scaling factor of `2.5`.

Inside some `arrow[...]`, there's an extra `'`.  In fact, that's from
[quiver]'s generated $\LaTeX$ code and that determines whether the arrow's
label text is above/below the arrow.

[original quiver diagram][diag1]

To finish, I'll include another TikZ diagram illustrating the matrix
representation of linear transformation with respect to two bases.

```tex
\[\begin{tikzcd}
    {[\cdot]_E} & {v_i} & {T(v_i)} \\
    {[\cdot]_B} & {e_i=[v_i]_B} & {[T(v_i)]_B}
    \arrow["{[T]_B}"', from=2-2, to=2-3]
    \arrow["T", from=1-2, to=1-3]
    \arrow["{?}", shift left=1, harpoon, from=1-2, to=2-2]
    \arrow["{B [v_i]_B}", shift left=1, harpoon, from=2-2, to=1-2]
    \arrow["{?}", shift left=1, harpoon, from=1-3, to=2-3]
    \arrow["{B[T(v_i)]_B}", shift left=1, harpoon, from=2-3, to=1-3]
\end{tikzcd}\]
```

![change of basis](change-basis.svg)

[original quiver diagram][diag2]

[dvisvgm]: https://dvisvgm.de/
[quiver]: https://q.uiver.app/
[diag1]: https://q.uiver.app/?q=WzAsOCxbMCwwLCJ7fSJdLFsxLDAsInt9Il0sWzAsMSwie30iXSxbMSwxLCJ7fSJdLFsyLDAsIlxcY2RvdHMiXSxbMiwxLCJcXGNkb3RzIl0sWzMsMF0sWzMsMSwie30iXSxbMCwxLCJQIl0sWzAsMiwiUV57LTF9IiwyXSxbMiwzLCJEIiwyXSxbMywxLCJRIiwyXSxbMSw0LCJQIl0sWzMsNSwiRCIsMl0sWzQsNiwiUCJdLFs1LDcsIkQiLDJdLFs3LDYsIlEiLDJdXQ==
[diag2]: https://q.uiver.app/?q=WzAsNixbMiwxLCJbVCh2X2kpXV9CIl0sWzIsMCwiVCh2X2kpIl0sWzEsMSwiZV9pPVt2X2ldX0IiXSxbMSwwLCJ2X2kiXSxbMCwwLCJbXFxjZG90XV9FIl0sWzAsMSwiW1xcY2RvdF1fQiJdLFsyLDAsIltUXV9CIiwyXSxbMywxLCJUIl0sWzMsMiwiPyIsMCx7Im9mZnNldCI6LTEsInN0eWxlIjp7ImhlYWQiOnsibmFtZSI6ImhhcnBvb24iLCJzaWRlIjoidG9wIn19fV0sWzIsMywiQiBbdl9pXV9CIiwwLHsib2Zmc2V0IjotMSwic3R5bGUiOnsiaGVhZCI6eyJuYW1lIjoiaGFycG9vbiIsInNpZGUiOiJ0b3AifX19XSxbMSwwLCI/IiwwLHsib2Zmc2V0IjotMSwic3R5bGUiOnsiaGVhZCI6eyJuYW1lIjoiaGFycG9vbiIsInNpZGUiOiJ0b3AifX19XSxbMCwxLCJCW1Qodl9pKV1fQiIsMCx7Im9mZnNldCI6LTEsInN0eWxlIjp7ImhlYWQiOnsibmFtZSI6ImhhcnBvb24iLCJzaWRlIjoidG9wIn19fV1d
