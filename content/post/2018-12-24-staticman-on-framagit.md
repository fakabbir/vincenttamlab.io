---
title: "Staticman on Framagit"
date: 2018-12-24T01:59:57+01:00
type: post
categories:
- blogging
tags:
- Staticman
- Framagit
- GitLab
bigimg: [{src: "/img/staticmanlab-frama.svg", desc: "Framagit + Staticman"}]
draft: false
---

It's a pity that some [GitLab] users have removed [Staticman] from their repo. I
saw demand for Staticman from [Framagit] users.  Therefore, after two days of
testing, another API instance for Framagit has been created at
https://staticman-frama.herokuapp.com.  (Edit: typo edited, please see my
comment below.)

The default GitLab base URL is set to "https://gitlab.com".  To provide
Staticman support for a self-hosted GitLab service, another Staticman API server
has to be set up.

I tried applying for [OpenShift], but it's been reported on Reddit that it takes
about 10--20 days to get started.  Amazon requires verification by credit card.
I _couldn't_ deploy Staticman using [Zeit Now][zeit]'s Node.js server builder.
Finally, I turned back to [Heroku].

To minimise the mistakes, here's a list of procedures that I've adopted for this
deployment.  You may view the
[source code of the public API instance for Framagit][src] for details.

1. I cloned the `deploy` branch of [my fork of Staticman][fork].
2. I created a new Heroku app for this clone.
3. I _copied_ the GitHub and GitLab tokens saved in Heroku config variables.
4. I generated an RSA key and update Heroku config variable `RSA_PRIVATE_KEY`.
5. I created a `framagit` branch from this branch.
6. I deployed the app to Heroku.
7. I tested this deployment by changing the form action URL of an existing
Staticman + GitLab demo site.  A web developer tool in a modern web browser will
be useful for this.  Testing is with Postman is even better for debugging, but
that requires the installation of the app.
8. I added a line for using the corresponding Heroku config variable as the
`gitlabBaseUrl` in the API config.
9. I set Heroku config variable for GitLab base URL as "https://gitlab.com",
which is the default value.
10. I repeated step 7 to see that step 9 was working.
11. I repeaded step 9 with a custom GitLab instance "https://framagit.org".
12. I replaced the GitLab token stored in the Heroku config variable by the one
for Framagit.
13. I deployed Staticman again to Heroku.
14. I repeated step 7 to see that steps 11--12 were working.

[fork]: https://github.com/VincentTam/staticman/tree/deploy
[src]: https://framagit.org/staticman-gitlab-pages/staticman
[Staticman]: https://staticman.net
[GitLab]: https://gitlab.com
[OpenShift]: https://manage.openshift.com/
[zeit]: https://zeit.co/
[Heroku]: https://www.heroku.com/
[Framagit]: https://framagit.org
