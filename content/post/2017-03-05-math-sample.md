---
title: Math Sample
subtitle: Using KaTeX
type: post
date: 2017-03-05
tags:
- example
- math
---

<i class="fa fa-exclamation-circle"></i> _This is a sample created by
the developer of Beautiful Hugo._

KaTeX can be used to generate complex math formulas server-side. 

<div>$$
\phi = \frac{(1+\sqrt{5})}{2} = 1.6180339887\cdots
$$</div>

Additional details can be found on [GitHub](https://github.com/Khan/KaTeX) or on the [Wiki](http://tiddlywiki.com/plugins/tiddlywiki/katex/).
<!--more-->

### Example 1

If the text between $$ contains newlines it will rendered in display mode:
```
<div>$$
f(x) = \int_{-\infty}^\infty\hat f(\xi)\,e^{2 \pi i \xi x}\,d\xi
$$</div>
```

<div>$$
f(x) = \int_{-\infty}^\infty\hat f(\xi)\,e^{2 \pi i \xi x}\,d\xi
$$</div>


### Example 2

```
<div>$$
\frac{1}{\Bigl(\sqrt{\phi \sqrt{5}}-\phi\Bigr) e^{\frac25 \pi}} =
1+\frac{e^{-2\pi}} {1+\frac{e^{-4\pi}} {1+\frac{e^{-6\pi}} {1+\frac{e^{-8\pi}}
\{1+\cdots}}} 
$$</div>
```
<div>​​$$
\frac{1}{\Bigl(\sqrt{\phi \sqrt{5}}-\phi\Bigr) e^{\frac25 \pi}} =
1+\frac{e^{-2\pi}} {1+\frac{e^{-4\pi}} {1+\frac{e^{-6\pi}} {1+\frac{e^{-8\pi}}
\{1+\cdots}}}
$$</div>

### Example 3

```
<div>$$
1 + \frac{q^2}{(1-q)}+\frac{q^6}{(1-q)(1-q^2)}+\cdots =
\prod_{j=0}^{\infty}\frac{1}{(1-q^{5j+2})(1-q^{5j+3})}, \quad\quad \text{for
}\lvert q\rvert<1.
$$</div>
```

<div>$$
1 + \frac{q^2}{(1-q)}+\frac{q^6}{(1-q)(1-q^2)}+\cdots =
\prod_{j=0}^{\infty}\frac{1}{(1-q^{5j+2})(1-q^{5j+3})}, \quad\quad \text{for
}\lvert q\rvert<1.
$$</div>
