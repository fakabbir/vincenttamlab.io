---
title: "Animated GIF Screenshots on Ubuntu"
date: 2019-07-31T12:56:31+02:00
categories:
- technical support
tags:
- Screenshots
- Conky
- Byzanz
toc: true
draft: false
---

### Background

I've to take screenshots to demonstrate [Conky]'s visual output.

![animated GIF screenshot taken by Byzanz](out.gif)

I've chosen [Byzanz] after reading [this answer on Ask Ubuntu][au107726].

[Peek] is more user-friendly, but I prefer CLI's precision.  That's feasible
thanks to a comment mentioning `xwininfo`.

After typing `xwininfo`, click on the target window.  Switching to adjacent
workplace is possible.

    $ xwininfo
    xwininfo: Please select the window about which you
              would like information by clicking the
              mouse in that window.

    xwininfo: Window id: 0x1c0000a "Desktop"

      Absolute upper-left X:  0
      Absolute upper-left Y:  0
      Relative upper-left X:  0
      Relative upper-left Y:  0
      Width: 1920
      Height: 1080
      Depth: 32
      Visual: 0x1a7
      Visual Class: TrueColor
      Border width: 0
      Class: InputOutput
      Colormap: 0x1c00009 (not installed)
      Bit Gravity State: NorthWestGravity
      Window Gravity State: NorthWestGravity
      Backing Store State: NotUseful
      Save Under State: no
      Map State: IsViewable
      Override Redirect State: no
      Corners:  +0+0  -0+0  -0-0  +0-0
      -geometry 1920x1080+0+0

The `--x` and `--y` parameters below correspond to the *absolute* upper-left
position of the window.

    $ byzanz-record --duration=15 --x=200 --y=300 --width=700 --height=400 \
    out.gif

### Problem

The window drawn by [Conky] *isn't* regarded as a "window" by `xwininfo`.  It
measures the _whole_ desktop wallpaper instead.

### Analysis

`xwininfo` has an `-id` parameter to allow specifying the target window.  This
motivates us to look into [Conky]'s standard output.  To simplify things, I'll
kill the Conky instance started since login by issuing `kill con<tab>`.
[Oh-My-Zsh][omz] (or whatever) then completes the argument with the right PID.
Key in `conky` to start another instance of conky.  Luckily, [Conky]'s messages
are useful enough for retriving the right `-id` parameter for `xwininfo`.

    $ conky
    conky: warning: invalid head index, ignoring head settings
    conky: warning: invalid head index, ignoring head settings
    conky: desktop window (400012) is subwindow of root window (1b1)
    conky: window type - normal
    conky: drawing to created window (0x1800002)
    conky: drawing to double buffer

The hexadecimal numbers `400012` and `1b1` in the parenthesis represent the
window IDs for the _whole_ desktop background.

    $ xwininfo -id 1b1
    X Error: 9: Bad Drawable: 0x1
      Request Major code: 14
      Request serial number: 3
    xwininfo: error: No such window with id 0x1

The prefix `0x` has to be added to signify that the window ID is a hexadecimal
number.
 
    $ xwininfo -id 0x400012

    xwininfo: Window id: 0x400012 "mutter guard window"

      Absolute upper-left X:  0
      Absolute upper-left Y:  0
      Relative upper-left X:  0
      Relative upper-left Y:  0
      Width: 1920
      Height: 1080
      Depth: 0
      Visual: 0x41
      Visual Class: TrueColor
      Border width: 0
      Class: InputOnly
      Colormap: 0x0 (not installed)
      Bit Gravity State: ForgetGravity
      Window Gravity State: NorthWestGravity
      Backing Store State: NotUseful
      Save Under State: no
      Map State: IsViewable
      Override Redirect State: yes
      Corners:  +0+0  -0+0  -0-0  +0-0
      -geometry 1920x1080+0+0

In the above message from [Conky], the line "drawing to created window
(0x1800002)" gives the actual `-id`.

### Solution

1. Use the above hexadecimal number for [Conky]'s window `-id`.  The `-stats`
option is here to remove the unnecessary details.

        $ xwininfo -id 0x1800002 -stats

        xwininfo: Window id: 0x1800002 "conky (vin100-LIFEBOOK-AH557)"

          Absolute upper-left X:  1607
          Absolute upper-left Y:  27
          Relative upper-left X:  1607
          Relative upper-left Y:  27
          Width: 313
          Height: 771
          Depth: 32
          Visual: 0xd6
          Visual Class: TrueColor
          Border width: 0
          Class: InputOutput
          Colormap: 0x1800001 (not installed)
          Bit Gravity State: ForgetGravity
          Window Gravity State: NorthWestGravity
          Backing Store State: Always
          Save Under State: no
          Map State: IsViewable
          Override Redirect State: no
          Corners:  +1607+27  -0+27  -0-282  +1607-282
          -geometry 313x771-0+27

2. Configure the above `byzanz-record` command to achieve the goal.

        $ byzanz-record --duration=2 --x=1607 --y=27 --width=313 --height=771 \
        out.gif

Edited: It turns out that the main problem has a much simpler solution.

    $ xwininfo -tree -root | grep conky
         0x1800002 "conky (vin100-LIFEBOOK-AH557)": ("conky" "conky")  313x771+1607+
    27  +1607+27

[Conky]: https://github.com/brndnmtthws/conky
[Byzanz]: https://manpages.ubuntu.com/manpages/bionic/en/man1/byzanz-record.1.html
[au107726]: https://askubuntu.com/a/123515/259048
[Peek]: https://github.com/phw/peek
[omz]: https://github.com/robbyrussell/oh-my-zsh
