---
title: "More Static Nested Comments"
date: 2019-07-30T21:05:35+02:00
type: post
categories:
- blogging
tags:
- Staticman
- Hugo
draft: false
---

### Background

I've made [nested comment support for Beautiful Hugo][1], which I've ported to
other [Hugo][2] themes like [Introduction][3], [Hyde-hyde][4], [etc][5].

### Problem I

Some of those [Hugo themes][6] that I worked with did show the HTML form, but
the CSS styles were gone on the GitLab Pages.

### Reason I

This is due to the lack of `resources/_gen/` in the indexed repo for themes
making use of `assets/`.

### Solution I

0. Check that you have the required prerequisites.  For example,
[Introduction][7] requires [autoprefixer][8] and [postcss-cli][9].
1. Update submodule for the theme in the directory holding the source code of
the static site.
2. Type `hugo [server]` so that the resources are locally generated.
3. Push the repo.

### Problem II

The approach in my linked blog post is too complicated.  It makes use of a mix
of JavaScript and [Hugo][2] template.  This can be hard to comprehend and port,
since the file structure for JavaScripts isn't the same in different themes. 
It's better to make the internal referencing for nested comment display entirely
in Hugo.

### Analysis II

In [Huginn][12] theme, I've modified [my PR for Beautiful Hugo][10] so that the
`id` attribute of the HTML tag containing the comment is set to the `_id` of the
data file for the comment. This works great in the demo site:
https://lstu.fr/hg.  As a result, I'm motivated to develop other features on top
of this, like the display of reply target on top of the comment form when the
reply button is clicked.

However, in [Beautiful Hugo's Staticman integration][11], the `id` attribute for
the `<article>` tag for each `.static-comment` has been set to `comment-N`.

### Solution II

Here's the approach of [my updated PR][10].  The first two steps have been
introduced with [Beautiful Hugo's Staticman integration][11].

1. Render all "main comments" (not replying to any comments).
2. Render all "comment replies", sorted in chronological order according to
their eldest ancestor.
3. When each comment is rendered, record its "thread number" as `hasComments`
and its "reply number" as `hasReplies`.  The former and the later start from one
ane zero respectively.
    - In case of "main comments", the `id` attribute would be `comment-N`;
    - In case of "comment replies", the `id` attribute would be `comment-NrM`,
    in which `r` stands for "reply".

    | comment `_id` | `threadID` | computed `hasReplies` | generated HTML tag `id` |
    | --- | --- | --- | --- |
    | `38862b00-06f2-11e9-9615-79c046dbfeb8` | nil | `0` | `comment-1` |
    | `518dbe00-0707-11e9-8912-c790845d78b1` | `3886...feb8` | `1` | `comment-1r1` |
    | `a0e9c470-b1c1-11e9-8550-2509fccf9571` | `3886...feb8` | `2` | `comment-1r2` |

4. Construct a mapping for each thread.  Take the first thread in
[a sample post on the demo site][14] as an example.

    ```go-html
    {{ $.Scratch.SetInMap "replyIndices" ._id ($.Scratch.Get "hasReplies") }}
    ```

    After a day of research with [Hugo's .Scratch][13], I've construced a
    mapping from `_id` to `hasReplies` (from the first column to the third
    column in the above table).  The reason for constructing a mapping instead
    of a [slice][15] (an array with indefinite length) is that there's _no_ easy
    method for searching for an element and its index in an array. One has to go
    primitive and [loop over the entire array][16].  Nesting an inner loop
    inside another inner loop would add difficulty to the maintenance work.

5. For each comment reply, construct the bookmark

    ```go-html
    <a href="#comment-NrM">{{ .replyName }}</a>
    ```

    by finding the right "reply number" `M` from the mapping, with the comment
    reply's `replyID` as the input argument for the mapping.  (i.e.
    "`replyIndices[.replyID]`")

    ```go-html
    {{- $replyTargetIndex := (index ($.Scratch.Get "replyIndices") .replyID) -}}
    ```

Since commit [`4c08241e`][17], this static bookmark reference has been
available.

### UI improvement after solving the main problem

See commits [`9d994eea..6c68c1ac`][18] for the actual comparison, and the commit
message of [`492c265d`][19] a brief summary.

![Improved Beautiful Hugo UI](rec.gif)

[1]: /post/2018-11-17-nested-comments-in-beautiful-hugo/
[2]: https://gohugo.io
[3]: https://github.com/victoriadrake/hugo-theme-introduction/pull/119
[4]: https://github.com/htr3n/hyde-hyde/pull/58
[5]: https://framagit.org/staticman-gitlab-pages
[6]: https://themes.gohugo.io/
[7]: https://github.com/victoriadrake/hugo-theme-introduction/
[8]: https://github.com/postcss/autoprefixer
[9]: https://github.com/postcss/postcss-cli
[10]: https://github.com/halogenica/beautifulhugo/pull/222
[11]: https://github.com/halogenica/beautifulhugo/pull/99
[12]: https://staticman-gitlab-pages.frama.io/huginn/
[13]: https://gohugo.io/functions/scratch
[14]: https://staticman-gitlab-pages.frama.io/bhdemo/posts/my-second-post/
[15]: https://gohugo.io/functions/slice/
[16]: https://stackoverflow.com/a/38654444/3184351
[17]: https://github.com/halogenica/beautifulhugo/pull/222/commits/4c08241e0f07d1c5f4fc80ed449c1f48ae285107
[18]: https://github.com/halogenica/beautifulhugo/pull/222/files/9d994eeadd02aa5332f78986a3b8257a4107674f..6c68c1ac9ab71f4dd28ed80c6e2f2150621dd6be
[19]: https://github.com/halogenica/beautifulhugo/pull/222/commits/492c265dff38f6e51f872c9ef1716e268096b4e6
