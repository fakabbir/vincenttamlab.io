---
title: "Network Interface Name Detection in Conky"
subtitle: "Once-off Conky config for network graphs"
date: 2019-08-08T18:42:08+02:00
categories:
- technical support
tags:
- Conky
- Lua
draft: false
---

When one changes connection type (say, from ethernet to Wi-Fi), the interface
name changes (e.g. `eth0` → `wlan1`).  To avoid changing [Conky] config file all
the time, here's a little [Lua] function for finding the network interface name.

```lua
function findInterface()
    local handle = io.popen('ip a | grep "state UP" | cut -d: -f2 | tr -d " "')
    local result = handle:read('*a'):gsub('\n$','')
    handle:close()
    return result
end
```

1. `ip a` gives everything about connection info.  Each entry looks like

        3: wlp3s0f0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000

2. Grep the "state UP" and extract the second field, using `:` as a delimiter.
Trim off the spaces around.
3. Use `io.popen()` to grep the shell output. The [use of `:read('*a')`][so] is
necessary.  It appends the trailing newline `\n` character to the output.
That's fine for the function `conky_myspeed(upordown)`,
but `conky_myspeedgraph(upordown, h, w)`.  Remove that with `:gsub('\n$','')`.
The `g` in `gsub()` stands for `global`.  The `sub()` method requires two
arguments limiting the scope of the string substitution.
4. Since the [Lua] function is to be called from time to time by [Conky], it's a
good idea to `:close()` the stream after having finished.

I tried putting everything in one single line without `:close()`ing the stream.
A number `1` prepending with some white spaces were prepended into the output.

The rest of the [Conky Lua script][myscript] consist mainly of routine usage of
`conky_parse()`.  To avoid repetitions like `${lua myupspeed}` and `${lua
mydownspeed}`, I've added one more argument in the function
`conky_myspeed(upordown)`.

To end this post, I'm going to include a short and simple [Perl]-compatible
regex pattern (PCRE) for matching the IP address in case that

    curl http://ipecho.net/plain

returns an HTML document from an ISP that requires web portal login.

    curl http://ipecho.net/plain | grep -Po '(\.?[0-9]{2,3}){4}' || echo 'Not found!'

That's _not_ a proper PCRE for IP extraction, since it matches `.0.0.0.0`.
However, that's easy to understand and short enough to be put inside the
[`.conkyrc` config file][cfg], so that `git diff`'s output won't be too heavy.

[Conky]: https://github.com/brndnmtthws/conky
[Lua]: https://www.lua.org/
[so]: https://stackoverflow.com/a/9676174/3184351
[myscript]: https://gitlab.com/VincentTam/dotfiles/commit/5af04ca5568ee3bc2311e2db029f2e8513142997#1c44d2b44110d3b7b9f44ddf1fd549a8c385dccb
[Perl]: https://www.perl.org/
[cfg]: https://gitlab.com/VincentTam/dotfiles/blob/5af04ca5568ee3bc2311e2db029f2e8513142997/conky/.conkyrc
