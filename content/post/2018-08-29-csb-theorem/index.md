---
title: "CSB Theorem"
subtitle: "A visual argument for CSB Theorem"
date: 2018-08-29T11:25:24+02:00
type: post
categories:
- math
tags:
- set theory
---

<i class="fa fa-info-circle" aria-hidden></i> This _isn't_ a substitute for
books <i class="fa fa-book" aria-hidden></i>.

### Reminder

- $A \preceq B$: $A$ can be "_injected_" into $B$.
- $A \sim B$: $A$ and $B$ share the _same cardinality_.
- $A \prec B$: $A$ can be "_injected_" into $B$, but it's "_smaller_" than $B$.
- A **finite** set can be "_counted_" from one to some nonnegative integer.
- **Infinite** is the "_antonym_" of finite.

### Wolf's proof

When I first saw this proof in Robert S. Wolf's
[_Proof, Logic and Conjecture: The Mathematician's Toolbox_][1], I gave it up
since it _wasn't_ as intuitive as the statement.

### An informal argument

Later, I found some interesting _illustrations_ in Richard Hammack's
[_Book of Proof_][2].

1. "_Draw_" gray $A$ and white $B$.
2. "_Draw_" the given injections $f: A \to B$ ($A$ contained in $B$) and
$g: B \to A$ ($B$ contained in $A$).  (Figure&nbsp;13.4)
3. "_Draw_" an _infinite chain_ of _alternating_ injections starting from $A$.
(Figure&nbsp;13.5)
4. In "_diagram&nbsp;$A$ at step infinity $\infty$_" ($A$ containing $B$
containing $A$ …), label the "_gray region_" as $G$.
5. Label remaining white region as $W$.  (i.e. $W := A \setminus G$)
6. "_Draw_" a "_homologous_" diagram with the one in step&nbsp;4 on the
right-hand side, but starting from $B$.  (i.e. $B$ containing $A$ containing
$B$…)  (Figure&nbsp;13.6)
7. It's natural to associate the gray regions $G \subseteq A$ with $f(G)
\subseteq B$ on both sides.  It remains to settle $W$.
    - Applying $f$ on $W$ _won't_ lead to any useful results.
    - Another given injection $g$ _can't_ be applied on $W$ due to domain
    mismatch.
    - Reverse the "_direction_" of $g$ to that it points to the white region
    wrapping gray $f(A)$.

It's nice to see a constructive and _formal_ proof immediately following this
intriguing argument.  The later actually guides me through the former.

[1]: http://www.columbia.edu/~vml2113/Teachers%20College,%20Columbia%20University/Academic%20Year%202011-2012/Spring%202012/MSTM%206051%20-%20Advanced%20Topics%20in%20Nature%20of%20Proofs/Proof,%20Logic,%20and%20Conjecture%20-%20The%20Mathematician%27s%20Toolbox.pdf
[2]: https://www.people.vcu.edu/~rhammack/BookOfProof/BookOfProof.pdf
