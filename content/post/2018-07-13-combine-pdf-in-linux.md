---
title: "Combine PDF in Linux"
subtitle: "Two CLI ways to merge PDF files"
date: 2018-07-13T17:30:54+02:00
type: post
categories:
- technical support
tags:
- PDF
- Linux
- QPDF
---

[pdfunite][1] is fairly easy to use.

    pdfunite PDF-sourcefile1..PDF-sourcefilen PDF-destfile

Another way is to use [QPDF][2].  I've learnt to add `--empty` from
[@qpdf/qpdf:#12][3].

    qpdf --empty --pages *.pdf -- out.pdf

[1]: https://manpages.ubuntu.com/manpages/trusty/man1/pdfunite.1.html
[2]: https://qpdf.sourceforge.net/
[3]: https://github.com/qpdf/qpdf/issues/11
